#!/usr/bin/sh
# Bootstrapping file for before you even have a username or account.

# Read README.md/README.r.md first (symlinked together dw about it). In particular
# ensure that your PARTITION CONFIGURATION is mounted to /mnt

# This file is meant to be run before you even have a username or account. 

# See: https://www.shellhacks.com/yes-no-bash-script-prompt-confirmation/
# https://wiki.archlinux.org/index.php/Installation_guide (section on pacstrap)
# For the actual pacstrap script: https://git.archlinux.org/arch-install-scripts.git/tree/pacstrap.in

# We have the packages here https://git.archlinux.org/archiso.git/tree/configs/releng/packages.x86_64 available
# in this script since this is to be run in the live ISO.

echo "Ensure that the new system (with correct partition layout, as specified in README.r.md), is mounted on /mnt."
echo "This will overwrite all kinds of stuff on that mountpoint, be warned."

read -p "Continue [y/N]?" res
case $res in 
    [Yy]* ) sleep 0;;
    * ) echo "Cancelling";exit;;
esac

echo "Pulling in pacman-contrib for useful scripts - specifically, rankmirror."
pacman -S pacman-contrib

echo "Performing pacstrap of certain packages. You will have access to vim and git and python and networkmanager, and some mirrorlists."
# We have PAM here too, doesn't seem to be in base? idk for sure tho
pacstrap /mnt base linux linux-firmware sudo arch-install-scripts cryptsetup pam htop devtools pacman-contrib vim git python cmake net-tools NetworkManager btrfs-progs dosfstools exfat-utils f2fs-tools e2fsprogs jfsutils nilfs-utils ntfs-3g reiserfsprogs xfsprogs mdadm dmraid lvm2 man-db man-pages texinfo 

echo "Generating fstab file..."
genfstab -U /mnt >> /mnt/etc/fstab

echo "Check for errors :)"
vim /mnt/etc/fstab

# Do I trust sh not to have some weird thing with using the same output variable? Nope!
read -p "Pull the dotfile repo into /mnt/tmp/dotfiles for use in your new system when you chroot in [Y/n]?" res1
case $res1 in
    [Nn]* ) echo "Not cloning dotfile repo.";continue;;
    * ) OUR_DIR="pwd";cd /mnt/tmp/;git clone https://gitlab.com/TheEdenCrazy/dotfiles.git;cd "$OUR_DIR";;
esac

# Generate and copy real mirrorlist (nonmanually)
# https://linuxhint.com/pacman_mirrors_arch_linux/
# I think the mirrorlist gets copied over but we can do a full refreshed download of it.
# -n 0 means output all.
read -p "Use rankmirror to generate a nice mirror list and copy to the new install on /mnt [Y/n]?" res2
case $res2 in    
    [Nn]* ) echo "Not pulling ranked mirrors.";;
    * ) {
        # Mirrorlist was already copied over so the file should exist and we can write to it.
        echo "Pulling ranked mirrors from mirrorlist"; 
        wget -O /tmp/updated_mirrorlist "https://www.archlinux.org/mirrorlist/?country=all&protocol=https&ip_version=4&ip_version=6";
        cat /tmp/updated_mirrorlist | sed -e 's/^#Server/Server/' -e '/^#/d' | rankmirrors -n 0 - > /mnt/etc/pacman.d/mirrorlist;
        echo "Mirrorlist should be written.";
    };;
esac

read -p "chroot into the new system [Y/n]?" res3
case $res3 in
    [Nn]* ) echo "Leaving pre-pacstrap-bootstrap. You can use arch-chroot /mnt at any time to get into your new system."; exit;;
    * ) echo "chrooting into the new system with arch-chroot"; arch-chroot /mnt;;
esac
