#!/usr/bin/python3

print("Exceptionally simple hostname file generator :). Can you tell I find bash scripting uncomfortably fragile?")
print("Note: This only does basic validation. Generally a good guideline that it should start with lowercase ascii and consist of lowercase ascii and dashes.")

has_valid_hostname = False


while not has_valid_hostname:
    new_hostname = input("Please enter hostname:").strip()
    check_hostname = input("Please confirm hostname:").strip()
    has_valid_hostname = (new_hostname == check_hostname) and not " " in new_hostname and 
    if not has_valid_hostname:
        print("Hostnames did not match or were invalid >.< .")

hostname_text = "{hostname}"

# Taken from: https://wiki.archlinux.org/index.php/Network_configuration
hosts_text = """\
127.0.0.1       localhost               {hostname}      localhost.localdomain
::1             localhost               {hostname}      localhost.localdomain
127.0.1.1       {hostname}.localdomain  {hostname}      
"""

hosts = open("/etc/hosts", "wt")
hostname = open("/etc/hostname", "wt")

hosts.write(hosts_text.format(hostname=new_hostname))
hostname.write(hostname_text.format(hostname=new_hostname))
