#!/usr/bin/python3

import os
import os.path
import sys
exit = sys.exit

# Note here: https://wiki.archlinux.org/index.php/Installation_guide
# the local timezones are basically directory+subfile of /etc/share/zoneinfo/<Region>/<City> or just a file in there for small regions (like Iceland <3)
# which you then want to force-symbolic-link to /etc/localtime

zone_basedir = "/usr/share/zoneinfo/"
localtime_file = "/etc/localtime"

def get_regions(top_region=""):
    """
    Get all regions, relative to zone_basedir/top_region. If you want full region name then combine top_region with the resulting
    list of regions.
    """
    top_path = os.path.join(zone_basedir, top_region)
    recursive_paths = []
    for curr_full_dir_path, dirnames, filenames in os.walk(top_path):
        # Get full paths
        paths = [os.path.join(curr_full_dir_path, fname) for fname in filenames]
        # Convert to relative paths
        paths = [os.path.relpath(path, top_path) for path in paths]
        recursive_paths.extend(paths)

    # Filter out all directories, we only want files (which represent timezones.
    # realpath removes symbolic links
    recursive_paths = [p for p in recursive_paths if os.path.isfile(os.path.realpath(os.path.join(top_path, p)))]
    return set(recursive_paths)

prompt = "temporal region setter > "

helpstr = """\
MicroTool for setting timezone. 4 commands exist:

help:
  Shows this message

list-zones [region]:
    List the timezones in the given region. If no region is provided, list all timezones.

set-zone [region path]:
    Set the timezone to that specified in the region path. This will in effect force-symbolic-link /usr/share/zoneinfo/[region path] to /etc/localtime

exit:
    Quit
"""

def inv_cmd():
    """
    print invalid command message
    """
    print("Invalid Command. Use command help to see commands.")

def valid_zone(zone_path, full_region_path=None, get_regions_res=None):
    """
    Return if a zone is valid and the full region path (links expanded) to it's file
    """
    if get_regions_res is None:
        get_regions_res = get_regions()
    if full_region_path is None:
         full_region_path = os.path.realpath(os.path.join(zone_basedir, zone_path))
    v = (os.path.commonpath([full_region_path, zone_basedir]) != os.path.commonpath([zone_basedir]) or (zone_path not in get_regions_res))
    # We are outside the directories, probably by using something like /.. as a region. Prevent the user from doing this - or the zone just doesn't exist.

    return (not v, full_region_path)

def inner_proc(cmd_string, argument):
    """
    Process a unified command (empty last argument for ungiven optionals).
    """
    commands = {"help", "list-zones", "set-zone", "exit"}
    lcs = cmd_string.lower()
    if lcs not in commands:
        print("Unrecognised command \"{}\". See command help for valid commands.".format(cmd_string))
        return
    if lcs == "help":
        print(helpstr)
        return
    elif lcs == "set-zone":
        rp = argument.strip()
        validz, full_region_path = valid_zone(rp)
        if not validz:
            print("Zone \"{}\" is invalid.".format(rp))
            return
        os.unlink(localtime_file)  # also removes if normal file :p
        os.symlink(full_region_path, localtime_file)
        return
    elif lcs == "list-zones":
        rp = argument.strip()
        zones = get_regions(rp)
        vzs = []
        for z in zones:
            full_zone = os.path.join(rp, z) 
            if valid_zone(full_zone, get_regions_res=zones):
                vzs.append(full_zone)

        vzs.sort()
        for a in vzs:
            print(a)
        return
    elif lcs == "exit":
        exit()


def proc(cmd_vs):
    """Process a 0, 1, or 2 string tuple as a command-argument pair"""
    processed_cmd_vs = []
    if len(cmd_vs) == 0:
        inv_cmd()
        return
    elif len(cmd_vs) == 1:
        processed_cmd_vs = [cmd_vs[0], ""]
    elif len(cmd_vs) == 2:
        processed_cmd_vs = [cmd_vs[0], cmd_vs[1]]
    else:
        inv_cmd()
        return
    inner_proc(processed_cmd_vs[0], processed_cmd_vs[1])



def repl():
    """
    Simple REPL, 3 commands.
    """
    while True:
        iv = input(prompt)
        cmd_vs = iv.strip().split(sep=" ", maxsplit=1)
        proc(cmd_vs)

if __name__ == "__main__":
    print("Type help for help if you need it")
    repl()
