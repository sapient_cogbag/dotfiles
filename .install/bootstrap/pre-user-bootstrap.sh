#!/usr/bin/sh
#Exciting but irrelevant thing: http://0pointer.net/blog/dynamic-users-with-systemd.html

loadkeys uk

echo "Updating pacman"
pacman -Syyu --needed
pacman -S bash bash-completion --needed
pacman -S base-devel --needed
pacman -S terminus-font
pacman -S firefox

# Good for bypassing firewalls, the great firewall, etc. Does so by pro
pacman -S trojan


# Time stuff (see the https://wiki.archlinux.org/index.php/Installation_guide)
python3 localtime_set.py
hwclock --systohc

echo "Uncomment the locale we want in locale.gen. This is almost certainly en_GB.UTF-8 UTF-8."
sleep 2
vim /etc/locale.gen
echo "Generating locales :)"
locale-gen

echo "Putting some useful defaults into /etc/locale.conf (that is, all the variables as en_GB.UTF-8."

echo 'LANG="en_GB.UTF-8"' > /etc/locale.conf
echo 'LC_MESSAGES="en_GB.UTF-8"' >> /etc/locale.conf
echo 'LC_MONETARY="en_GB.UTF-8"' >> /etc/locale.conf
echo 'LC_PAPER="en_GB.UTF-8"' >> /etc/locale.conf
echo 'LC_MEASUREMENT="en_GB.UTF-8"' >> /etc/locale.conf
echo 'LC_ADDRESS="en_GB.UTF-8"' >> /etc/locale.conf
echo 'LC_TIME="en_GB.UTF-8"' >> /etc/locale.conf

read -p "Edit the /etc/locale.conf file [y/N]?" res
case $res in
    [Yy]* ) vim /etc/locale.conf;;
    * ) sleep 0;;
esac 

echo "Setting keyboard layout to uk and virtual console font to terminus 116n..."
loadkeys uk
echo 'KEYMAP=uk' > /etc/vconsole.conf
echo 'FONT=ter-116n' >> /etc/vconsole.conf

echo "Hostname...."
python3 set_hostname.py

echo "miscellaneous /etc/hostnames.conf."

echo '# Resolver configuration file.' > /etc/host.conf
echo '# See host.conf(5) for details.' >> /etc/host.conf

echo 'multi on' >> /etc/host.conf

echo "Set root password...."
passwd

echo "Installing bootloader..."
pacman -S grub

