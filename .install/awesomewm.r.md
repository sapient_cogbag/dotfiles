# Awesomewm stuff:


\>pkglist:
```
awesome
vicious
awesome-freedesktop
```

**Luarocks for awesomewm stuff**


\>pkglist:
```
lua-sec  # SSL/TLS for lua stuff, I think
luarocks
```


\>script:
```
luarocks install --local pulseaudio\_widget
```


**Lua Editor**


\>pkglist:
```
zerobrane-studio
```


**Xorg Testing Server (in-window x session thingy instance):**
```
xorg-server-xephyr 
```



**Greeter and Locker:**

\>pkglist:
```
accountsservice  # Simple account service over dbus :)
lightdm  # + some greeter (default is lightdm-gtk-greeter)
light-locker  # (Screen locker. Requires lightdm)
lightdm-gtk-greeter
lightdm-gtk-greeter-settings
```

**Compositor:**
```
picom
```
