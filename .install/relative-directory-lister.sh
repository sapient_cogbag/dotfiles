#!/usr/bin/bash
# Copyright (c) sapient_cogbag <sapient_cogbag@protonmail.com>
# 
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.

shopt -s nullglob
# globs for dot-prefixed files/dirs as well as non-dot-prefixed
# ones (not . or ..)
shopt -s dotglob

input_dir="${1:-$(pwd)}"
# Manual tilde expansion
lnname_file="$input_dir/.link_names"
# Newline
IFS='
'
files=()
if [[ -r "$lnname_file" ]]; then
    # Change our working directory to the input directory!
    for g in $(cat "$lnname_file"); do
        files+=("$g")
    done
else
    echo "No '$lnname_file' in the directory '$input_dir'"
    exit 1
fi


# iterates over output lines via IFS
for fdname in "$input_dir"/*; do
    for allowed_fname in "${files[@]}"; do
        if [[ "$input_dir/$allowed_fname" == "$fdname" ]]; then
            echo "$fdname"
            break
        fi
    done
done
