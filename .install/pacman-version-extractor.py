#!/usr/bin/env python
# 
# Copyright (c) 2020 sapient_cogbag <sapient_cogbag@protonmail.com>
# 
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.

import subprocess
import sys
import string

helpstr = """
Print the version of an installed package with `pacman`. Simply pass all the
packages you want to get the versions of and be done ^.^

This is licensed under the GNU All Permissive License
"""

if len(sys.argv) > 1:
    pkgs = sys.argv[1:]
    for p in pkgs:
        output = subprocess.run(["pacman", "-Qi", p], capture_output=True)
        if output.returncode != 0:
            print("~")
        else:
            ostring = str(output.stdout, encoding="utf8")
            lines = ostring.splitlines()
            for line in lines:
                if line.startswith("Version"):
                    version_splices = line.lstrip("vVersion" + string.whitespace).strip(":" + string.whitespace)
                    # Actual versions do not contain -, those come from the pkgbuild I think,
                    # so we strip any hyphens off
                    version = version_splices.split('-')[0]
                    print(version)
                    break

else:
    print(helpstr)
    sys.exit(1)
