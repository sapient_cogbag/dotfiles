***Packages for LaTeX***

\>pkglist:
```
texlive-core  # Core texlive distribution stuff, contains xetex for modern font usage
texlive-most  # most others.
biber  # Some citation thing I think
```

*Nice editor from KDE (they really do make everything huh):*
\>pkglist:
```
kile
```
