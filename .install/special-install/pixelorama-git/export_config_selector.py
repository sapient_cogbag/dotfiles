#!/usr/bin/env /usr/bin/python3

# Forces export_config to only contain the single thing we care about ~ Linux/X11 on x86_64, setting that to default nyaa
# This should be more resilient than manually maintaining a file ^.^ nya
import re
import sys
# Matches as much total whitespace around the name both inside and outside the brackets as possible nyaa
# \s is whitespace, \S is nonwhitespace nya
section_regex = re.compile(r"\s*\[\s*(?P<section_name>.*?)\s*\]\s*$")
keyvalue_regex = re.compile(r"\s*(?P<key>\S*?)\s*=\s*(?P<value>.*?)\s*$")

class Section(object):
    def __init__(self):
        # key => value (in the ini section nya)
        self._entries = dict()

    def add_entry(self, key, value):
        """
        Adds (or overwrites) an entry ;p nyaa
        """
        self._entries[key] = value

    def entries(self):
        return self._entries 

    def has_key(self, key):
        return key in self._entries

    def __getitem__(self, key):
        return self._entries.__getitem__(key)

    def dump(self, title):
        """
        Dump the section as lines with the given title nyaa 
        """
        r = "[" + title + "]\n"
        for k, v in self._entries.items():
            r += str(k) + "=" + str(v) + '\n'
        r += '\n'
        return r


class ProcessingStates(object):
    """
    Holds a state that is modified as you run through the file.
    """
    def __init__(self):
        # section name -> section
        self._sections = dict()
        self._current_section_name = None

    def process_line(self, line):
        """
        Process a line of input
        Errors are ignored ;p nya
        """
        kv_match = keyvalue_regex.match(line)
        if kv_match:
            if self._current_section_name == None:
                print("no section for : '" + line + "'", file=sys.stderr)
                return
            key = kv_match['key']
            value = kv_match['value']
            self._sections[self._current_section_name].add_entry(key, value)
            #print("read '" + key + "' with value '" + value + "'", file=sys.stderr)
            return
        section_match = section_regex.match(line)
        if section_match:
            sname = section_match['section_name']
            self._current_section_name = sname
            if sname not in self._sections:
                self._sections[sname] = Section()

    def get_sections_with_kvs(self, criteria):
        """
        Find all sections with the given keys holding the given values

        Returns a list of sections with the key and value given in the criterion
        if the value is a compiled regex it will match on that ;p (with.match, not .search)
        nya
        """
        result = []
        for name, contents in self._sections.items():
            valid = True
            for keyname, value_criterion in criteria.items():
                valid = valid and contents.has_key(keyname)
                # currently did not have the key 
                if not valid:
                    break
                val = contents[keyname]
                matches = (value_criterion == val) if type(value_criterion) == str else value_criterion.match(val)
                valid = valid and matches
                if not valid:
                    break

            if valid:
                result.append(name)
        return result

    def get_sections_starting_with(self, prefix):
        """
        Get all the section names starting with the prefix nyaa
        """
        return [s for s in self._sections.keys() if s.startswith(prefix)]

    def select(self, allowed_names):
        """
        Remove all sections that aren't in the allowed list of names nyaa.
        """
        allowed_nms = set(allowed_names)
        curr_names = set(self._sections.keys())
        
        for n in curr_names:
            if n not in allowed_nms:
                del self._sections[n]

    def section_name_transform(self, transform):
        """
        transforms the names of sections here according to a function
        that takes a string and outputs another string ;p
        Note that if two things make the same name the order isn't well
        defined as to which gets to exist ;p nya
        """
        old_names = set(self._sections.keys())

        for old_name in old_names:
            old_section = self._sections[old_name]
            del self._sections[old_name]
            transformed_name = transform(old_name)
            self._sections[transformed_name] = old_section

    def dumpstr(self):
        """
        Dump out the transformed parser state into a string nya
        """
        result = ""
        
        for name, section in self._sections.items():
            result += section.dump(name)

        return result

    def __getitem__(self, key):
        return self._sections[key]


def parse(raw_text):
    """
    Turn raw text into a ProcessingStates 
    """
    inistate = ProcessingStates()
    for line in raw_text.split('\n'):
        inistate.process_line(line)
    return inistate

# Default settings if we find no Linux/X11 presets (this shouldn't happen at all nya)
# taken from: https://github.com/Orama-Interactive/Pixelorama/blob/master/export_presets.cfg

default_sections = parse("""
[preset.0]

name="Linux/X11 64-bit"
platform="Linux/X11"
runnable=true
custom_features=""
export_filter="all_resources"
include_filter=""
exclude_filter=""
export_path=""
patch_list=PoolStringArray(  )
script_export_mode=1
script_encryption_key=""

[preset.0.options]

texture_format/bptc=false
texture_format/s3tc=true
texture_format/etc=false
texture_format/etc2=false
texture_format/no_bptc_fallbacks=true
binary_format/64_bits=true
binary_format/embed_pck=true
custom_template/release=""
custom_template/debug=""
""")

def list_prefixed(check, prefix):
    """
    check if one list is prefixed by another nyaa
    """
    if len(prefix) > len(check):
        return False
    for i in range(len(prefix)):
        if check[i] != prefix[i]:
            return False
    return True
            
def find_prefixed_by(checks, potential_prefixes):
    """
    Search through the given lists and see if they're
    prefixed by something nya

    returns a list of pairs of (prefix, list)
    """
    result = []
    for to_check in checks:
        for prefix in potential_prefixes:
            if list_prefixed(to_check, prefix):
                result.append((prefix, to_check))
    return result


def locate_linux_64bit_option(inifile):
    """
    Returns a list of section name pairs (preset.<num>, preset.<num>.options) 
    which are for linux 64 bit nyaa
    """

    # these are lists of section names split by their paths.
    linux_presets = [s.split('.') for s in inifile.get_sections_with_kvs({
        "platform": re.compile('".*Linux/X11.*"')
    })]

    b_64bit_option_presets = [s.split('.') for s in inifile.get_sections_with_kvs({
        "binary_format/64_bits": "true"
    })]

    # Locate the 64 bit preset by checking the components and finding 
    # which names match in the first bits nya
    matches = find_prefixed_by(b_64bit_option_presets, linux_presets)
    
    # Rejoin the sections ;p nyaa
    return [
        ('.'.join(main_preset), '.'.join(options)) for (main_preset, options) in matches
    ]




if __name__ == "__main__":
    inifile = ProcessingStates()
    for line in sys.stdin:
        inifile.process_line(line)

    linux64bitpresets = locate_linux_64bit_option(inifile)
    # If we don't have it, dump out the default nyaa
    if len(linux64bitpresets) == 0:
        sys.stdout.write(default_sections.dumpstr())
    else:
        # grab the first one nya
        main, options = linux64bitpresets[0] 
        # remove everything else ;p nya
        inifile.select([main, options])
        
        def transform(old):
            return old.replace(main, "preset.0")
        
        inifile.section_name_transform(transform)
        options = transform(options)
        main = transform(main)

        inifile[options].add_entry("binary_format/embed_pck", "true")
        sys.stdout.write(inifile.dumpstr())
    

