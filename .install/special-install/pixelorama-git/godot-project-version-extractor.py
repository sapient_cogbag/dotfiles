#!/usr/bin/env python3
# 
# Copyright (c) 2020 sapient_cogbag <sapient_cogbag at protonmail dot com>
# 
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.

import subprocess
import sys
import string
import re

helpstr = """
Print the version of a godot project file passed on the command line.
This does not perform any modification of the version, per arch-linux
packaging guidelines (https://wiki.archlinux.org/index.php/Arch_package_guidelines)

This is licensed under the GNU All Permissive License
"""


if len(sys.argv) == 2:
    pkg = sys.argv[1]
    with open(pkg, 'rt') as f:
        fcontents = f.readlines()
        for ln in fcontents:
            ln = ln.strip()
            if ln.startswith("config/Version"):
                ln = ln[len("config/Version"):].strip()
            else:
                continue
            if ln.startswith("="):
                ln = ln[len("="):].strip()
            else:
                continue
            if ln.startswith('"') and ln.endswith('"'):
                ln = ln[1:-1]
                print(ln)
                sys.exit()
            else:
                continue



else:
    print(helpstr)
    sys.exit(1)
