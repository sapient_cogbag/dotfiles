#!/usr/bin/sh
cd /var/lib/pretendyourexyzzy/pretendyourexyzzy
source /etc/pretendyourexyzzy.conf.sh
mvn -Djetty.port=$PYX_PORT jetty:run -Dhttps.protocols=TLSv1.2 -Dmaven.buildNumber.doCheck=false -Dmaven.buildNumber.doUpdate=false -Dmaven.test.skip=true 
