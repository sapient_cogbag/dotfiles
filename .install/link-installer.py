#!/usr/bin/env python
# Copyright (c) 2020 sapient_cogbag <sapient_cogbag@protonmail.com>
# 
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.

import os
import os.path
import sys
import subprocess

# Holds (
#   ((directory, install location)... pairs), 
#   dependencies
# ) for each module.
# formatting provides {repo} in the from-install directory and 
# {home} and {cfg_dir} and {repo} in the output directory
modules = {
    "home": (
        (("{repo}/.home", "{home}"), ("{repo}/.home/.ssh", "{home}/.ssh")), 
        tuple()
    ),
    "powerline": (
        (("{repo}/powerline", "{cfg_dir}/powerline"),), 
        ("home",)
    ),
    "kitty": (
        (("{repo}/kitty", "{cfg_dir}/kitty"),),
        tuple()
    )
}

# Special grouping module
modules["all"] = (
        tuple(), 
        tuple(k for k in modules.keys())
)

program_path = sys.argv[0]

helpstr="""\
USAGE: link-installer.py [--list] [modules]... 

Installs the given modules into $HOME and $XDG_CONFIG_HOME ($HOME/.config
if undefined) from this module, from the parent git repository of
this program, by linking the output locations to locations within this
repository.

This program will refuse to do the install process if it will involve
writing to a configuration directory that is the same as the repository
directory - it will NOT refuse to install just to the $HOME directory.

This program depends on the file .install/relative-directory-linker.sh 
in the repository.

Dependencies between modules are resolved automatically. 

Modules available are:\n
""" + ''.join(('   * ' + k + '\n' for k in modules.keys())) + """
Licensed under the GNU All-Permissive License"
Copyright (c) 2020 sapient_cogbag <sapient_cogbag@protonmail.com>\
"""


def locate_repository_root(argv):
    """Attempt to locate repository root folder."""

    # We try to locate root by finding a .git folder.
    # if not found then we spit out a None
    our_location = os.path.normpath(os.path.abspath(argv[0]))
    rest, end = os.path.split(our_location)
    while rest not in {"", "/", "/.", "./"}:
        git_dir_path = os.path.join(rest, ".git")
        is_repo_root = os.path.exists(git_dir_path) and os.path.isdir(git_dir_path)
        if is_repo_root:
            return rest
        rest, end = os.path.split(rest)

def resolve_module_deps(selected_modules, already_resolved=None):
    """
    Resolves the given set of modules into a proper
    set. Allows you to provide modules to mark as "already resolved"
    These will be automatically added to the returned dependencies.

    returns a pair of (module set, unmatched modules)

    If there is a circular dependency, the ordering is undefined
    """
    unmatched = set()
    all_deps = set()
    preresolved = set() if already_resolved is None else already_resolved
    
    for mod in selected_modules:
        # nonexistent modues get spat out
        if mod not in modules.keys():
            unmatched.add(mod)
            continue
        # add existent modules to all dependencies.
        all_deps.add(mod)
        if mod in preresolved:
            continue
        # Get all dependencies of the dependencies, as well as other unresolved modules
        # in those dependencies (nonexistent dependencies nya)
        dependencies, unresed = resolve_module_deps(
                modules[mod][1], # dependencies of the current module
                preresolved | all_deps  # prevent Stack Overflow from circular deps
            
        )
        # Add the found dependencies to all the desired modules nya
        all_deps.update(dependencies)
        # find all nonexistent dependency modules and add them to
        # the list of nonexistence nya.
        unmatched.update(unresed)

    return all_deps, unmatched


def get_in_repo_dir_formatmap(home, cfg_dir, repo_directory):
    """
    Get the formatting map for the "source" parts of module directories.
    """
    return {"repo": repo_directory}

def get_outloc_dir_formatmap(home, cfg_dir, repo_directory):
    """
    Get the formatting map for the destination directories for modules.
    """
    return {"repo": repo_directory, "home": home, "cfg_dir": cfg_dir}


def install_module(modname, home, cfg_dir, repo):
    """
    Install the given module.
    """
    linking_script = os.path.join("{repo}".format(repo=repo), ".install/relative-directory-linker.sh")

    # Get the formatting maps.
    formatter_source = get_in_repo_dir_formatmap(home, cfg_dir, repo)
    formatter_output = get_outloc_dir_formatmap(home, cfg_dir, repo)
    
    # get the source, output directory pairs:
    pairs = modules[modname][0]

    for source_directory, end_directory in pairs:
        # these are the actual source and output directories.
        actual_src = os.path.abspath(source_directory.format_map(formatter_source))
        actual_out = os.path.abspath(end_directory.format_map(formatter_output))
        # Make sure the directories actually exist :)
        os.makedirs(actual_out, exist_ok=True)
        
        subprocess.run(["bash", linking_script, actual_out, actual_src])

def writes_to_config_output(module_name):
    """
    Checks if the given module (without checking deps) writes to the output
    config directory.
    """
    # Pull output directories.
    # ':' can't be in a real directory name so test nya
    # set home as just "~"
    odirs = (x[1] for x in modules[module_name][0])
    return any(':' in odir.format(cfg_dir=':', home="~") for odir in odirs)


def main(argv):
    if len(argv) < 2:
        print(helpstr)
        sys.exit(1)

    # Get environment vars.
    home = os.getenv("HOME")
    if home is None:
        print("No $HOME????")
        sys.exit(2)

    cfg_dir = os.path.normpath(os.path.abspath(os.getenv("XDG_CONFIG_HOME", os.path.join(home, ".config"))))



    selected_modules = set(argv[1:])
    
    # plain mode means actual install, list mode means only list all dependencies
    mode = "plain"
    if "--list" in selected_modules:
        selected_modules.remove("--list")
        mode="list"



    repo_directory = locate_repository_root(argv)
    if repo_directory is None:
        print("Could not locate parent repository. Our path (sys.argv[0]) is: {}".format(argv[0]), file=sys.stderr)
        sys.exit(2)

    # Normalise the repo directory
    repo_directory = os.path.normpath(os.path.abspath(repo_directory))

    dependencies, unresolved = resolve_module_deps(selected_modules)

    # Check we aren't gonna do weirdness and link to ourselves
    if repo_directory == cfg_dir and mode != "list" and any(writes_to_config_output(mname) for mname in dependencies):
        print("Error: Both the config and git repo directory resolved to {} - not creating links to ourselves".format(repo_directory))
        sys.exit(2)

    print("Located repository directory '{}'".format(repo_directory))
    print("Located config directory {}".format(cfg_dir)) 


    print("Found module dependencies: \n" + ''.join(('   * ' + d + '\n' for d in dependencies)))

    if len(unresolved) > 0:
        print("Error: Unresolved modules: \n" + ''.join(('   * ' + d + '\n' for d in unresolved)))
        sys.exit(2)

    if mode == "list":
        return

    # Goes through and installs the modules!
    for module in dependencies:
        install_module(module, home, cfg_dir, repo_directory)


if __name__ == "__main__":
    main(sys.argv)

