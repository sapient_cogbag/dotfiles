# Terminal Theming and Related (e.g. vim theming)

ruby-term-ansicolor gives us term\_colortab which we alias to colours

\>pkglist:
```
python-powerline-git
ruby-term-ansicolor
```


\>script:
```
# powerline daemon service :p
systemctl --user --now enable powerline-daemon
```
