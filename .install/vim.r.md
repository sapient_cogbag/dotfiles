***Vim Plugin Segment***

\>pkglist:
```
vim-lsp-git  # Language Server stuff.
vim-asyncomplete-git  # Same for above, autocomplete :) nya
vim-asyncomplete-lsp-git  # links the above two ^.^

vim-latexsuite
vim-pandoc-git
vim-pandoc-syntax-git

clang # :3 ~ clang completion which works with rainbow highlighting nya
fzf  # nya ~ this actually provides a plugin which provides the vim :FZF command nya
```



\>script:
```
PKGNAME=vim-lsp-cxx-highlight-git ./special-installer.sh
PKGNAME=vim-semantic-highlight-git ./special-installer.sh
``` 

