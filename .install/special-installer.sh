#!/usr/bin/sh
# Installs the package with a PKGBUILD in $XDG_CONFIG_HOME/.install/special-install
# under the directory with the package name as it's name, by copying
# all the files in a working directory and then running makepkg -si
# The package name is specified with the environment variable PKGNAME


PKGNAME=${PKGNAME:-"#"}

# Uses the /var/makepkg-build/$USER directory for git cloning and makepkging in

SRC_BASEDIR="/var/makepkg-build/$USER"
# The directory we hold as "current" (it isn't really but 
# in all sensible installs it is, running this script outside the
# install will still work though

CURRDIR="${XDG_CONFIG_HOME:-$HOME/.config}/.install/"

PKGBUILD_DATA_LOC="${CURRDIR}/special-install/${PKGNAME}/"
PKGBUILD_WORKING_LOC="$SRC_BASEDIR/${PKGNAME}"

# Make our location WORK nyaaaaaaaa
mkdir -p "${PKGBUILD_WORKING_LOC}"

cp "${PKGBUILD_DATA_LOC}"/* "${PKGBUILD_WORKING_LOC}"
cd "${PKGBUILD_WORKING_LOC}"

makepkg -si
