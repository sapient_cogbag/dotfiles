# USBGuard RMD file


TODO: Auto-Generate initial USB rules to allow preexisting devices on the machine ^.^

\>pkglist:
```
usbguard
```


(no current package) https://github.com/Cropi/usbguard-notifier - prefer below, because it has better functionality.

Use hacked together version of the applet for newer libusbguard versions HERE and recompile on new versions:


\>script:
```
PKGNAME=usbguard-applet-qt-git ./special-installer.sh
```

For USBGuard IPC, the IPCAllowX is not sufficient anymore
One needs to create a file in the IPC permission folder in /etc/usbguard/ with name <username>[:<group>] 
with the contents:


\>file:/etc/usbguard/IPCAccessControl.d/$USER
```
Devices=modify list listen
Policy=modify list
Exceptions=listen
Parameters=modify list
```

Specifying full permissions (see `man usbguard-daemon.conf`), or, of course, a subset of them.

TODO: Also need to add the capabilities "CAP\_DAC\_READ\_SEARCH CAP\_DAC\_OVERRIDE"
to CapabilityBoundingSet in the systemd service file at /etc/systemd/system/basic..../usbguard.service
See https://github.com/USBGuard/usbguard/issues/287 for info

Also note there may be a fix: https://github.com/USBGuard/usbguard/issues/289 since the DAC OVERRIDE capability is
apparently kinda dangerous nya

