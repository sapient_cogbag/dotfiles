# Networky-Stuff


**NetworkManager:**
\>pkglist:
```
networkmanager

# Mobile stuff
modemmanager
mobile-broadband-provider-info

# Connection Sharing
dnsmasq

# For generic usb devices and related switching info:
usb_modeswitch

# For OpenNIC: https://wiki.opennic.org/start - DNS not
# controlled by a central ICANN agency - this automatically
# updates a list of DNS servers to use from the list of
# OpenNIC servers: https://github.com/kewlfft/opennic-up

opennic-up
```

\>script:
```
sudo systemctl enable --now NetworkManager.service
sudo systemctl enable --now ModemManager.service
sudo systemctl enable --now opennic-up.timer
```




**Applet for systray + conn editor:**


\>pkglist:
```
network-manager-applet
nm-connection-editor
```


**Network Sniffing and Similar:**


\>pkglist:
```
wireshark
```

Sets up the system controls/security restrictions of stuff...

\>script:
```bash
# -> see https://wiki.wireshark.org/CaptureSetup/CapturePrivileges for making a 
# group -s is -r in wireshark but this might be added automatically by `pacman`

# add user to wireshark group
sudo gpasswd -a $USER wireshark
# Auto-relogin with new added group capabilities. Allows following without reboot. Creates a new inner shell so we probably
# have to put in a little hack in the parser for this hybrid file format to catch this case.
newgrp wireshark
# set the group of dumpcap, this also resets security bits or suid.
sudo chgrp wireshark /usr/sbin/dumpcap
# Add security capabilities
sudo setcap cap_net_raw,cap_net_admin+eip /usr/sbin/dumpcap
```

```.gnupg/gpg-agent.conf
enable-ssh-support
ssh-fingerprint-digest sha256
```
