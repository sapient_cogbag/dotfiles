# GTK/Qt theming rmd script

See https://wiki.archlinux.org/index.php/Uniform_look_for_Qt_and_GTK_applications 

\>pkglist:
```
qt5-styleplugins  # (for gtk2 -> qt5 theme application)
qt6gtk2 # (for gtk2 -> qt6 theme application)
```


Long term solution is making qt package for my customised Arc
