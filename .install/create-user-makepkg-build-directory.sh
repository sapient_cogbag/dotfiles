#!/usr/bin/sh
sudo mkdir -p "/var/makepkg-build/$USER"
# Change the group
sudo chgrp $USER "/var/makepkg-build/$USER"
# Add groups of writing and make permissions sticky for
# group permissions on subdirs/files
sudo chmod g+ws "/var/makepkg-build/$USER"
# Set ACLs for all subdirs and subfiles to ensure group rwx
sudo setfacl -m u::rwx,g::rwx "/var/makepkg-build/$USER"
sudo setfacl -d --set u::rwx,g::rwx,o::- "/var/makepkg-build/$USER"
