***Language servers for Stuff (mostly vim)***

\>pkglist:
```
ghdl
cmake-language-server-git
rust-analyzer-git
vim-language-server
```

Installing the VHDL language server is complex
- it does not have a proper package in either the
AUR or `pip`. Therefore we just have to run the install
methodology from [the git repo](https://github.com/ghdl/ghdl-language-server)
Note that we build the python library seperately - this means we do not need 
a special install script to add it, instead just downloading it from git and
using that, only with our PKGBUILD


Therefore we use the scripts in this directory to do it.
\>script:
```
PKGNAME=python-libghdl-py-git ./special-installer.sh
PKGNAME=python-ghdl-ls-git ./special-installer.sh
```
