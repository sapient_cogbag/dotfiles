***Makepkg non-root configuration***

This file creates a build directory in /var/makepkg-build/$USER for running
makepkg without an AUR helper as user `$USER`, following the guide 
[here](http://allanmcrae.com/2015/01/replacing-makepkg-asroot/)

This creates a `/var/makepkg-build/$USER` directory controlled by user `$USER`
That way sudo works fine with makepkg rather than making a security vulnerability
by allowing the nobody user to run makepkg, and also it does not require any
more complex configuration of the sudo config file

\>script:
```
./create-user-makepkg-build-directory.sh
```
