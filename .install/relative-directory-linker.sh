#!/usr/bin/bash
# Copyright (c) 2020 sapient_cogbag <sapient_cogbag@protonmail.com>
# 
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.

shopt -s nullglob
# globs for dot-prefixed files/dirs as well as non-dot-prefixed
# ones (not . or ..)
shopt -s dotglob

if [[ $# -lt 1 ]]; then
    echo "No arguments provided. Need at least a destination directory."
    echo "Usage:"
    echo " <destination-directory> [source-directory: defaults to curr directory]"
    echo ""
    echo " Looks for a .link_names file in the source directory. Each line in"
    echo " that file is a name for a file/directory to link to a file of the"
    echo " same name in the destination directory. This will delete anything"
    echo " preexisting."
    echo ""
    echo "Licensed under the GNU All-Permissive License"
    echo "Copyright (c) 2020 sapient_cogbag <sapient_cogbag@protonmail.com>"
    exit 1
fi

output_dir="${1:-$HOME}"
input_dir="${2:-$(pwd)}"
# Manual tilde expansion
lnname_file="$input_dir/.link_names"
# Newline
IFS='
'
files=()
if [[ -r "$lnname_file" ]]; then
    # Change our working directory to the input directory!
    for g in $(cat "$lnname_file"); do
        files+=("$g")
    done
else
    echo "No '$lnname_file' in the directory '$input_dir'"
    exit 1
fi


# iterates over output lines via IFS

# PROBLEM - this is not *recursive* and hence does not manage inner directories :/
# We must manually manage this for now until we make a real rust implementation of all this gnarly install shit nya
# TODO: MAKE EVERYTHING RUST!!!
for fdname in "$input_dir"/*; do
    for allowed_fname in "${files[@]}"; do
        if [[ "$input_dir/$allowed_fname" == "$fdname" ]]; then
            echo "Directory-Content-Linking from '$fdname' to '$output_dir/$allowed_fname'"
            rm -rf "$output_dir/$allowed_fname"
            ln -srf "$fdname" "$output_dir/$allowed_fname"
            break
        fi
    done
done
