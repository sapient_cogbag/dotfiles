# My Dotfiles - This file is a combo doc/instruction file for installs. Full specification at some point:

Before running this file you should:

* Read https://wiki.archlinux.org/index.php/Installation_guide to understand what we're gonna do (roughly)

* Ensure network interface is available with `ip link`, also good for bringing wireless interfaces up if needed.

* The Arch ISO has the following programs for connecting to wireless cards: `iw`, `iwconfig`, `wpa_supplicant` (has wpa/wpa2)
  I think, and `iwd` (also has wpa/wpa2). Check for and if present start `networkmanager` service (`NetworkManager.service`)

* Create your partition layout (UEFI+GPT, please, none of that MBR nonsense) - remember to make an EFI PARTITION with filesystem 
  FAT32, and with size 200MiB - 1GB (I usually use 1GB because of my tendency to forget to uninstall kernel versions which is actually
  good in case of weird kernel driver errors or whatever) - `parted`, `gdisk` and `fdisk` are good for this, and the partition should have 
  flag "esp".

* Have a look at crypto swap :p - https://wiki.archlinux.org/index.php/Dm-crypt/Swap_encryption#With_suspend-to-disk_support

* Load the partition layout to /mnt, with the EFI System Partition mounted to /boot (this lets `pacman` do it's thing :p)

* Run the `pre-pacstrap-bootstrap.sh` script in `.install/bootstrap`. This will do `pacstrap` with some useful packages and also make it easy to clone
  the dotfiles repo to the new install before chrooting.

* Chroot into the new system and run the `pre-user-bootstrap.sh` file. This will result in you creating your user.


*Microcode:*

Linux is adaptable, and we can install both `intel-ucode` and `amd-ucode` and it will work automatically 
 [archwiki-page](https://wiki.archlinux.org/index.php/Microcode)


\>pkglist:
```
intel-ucode
amd-ucode
```

`systemctl enable --now ipfs@username.service`

*General Packages:*

TODO: Laptop + Power Management - https://wiki.archlinux.org/index.php/Laptop

\>pkglist:
```
tinyserial  # (for serial port communication. Good for reverse-engineering/unlocking own devices/etc.) 
minicom  # (for above)
ssterm  # Easy COM terminal, works better than the above
geoclue
redshift
redshift-qt
vim
python-dbus
collectd
viewnior
bluez-utils  # (utilities for messing around with bluetooth)
informant  # Pacman hooks to notify of arch linux news
```

Add the $USER to the informant group so they don't need sudo :)
\>script:
```
sudo groupadd -a -G informant $USER
```


This creates a `/var/makepkg-build/$USER` directory controlled by user `$USER`
That way sudo works fine with makepkg rather than making a security vulnerability
by allowing the nobody user to run makepkg, and also it does not require any
more complex configuration of the sudo config file

\>rmd:.install/makepkg-build-directory-preconfigure.r.md

\>rmd:.install/networking-things.r.md

\>rmd:.install/theming.r.md

\>rmd:.install/term-theming.r.md

\>rmd:.install/usbguard.r.md

\>rmd:.install/awesomewm.r.md

\>rmd:.install/documents.r.md

\>rmd:.install/gnuradio.r.md

\>rmd:.install/pwmanager.r.md

\>rmd:.install/desktop.r.md

\>rmd:.install/latex.r.md

\>rmd:.install/vhdl.r.md

\>rmd:.install/vim.r.md

\>rmd:.install/language-servers.r.md

\>rmd:.install/artsystuff.r.md

Installs user services (stored in \<cfgdir\>/systemd/user) :)
This specifically regularly updates the peer blocklist AND runs it on user session start nyyaaa
(since the service is oneshot ;p

\>sysdusr:torrent-blocklist-nightly-downloader.timer
\>sysdusr:torrent-blocklist-nightly-downloader.service

*Firefox Colour Theme*
See the file .info/firefox-colour for the firefox colour theme url

*DNS over HTTPS:*
See: https://support.mozilla.org/en-US/kb/firefox-dns-over-https to enable
we may want to investigate using this with that one decentralised alt-DNS system nyaa

**TODOs:**

*SELinux:*

https://wiki.archlinux.org/index.php/SELinux
http://www.fosteringlinux.com/tag/selinux/
