#!/usr/bin/env python3
import tempfile
import os
import subprocess
import os.path
import time


home = os.getenv("HOME", None)
if home is None:
    raise Exception("No home environment variable")


def XDG():
    """
    Grab a dictionary containing the appropriate values for the XDG standard
    see: https://wiki.archlinux.org/index.php/XDG_Base_Directory

    Things which are lists, are, well, lists.
    Things with defaults have those defaults set appropriately nyaa

    note that this does NOT include XDG_RUNTIME_DIR because it requires
    system specific defaults, though we may well find this is relatively
    standardised and use that instead nya

    Also sets HOME for convenience
    """
    home = os.getenv("HOME")

    xdg_config_home = os.getenv("XDG_CONFIG_HOME", os.path.join(home, ".config"))
    xdg_cache_home = os.getenv("XDG_CACHE_HOME", os.path.join(home, ".cache"))
    xdg_data_home = os.getenv("XDG_DATA_HOME", os.path.join(home, ".local", "share"))

    # system dirs nyaa
    xdg_data_dirs = os.getenv("XDG_DATA_DIRS", "".join([
        os.path.join("/", "usr", "local", "share"),
        ":",
        os.path.join("/", "usr", "share")
        ]
    ))
    xdg_config_dirs = os.getenv("XDG_CONFIG_DIRS", os.path.join("/", "etc", "xdg"))

    return {
        "HOME": home,
        "XDG_CONFIG_HOME": xdg_config_home,
        "XDG_CACHE_HOME": xdg_cache_home,
        "XDG_DATA_HOME": xdg_data_home,
        "XDG_DATA_DIRS": xdg_data_dirs.split(":"),
        "XDG_CONFIG_DIRS" : xdg_config_dirs.split(":")
    }

xdg = XDG()


# Note: REQUIRES WGET AND gunzip
# The blocklist tar URL nyaa
# https://github.com/DavidMoore/ipfilter/issues/64
# nyaa, see for other blocklists on:
# from https://wiki.manjaro.org/index.php?title=Block_Lists_for_Deluge_%26_qBittorrent
# even if we aren't using Deluge nyaaaa

compressed_file_url = "https://github.com/DavidMoore/ipfilter/releases/download/lists/ipfilter.dat.gz"

DAY = 24 * 60 * 60

# TODO: Make a qt/ini config file state setter nyaa
# that doesn't bork up comments, so this is done properly.
# rather than essentially being a hack
# these are formatted with xdg
ipblock_locations = {
    'qbittorrent': "{HOME}/ipfilter.dat"
}

def exponential_backoff(callable_fn, max_period = DAY):
    """
    Note: not a super accurate implementation, 
    THis will also die if the exponential backoff
    gets longer that half the max period.
    """
    period = 5

    while period < max_period/2:
        try:
            callable_fn()
            return
        except Exception:
            # nyaa
            period = period * 2
            print("Error ~ exponential backoff period ~ {}".format(period))
            time.sleep(period)
            
    print("Could not perform task within sufficiently close exponential backoff")
    raise Exception("Error: Exponential backoff failed to successfully complete task nyaa")


 


with tempfile.TemporaryDirectory() as temporary:
    def task():
        subprocess.run(["wget", '-P', temporary, compressed_file_url], check=True) 
        subprocess.run(["gunzip", os.path.join(temporary, 'ipfilter.dat.gz')], check=True)
    exponential_backoff(task)
    unzipped_location = os.path.join(temporary, "ipfilter.dat")
    for application, location in ipblock_locations.items():
        localised_location = location.format_map(xdg)
        print("Copying block file for application '{}', to '{}'. nya".format(application, localised_location))
        subprocess.run(["cp", "-v", unzipped_location, localised_location], check=True)

