--[[--
  Use this file to specify **User** preferences.
  Review [examples](+/opt/zbstudio/cfg/user-sample.lua) or check [online documentation](http://studio.zerobrane.com/documentation.html) for details.
--]]--

-- to have 4 spaces when TAB is used in the editor
editor.tabwidth = 4

-- to disable wrapping of long lines in the editor
editor.usewrap = false

-- to specify a default EOL encoding to be used for new files:
-- `wxstc.wxSTC_EOL_CRLF` or `wxstc.wxSTC_EOL_LF`;
-- `nil` means OS default: CRLF on Windows and LF on Linux/Unix and OSX.
-- (OSX had CRLF as a default until v0.36, which fixed it).
editor.defaulteol = wxstc.wxSTC_EOL_LF


local purple_text_bg = {19, 0, 44}
-- defaults
styles.fg = {255,255,255}
styles.bg = purple_text_bg

-- main text with purple background
styles.text = {bg=purple_text_bg, fg={255,255,255}}
-- selected line, slightly lighter purple
styles.caretlinebg={bg={60,0, 90}, fg={255,255,190}}

-- cursor
styles.caret = {fg={255,255,255}}

--comments
styles.comment = {fg={10, 150, 255}, i=true}

stringforeground = {50, 255, 50}

-- complete string
styles.stringtxt = {fg=stringforeground}

-- incomplete string
styles.stringeol = {fg={255,0,0}, fill=true, u=true}

orange = {255,147,0}

-- preprocessor (not sure what this is just make it orange lmao)
styles.preprocessor = {fg=orange}

--orange
styles.operator = {fg={180, 120, 0}}

-- pink numbers <3
styles.number = {fg={217,0,255}, i=true}

--linecounts: bg is lighter purple, fg is medium yellow
styles.linenumber = {fg={255, 255, 0},bg={51,0,103}}

--current-matched-braces (go bold when cursor is near).
-- We're making them cyaaaaaan <3
styles.bracematch = {fg={0, 77, 177}, b=true}

-- broken/unmatched braces when hovered - make red.
styles.bracemiss = {fg={190,0,0}, b=true}

-- Control characters (I think)
styles.ctrlchar = {fg=orange}

--[[
Not sure what exactly this is :(
set it to something sensible based off https://github.com/pkulchenko/ZeroBraneStudio/blob/master/cfg/tomorrow.lua
]]
styles.indent = {
  fg=styles.comment.fg,
  i=true
}

-- Selection
styles.sel = {bg = {78, 82, 154}}

-- The collapsible block thingies
styles.fold = {
  bg = purple_text_bg,
  fg = styles.number.fg, -- Pretty magenta
  sel = {255, 170, 0} -- Colour of the little droppable sections when selected - orangey gold
}

-- Dunno what these are
styles.whitespace = {}
styles.edge = {}

-- We will leave the markers alone apart from fixing output and prompt backgrounds and stuff
-- to change markers used in console and output windows (see user-sample_
styles.marker.message = {ch = wxstc.wxSTC_MARK_ARROWS, fg = {255,255,255}, bg = purple_text_bg}
styles.marker.output = {ch = wxstc.wxSTC_MARK_BACKGROUND, fg = {255,255,255}, bg = purple_text_bg}
styles.marker.prompt = {ch = wxstc.wxSTC_MARK_CHARACTER+('>'):byte(), fg = {255,255,255}, bg = purple_text_bg}
styles.marker.error = {ch = wxstc.wxSTC_MARK_BACKGROUND, fg= {255,255,255}, bg = {75,0,0}} -- dark red bg error


globvarcolour = {0,255,255} -- rich cyanish thing

-- Indicators for different stuff
-- styles.indicator.fncall = {st=wxstc.wxSTC_INDIC_BOX, fg = {255, 0, 255}} -- magenta

-- No extra effects than colouration for local and global variables
styles.indicator.varlocal = {st=wxstc.wxSTC_INDIC_TEXTFORE, fg = {255, 99, 255}} -- light pink

styles.indicator.varglobal = {st=wxstc.wxSTC_INDIC_TEXTFORE, fg = globvarcolour} -- bold only affects
-- "inbuilt" functions, which is good. 

-- shadowed variables are squiggleunderlined in blue
styles.indicator.varmasked = {st=wxstc.wxSTC_INDIC_SQUIGGLE, fg = {0, 0, 255}}
-- Those doing the masking are squiggleunderlined in red ^.^
styles.indicator.varmasking = {st=wxstc.wxSTC_INDIC_SQUIGGLE, fg = {255, 0, 0}}

-- Make self all pretty <3
styles.indicator.varself = {st=wxstc.wxSTC_INDIC_TEXTFORE, fg = {0, 255, 0}} -- Green!

b = 0

local function a()
    local b = {}
    b.a = b
end

a()

k={}
function k:a(alpha) 
    self.underline_alpha = alpha
end

-- to set compact fold that doesn't include empty lines after a block
editor.foldcompact = true

do
    -- Keyword styles and stuff
    -- TODO: figure out what 4-7 are for lol
    -- We could also vary colours for different keywords like in user-sample.lua

    --[[
    and       
    break     
    do        
    else   
    elseif    
    end -- Probably changes based on other one.    
    for       
    function  
    goto      
    if        
    in
    local           
    not       
    or        
    repeat    
    return
    then         
    until     
    while
    ]]
    styles.keywords0 = {fg={255,255,0}}

    -- false, nil, true, _G, _VERSION, _ENV
    styles.keywords1 = {fg={0,185,16}, b=true}

    --[[
    assert
    dofile
    collectgarbage
    error
    getmetatable
    ipairs
    load
    loadfile
    next
    pairs
    pcall
    print
    rawequal
    rawget
    rawlen
    rawset
    select
    setmetatable
    tonumber
    tostring
    type
    xpcall
    ]]
    styles.keywords2 = {fg={0,209,255}, b=true}

    --[[
    os.*
    io.*
    coroutine.*
    package.*
    string.*
    table.*
    math.*

    debug.*
    ]]
    styles.keywords3 = {fg=globvarcolour, i=true, b=true}


end

stylesoutshell = styles -- apply the same scheme to Output/Console windows
styles.auxwindow = styles.text -- apply text colors to auxiliary windows
styles.calltip = styles.text -- apply text colors to tooltips


-- Allow static analyser to infer values (luainspect)
staticanalyzer.infervalue = true