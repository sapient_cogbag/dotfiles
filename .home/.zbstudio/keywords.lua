-- Keywords utility file for testing themes

and       
break     
do        
else   
elseif    
end -- Probably changes based on other one.
false     
for       
function  
goto      
if        
in
local     
nil       
not       
or        
repeat    
return
then      
true      
until     
while

os.clock
io.write
coroutine.create
package.config
string.byte
table.concat
math.abs

debug.debug

utf8.codepoint

assert
dofile
collectgarbage
error
_G
getmetatable
ipairs
load
loadfile
next
pairs
pcall
print
rawequal
rawget
rawlen
rawset
select
setmetatable
tonumber
tostring
type
_VERSION
_ENV
xpcall
