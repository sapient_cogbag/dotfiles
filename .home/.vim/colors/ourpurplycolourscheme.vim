" Tell what type of background we have. A lot of the structure in here is
" inspired by the default theme nyaaa (which is very minimal, which is good, uwu) nyaa 
set background=dark

" clear out Concealed characters to not have special highlighting
highlight Conceal NONE


"""""""""
"{{{ MENUS:"
"""""""""

" non-selected thing nya. Make it dark purple and orange :3
highlight Pmenu cterm=NONE ctermbg=53 ctermfg=202 guibg=#4a0075 guifg=#ff8800
" selected thing
highlight PmenuSel cterm=bold ctermbg=54 ctermfg=33 guibg=#5f0087 guifg=#0088ff
"}}}

""""""""""""""""""""""""""""""
"{{{ GENERIC CODE STUFF ^.^ nya:"
""""""""""""""""""""""""""""""

" Identifiers (vars etc when no rainbow highlights, at least until we get rainbow highlighting,
" lets try clearing it :3)
highlight Identifier cterm=NONE ctermbg=NONE ctermfg=198 guifg=#ff0087

" Keywords and Statements (orangy-yellow and bold nyaa)
highlight Statement cterm=bold ctermbg=NONE ctermfg=208 guifg=#ff8700

" Strings (simple green uwu)
highlight String cterm=NONE ctermbg=NONE ctermfg=34 guifg=#00af00

" Operators
hi Operator cterm=bold ctermbg=NONE ctermfg=196 guifg=#ff0000

" Preprocessing/includes/python imports - similar to statements
hi PreProc cterm=NONE ctermbg=NONE ctermfg=166 guifg=#ff4800

" Function names
hi Function cterm=NONE ctermfg=49 guifg=#00ffaf

" Identifier and Function modifiers (annotations and similar) nyaa
hi Annotation cterm=NONE ctermfg=190 guifg=#d7ff00

" Comments
hi Comment cterm=NONE ctermfg=13 guifg=#ff00ff

" Change types
hi Type NONE
hi Type ctermfg=48 guifg=#00ffaf

" static constexpr like stuff :3 nyaa
hi StorageClass NONE
hi StorageClass cterm=bold ctermfg=11 guifg=#ffff00

"}}}


""""
" {{{ Sign columns, folds, and other such theming nya ^.^
""""
hi SignColumn cterm=bold ctermfg=206 ctermbg=17 guibg=#270027 guifg=NONE 

hi FoldedCombo NONE
hi FoldedCombo cterm=italic ctermfg=32 ctermbg=18 guibg=#200062 guifg=#0087d7

" Main buffer folding :3
hi Folded NONE
hi link Folded FoldedCombo

" in the sign column (not the numbers)
hi FoldColumn NONE
"hi link FoldColumn FoldedCombo
hi FoldColumn cterm=bold ctermbg=17 guibg=#270027 

"}}}


""""
" {{{Python stuff:
""""

" make decorator (the @ part, not name nyaaa) an operator not the same colour
" as #defines:
hi link pythonDecorator Operator
" also for the matrix multiply
hi link pythonMatrixMultiply Operator
" Decorator names different to normal function names, pale yellow nyaaa <3
hi link pythonDecoratorName Annotation
"}}}


""""
" {{{Vim stuff:
""""
hi vimHiTerm ctermfg=190 guifg=#d7ff00

" These are the group bits in these colour specifiers nyaa
hi link vimHiGroup Identifier
hi link vimGroup Identifier
"}}}


""""""
" {{{C/CPP stuff:
""""""
hi Keyword NONE
" the actual keyword
hi link Structure Keyword 
" Cpp for modifiers (export, inline, explicit, override, virtual, final) 
hi cppModifier cterm=italic  ctermfg=9 guifg=#ff0000
"}}}


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" {{{ADMINISTRATIVE STUFF - STOPS SYNTAX LOADING BREAKING STUFF:"
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" DIDNT REALISE YOU HAD TO DO THIS MANUALLY >>>>>>>>>>
let g:colors_name = 'purple'
"}}}

