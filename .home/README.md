Contains files and folders that should be linked into `$HOME`

This is so I don't have a git repo in my home directory :p

Contains stuff like .bashrc and .xprofile

GTK theme is set in .gtkrc-2.0 (todo: 3.0)
