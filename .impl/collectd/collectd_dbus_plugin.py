import collectd
import dbus


data = {}
threading = None

# Class that collects events from the asynchronous multithreaded collectd callbacks
# and merges them into a single collection, with a continuously running dbus service.
#
# Actual class definition in initialisation() since that's when the threading module becomes available 
CollectorThread = None

# Holds the actual collector thread, with locks on it's items.
collector_thread_instance = None

# The base bus path to put this script onto
base_dbus_bus_path = "to.uk.sapient"
base_dbus_object_path = "/to/uk/sapient"
base_dbus_interface_path = "to.uk.sapient"


class DBusStatisticsLockingObject(dbus.Object):
    """
    Provides a dbus interface for various statistics collated from collectd
    This automatically locks access to various stats to be thread safe. It also
    broadcasts signals for statistics changing.
    """


def initialisation():
    """
    Initialisation time.

    Loads the threading module because it isn't available at this point.
    """
    import threading as threading_module
    global threading = threading_module

    

    class CollectorThreadClassDefinition(threading_module.Thread):
        """
        Runs an async dbus loop and collect values into a locked dictionary,
        also broadcasting signals over dbus for:
         - stat updates
         - pinging that we are available
         - notifying that we are going to shutdown

        We also provide callable dbus functions to retrieve the last value for any given statistic. 
        """



    global CollectorThread = CollectorThreadClassDefinition

def on_value_written(value):
    """
    value is a collectd.Value
    """

def do_shutdown():
    """
    Run on shutdown
    """


collectd.register_init(initialisation)
collectd.register_write(on_value_written)
collectd.register_shutdown(do_shutdown)
