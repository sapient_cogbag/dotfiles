# Some of this is yanked from powerline.segments

from __future__ import (unicode_literals, division, absolute_import, print_function)

import os

from powerline.lib.unicode import out_u
from powerline.theme import requires_segment_info
from powerline.segments import Segment, with_docstring
import powerline.segments.common.env as powerline_env

@requires_segment_info
def envformat(
        pl, 
        segment_info, 
        variable=False, 
        exists_fmt="{variable_name}", 
        exists_highlight_groups=None, 
        noexists_fmt=False, 
        noexists_highlight_groups=None
    ):
    """
    Creates a segment that works differently depending on 
    whether or not a given environment variable exists or not.

    exists_fmt and noexists_fmt are format strings that specify the 
    segment contents if the specified environment variable exists or doesn't,
    respectively.

    exists_highlight_groups and noexists_highlight_groups are lists of
    extra highlight groups to have in the output, in addition to the default
    values, in priority order 

    The default highlight groups are, in priority order:
        "env:envf"  # default
        "env:envf_exists_<variablename>" and "env:envf_exists" if the variable exists
        "env:envf_noexists_<variablename>" and "env:envf_noexists" if the variable does not


    Highlight groups used: ``env:envf``[ or ``env:envf_exists`` or ``env:envf_exists_variable``]*[ or ``env:envf_noexists`` or ``env:envf_noexists_variable``]


    if they are set to anything other than a string, then nothing is output if
    the variable exists or doesn't, respectively.

    The formatting is called with the following parameters:
        variable_name: the name of the environment variable.
        variable_value: the value of the environment variable (only available for
            exists_fmt)
        env: the dictionary holding all the environment variables and their values.
    """

    # Exists highlight groups
    full_exists_hgrps = (exists_highlight_groups if exists_highlight_groups is not None else []) + [
            "env:envf_exists_{}".format(variable), 
            "env:envf_exists",
            "env:envf"
    ]

    # Noexists highlight groups
    full_noexists_hgrps = (noexists_highlight_groups if noexists_highlight_groups is not None else []) + [
        "env:envf_noexists_{}".format(variable),
        "env:envf_noexists",
        "env:envf"
    ]

    # See powerline/segments/common/env.py in python
    does_exist = variable in segment_info["environ"]
    if does_exist:
        value = segment_info["environ"][variable]
        if isinstance(exists_fmt, str):
            # Do formatting:
            contents = exists_fmt.format(
                    variable_name=variable, 
                    variable_value=value, 
                    env=segment_info["environ"]
            )
            return [{
                "contents": contents,
                "highlight_groups": full_exists_hgrps 
            }]
        else:
            return None
    else:
        if isinstance(noexists_fmt, str):
            # Do formatting:
            contents = noexists_fmt.format(
                    variable_name=variable,
                    env=segment_info['environ']
            )
            return [{
                "contents": contents,
                "highlight_groups": full_noexists_hgrps
            }]
        else:
            return None


@requires_segment_info
def displayuser(
        pl, 
        segment_info, 
        hide_user=None, 
        hide_domain=False, 
        highlight_groups=None, 
        divider_highlight_group=None
    ):
    """
    Acts like powerline.segments.common.env.user but if an environment
    variable "$DISPLAYUSER" is set then we spit out that instead of the
    normal username. Note that the hide_user criterion is applied to 
    the actual username, NOT the display username.

    This also adds the ``user:divider`` divider highlight group which also
    can be overridden by the divider_highlight_group option (powerline only
    allows one). If the value is integer 0, then the divider group will be
    selected exclusively between "user:divider" and "superuser:divider" 
    depending on whether you are uid 0 or not.

    also allows you to specify extra highlight groups with highlight_groups parameter.

    Highlight groups work perfectly fine :)
    """
    initial_result = powerline_env.user(pl, segment_info, hide_user=hide_user, hide_domain=hide_domain)
    environ = segment_info["environ"]
    if initial_result is not None: 
        if "DISPLAYUSER" in environ:
            initial_result[0]["contents"] = environ["DISPLAYUSER"]
        # divider
        initial_result[0]["divider_highlight_group"] = "user:divider"
        # mode0 selector
        if divider_highlight_group == 0 and "superuser" in initial_result[0]["highlight_groups"]:
            initial_result[0]["divider_highlight_group"] = "superuser:divider"
        elif divider_highlight_group is not None:
            initial_result[0]["divider_highlight_group"] = divider_highlight_group

        # Add custom highlight groups
        hgs = ([] if highlight_groups is None else highlight_groups)
        initial_result[0]["highlight_groups"] = hgs + initial_result[0]["highlight_groups"]
    return initial_result
