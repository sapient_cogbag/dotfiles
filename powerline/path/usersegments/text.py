# Some of this is yanked from powerline.segments

from __future__ import (unicode_literals, division, absolute_import, print_function)

import os

from powerline.lib.unicode import out_u
from powerline.theme import requires_segment_info
from powerline.segments import Segment, with_docstring
import time

# This one literally just is a plain divider function because
# the string-type things don't wanna work and this is easier

def text(pl, contents="", highlight_groups=None):
    """
    Highlight groups used: ``text:text``[ or ``text:highlight_groups`` ]

    Simply creates a given text data with specified contents and
    if specified the highlight groups, in priority order,
    and finally the text:text highlight group
    """
    hgrps = (highlight_groups if highlight_groups is not None else []) + ["text:text"]

    return [{
        "contents": contents,
        "highlight_groups": hgrps
    }]


