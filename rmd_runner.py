#!/usr/bin/env python
"""
Executes the Runnable Markdown format/specification...

The RMD format consists of standard markdown with runnable sections specified as:
  optional \>section-type:required-params:...[optional-params]:...
  whitespace
  ``` (optional section)
  text section
  ```

RMD files have the suffix .r.md generally, and we have LINENO files which consist of the original filename plus an added .lineno suffix, and
contain text saying the linenumber that the executor got to when erroring (the failing line), or `done`, specifying that that
section is done. This allows rmd files to effectively act as a sequence of instructions where an instruction failure can effectively be
stopped at and resumed after user intervention. Note that loading from lineno files is only supported if changes to the previous lines 
do not add any lines, and other changes to lines only occur AFTER the given instruction.


USEFUL SPECIFIERS:
    bool specifier ~ takes 0, false, true, f, t, n<anything>, y<anything> ~ nya, case insensitively converts these to True or False


SECTION TYPES:
    \>file:<path>:[override]
    This places the text section contents into the file at `path`, creating directories as needed. the path is a required argument.
    The path can either be absolute or relative to the current script directory. Environment variables are substituted ^.^ nya
    The override option is 


    \>pkglist:[pacman-program]:[always=false]:
    This interprets the text section contents as a list of packages to be passed as parameters to `yay`. Hence this can take both AUR and normal
    packages. 

    The optional pacman-program parameter is a command to run instead of yay, it should accept standard -S options for installing. For
    plain pacman usage you can use `sudo pacman` as the value. The text section will ignore anything after a hash as a comment, and remove 
    whitespace surrounding each package on each line, before calling the command. The optional arguments are of the form `key=value`.
    Pairs are:
        * always={bool specifier}  - Default is always = false/0, you can set it to true. When false, this will use --needed in package installs
          when true it will not use that nya.



"""

import os
import os.path
import sys
import enum
import abc




def argument_parsing(provided_optarg_dictionary, argument_specification, error_on_unrecognised=False):
    """
    Attempt to parse a dictionary of arguments to names using the provided specifiers.
    The argument value dictionary is a map {<argument name>:<provided string>} nyaaa
    the argument specification is a map {<argument name>: {"parser": <parser type>, "default": <default (post parsing) value>}}
    if error_on_unrecognised is true then we error when an argument in the dictionary is not recognised.
    if false, the argument is passed through with no parsing of the data at all (i.e. nyaaa the data is passed through as an unparsed value/string) 
    
    where the parser type is one of:
        * "bool" - parses case insensitively, as per bool specifiers nyaaa 

    This returns a pair (optional error string, parsed argument dictionary including defaults fulfilled)
    """

    def bool_parser(data):
        """
        Parser for bool specifiers nya
        """
        data = data.strip().lower()
        
        if data in {'1', 't', 'true'} or data.startswith('y'):
            return (True, None)
        
        elif data in {'0', 'f', 'false'} or data.startswith('n'):
            return (False, None)

        # error
        return (None, "Invalid boolean specifier '{}'".format(data))
    
    # Parser functions here take the data string, and spit out a pair (value, optional error string)
    parser_types = {
        "bool": bool_parser
    }

    resulting_dict = {}

    # Fill in defaults
    for arg_specified_name in argument_specification.keys():
        default_value = argument_specification[arg_specified_name]["default"]
        resulting_dict[arg_specified_name] = default_value

    # parse args, overriding any defaults nya
    for arg in provided_optarg_dictionary.keys():
        data = provided_optarg_dictionary[arg]

        # check for existence, and if necessary, error
        if arg not in argument_specification:
            if error_on_unrecognised:  # Error
                return ("Unrecognised argument name {}, with data '{}'".format(arg, data), None)
            else:
                # Pass through nyaa
                resulting_dict[arg] = data
        # argument exists, parse nyaaaa ^.^
        else:
            parser_code = argument_specification[arg]
            parser_function = parser_types[parser_code] # nyaaa
            parsed_value, error_string = parser_function(data)
            if error_string is not None:  # error happened
                return (error_string, None)
            else:
                resulting_dict[arg] = parsed_value

    return (None, resulting_dict)
           




class S_VAL_REQ_ENUM(enum.IntEnum):
    """
    Enum opt/req/none status of section text.
    """
    DISALLOWED = 0  # No text section allowed - this will mean any following section will be ignored.
    ALLOWED = 1 # Section allowed but not required
    REQUIRED = 2 # Text section required.


class SectionSpecifier(abc.ABC):
    """
    Specifier for a section type.
    """
    @abc.abstractmethod
    def parameter_count(self):
        """
        Get the number of parameters to specify.

        Should return two integers in a tuple - the first is the number of REQUIRED parameters, the second is the number of OPTIONAL parameters.

        If the second is infinity, that will work too.
        """

    @abc.abstractmethod
    def section_required(self):
        """
        Retuns an S_VAL_REQ_ENUM indicating whether a text section is disallowed, allowed, or required.
        """

    @abc.abstractmethod
    def process(self, arguments, text_content):
        """
        Actually perform the processing of the text section and argument.

        arguments are the parameters, text content is raw text (optional)

        This returns an optional parsing error (if not None it should be a string, and this will be an error). Exceptions
        thrown will also be caught.
        """
            
                 

class FileSpecifier(SectionSpecifier):
    """
    Specifier for setting file data.
    """
    def parameter_count(self):
        pass 
