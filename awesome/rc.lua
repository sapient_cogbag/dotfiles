require("imports")
local widget_gen = require("widget_gen")
local keys = require("keys")
local util = require("utils.util")
local chval = require("utils.chval")
local widget_util = require("utils.widget_util")
local actions = require("actions")

-- In root of this awesome config, we use
-- https://github.com/awesomeWM/awesome/blob/master/.luacheckrc
-- to define a .luacompleterc
-- (this is here because json doesn't allow comments nyaaa)




-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({ 
        preset = naughty.config.presets.critical,
        title = "Oops, there were errors during startup!",
        text = awesome.startup_errors 
    })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true
        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = tostring(err) })
        in_error = false
    end)
end
-- }}}

-- {{{ Variable definitions
-- Themes define colours, icons, font and wallpapers.
-- beautiful.init(gears.filesystem.get_themes_dir() .. "default/theme.lua")
beautiful.init("~/.config/awesome/themes/" .. "purple-and-etc-theme.lua")
local successful_pulse_load, pulse = pcall(require, "pulseaudio_widget")


-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
    awful.layout.suit.fair,
    awful.layout.suit.tile,
    --awful.layout.suit.tile.left,
    --awful.layout.suit.tile.bottom,
    --awful.layout.suit.tile.top,
    awful.layout.suit.floating,
    --awful.layout.suit.fair.horizontal,
    --awful.layout.suit.spiral,
    --awful.layout.suit.spiral.dwindle,
    --awful.layout.suit.max,
    --awful.layout.suit.max.fullscreen,
    --awful.layout.suit.magnifier,
    --awful.layout.suit.corner.nw,
    -- awful.layout.suit.corner.ne,
    -- awful.layout.suit.corner.sw,
    -- awful.layout.suit.corner.se,
}
-- }}}

local rules = require("rules")
-- {{{ Menu
-- Create a launcher widget and a main menu
myawesomemenu = {
   { "Hotkeys", function() hotkeys_popup.show_help(nil, awful.screen.focused()) end },
   { "Manual", terminal .. " -e man awesome" },
   { "Edit Config", editor_cmd .. " " .. awesome.conffile },
   { "Restart", awesome.restart },
   { "Lock Screen", function() actions.lock_screen() end},
   { "Quit", function() awesome.quit() end },
}

mymainmenu = freedesktop.menu.build({
	before =  {
	    { "Awesome Menu", myawesomemenu, beautiful.awesome_icon },
	},
    after = {
	    { "Open Terminal", terminal },
	},
	submenu = "All Programs"
})

mylauncher = awful.widget.launcher({ 
    image = beautiful.awesome_icon,
    menu = mymainmenu 
})

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

-- Keyboard map indicator and switcher
mykeyboardlayout = awful.widget.keyboardlayout()



-- Function that generates a function that will move an object next to the argument provided.
-- See https://awesomewm.org/doc/api/libraries/awful.placement.html#next_to for what the other arguments mean. 
function next_to_object(wibox_or_drawable_or_find_widget_result_against, preferred_positions, preferred_anchors)
	args = {
		preferred_positions = preferred_positions,
		preferred_anchors = preferred_anchors,
		geometry = wibox_or_drawable_or_find_widget_result_against
	}
	return (function (obj_move)
	    return awful.placement.next_to(obj_move, args)
	end)
end

-- {{{ Wibar
-- Create a textclock widget
mytextclock = wibox.widget.textclock("%H:%M:%S", 0.5)
mytextclock.date_tooltip = awful.tooltip {
	objects = {mytextclock},
	timer_function = (function() 
		return os.date("%A, %d %B %Y")
	end)
}

-- Make a calendar viewable from the text clock.
-- We do not need to regenerate every time, simply set the date when it is created. 

mytextclock.internal_calendar = wibox.widget.calendar.month(os.date('*t'))
mytextclock.calendar_popup = awful.popup({
	widget = wibox.widget {
		mytextclock.internal_calendar,
		color = beautiful.get().border_focus,
		margins = 3,
		layout = wibox.container.margin
	},
	preferred_positions = {"top", "bottom", "left", "right"}, 
	preferred_anchors = {"middle", "front", "back"},
	ontop=true,
	visible=false,
	
})

mytextclock:connect_signal("button::press", function(self, lx, ly, button, mods, find_widget_result)
	mytextclock.internal_calendar.date = os.date('*t')
	mytextclock.calendar_popup.visible = not mytextclock.calendar_popup.visible
	if mytextclock.calendar_popup.visible then
		mytextclock.calendar_popup:move_next_to(find_widget_result)
	end
end)



-- lx/ly are offsets from the widget coords, button is the mousebutton pressed, mods are the ctrl/shift/ModX/etc. keys pressed,
-- find_widget_result is the result of wibox.drawable.find_widget, which is something you can actually use to move next to since
-- it is actually a drawable thing whereas widgets are odd.  
--mytextclock:connect_signal("button::press", function(lx, ly, button, mods, find_widget_result)
--	mytextclock.internal_calendar.date = os.date('*t')		
--	mytextclock.calendar_popup.visible = not mytextclock.calendar_popup.visible
--	if mytextclock.calendar_popup.visible then
--		awful.placement.closest_corner(mytextclock.calendar_popup)
--		--print(find_widget_result)
--		--next_to_object(find_widget_result, {"top", "bottom", "left", "right"}, {"middle", "front", "back"})(mytextclock.calendar_popup)
--	end
--end)


-- Create a wibox for each screen and add it


local function set_wallpaper(s)
    -- Wallpaper
    if beautiful.wallpaper then
        local wallpaper = beautiful.wallpaper
        -- If wallpaper is a function, call it with the screen
        if type(wallpaper) == "function" then
            wallpaper = wallpaper(s)
        end
        gears.wallpaper.maximized(wallpaper, s, true)
    end
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)


-- Hack the pulseaudio widget to add events for volume change :p
-- See: https://github.com/stefano-m/awesome-pulseaudio_widget/blob/master/pulseaudio_widget.lua
-- for the callbacks we are hooking into.
-- Emitted signals are `audio::muted`, `audio::unmuted`, and `audio::volume`
-- When muted, we emit a volume signal with parameter 0
-- when unmuted we emit a volume signal with the parameter for
--
-- TODO: Implement sane audio widget that doesn't require these nasty hacks using
-- pulseaudio_dbus directly!
-- 
local function pulseaudio_widget_hack(audiowidget)
    local old_updateappearance = audiowidget.update_appearance
    
    audiowidget.update_appearance = function(self, volumestate)
        -- Muted.
        if volumestate == "Muted" then
            self:emit_signal("audio::muted")
            self:emit_signal("audio::volume", 0)
        elseif volumestate == "Unmuted" then 
            -- Volume is "Unmuted", get the volume using the method in the aforementioned file
            self:emit_signal("audio::unmuted")
            local vol = self:get_volume_by_percentage()
            self:emit_signal("audio::volume", vol)
        else -- Just volume change
            local vol = tonumber(volumestate)
            self:emit_signal("audio::volume", vol)
        end
        -- protection against missing sinks/pulseaudio connection failure/load errors
        -- TODO: Make it so this sets a muted icon when there was an error
        if self.sink then
            old_updateappearance(self, volumestate)
        end
    end
    
    audiowidget.get_volume_by_percentage = function(self)
        if self.sink then
            return self.sink:get_volume_percent()[1]
        else
            return 0
        end
    end
    
    audiowidget.get_is_muted = function(self)
	-- Protection against missing sinks/audio errors
        if self.sink then
            return self.sink:is_muted()
        else
            return true
        end
    end
    
    -- Emit new signals with current state. This is good for forcing
    -- updates when the widget has just been initialised.
    audiowidget.emit_refreshed_state = function(self)
        local is_muted = self:get_is_muted()
        local vol = self:get_volume_by_percentage()
        if is_muted then
            self:emit_signal("audio::muted")
            self:emit_signal("audio::volume", 0)
        else
            self:emit_signal("audio::unmuted")
            self:emit_signal("audio::volume", vol)
        end
    end
    
    -- set volume by percent
    -- See the code for pulseaudio_widget for how this works
    audiowidget.set_volume_by_percentage = function(self, volume)
        if self.sink and self:get_volume_by_percentage() ~= volume then
            self.sink:set_volume_percent({volume})
        end
    end
    
    audiowidget.set_is_muted = function (self, muted_v) 
        if self.sink and self:get_is_muted() ~= util.to_bool(muted_v) then
            self.sink:set_muted(util.to_bool(muted_v))
            self:update_appearance((muted_v and "Muted") or "Unmuted")
        end
    end
    
    return audiowidget
end


awful.screen.connect_for_each_screen(function(s)
    -- Wallpaper
    set_wallpaper(s)

    -- Each screen has its own tag table.
    awful.tag({ "1", "2", "3", "4", "5", "6", "7", "8", "9" }, s, awful.layout.layouts[1])

    -- Generator for boundary powerline widgets.
    -- If `shiftfrac` is not nil, then set the shift fraction to that
    -- (set to 1 if you are the end of a widget, -1 if you want to make the previous one
    -- have a full, unclipped end, and 0 if you just want a start).
    local function boundary_powerline_generator(shiftfrac)
        local shiftfraction = shiftfrac or 0
        return widget_gen.generate_taskbar_powerline_boundary(
            powerline_taskbar_boundary_width,
            taskbar_height_needs_dpi,
            shiftfraction,
            beautiful.menu_border_color
        )
    end


    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(
        keys.layout_box_buttons
    )
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist {
        screen  = s,
        filter  = awful.widget.taglist.filter.all,
        buttons = keys.taglist_buttons
    }

    -- Create a tasklist widget
    -- See: https://awesomewm.org/doc/api/classes/awful.widget.tasklist.html
    s.mytasklist = awful.widget.tasklist {
        screen  = s,
        filter  = awful.widget.tasklist.filter.currenttags,
        buttons = keys.tasklist_buttons,
        style = { -- Make stuff powerliney
            shape_border_width = 0,
            shape_border_color = beautiful.border_normal,
            shape = gears.shape.powerline,
            shape_clip = false
        },
        layout = {
            spacing = dpi(-(taskbar_height_needs_dpi/2)), -- We remove a chunk of spacing 
            -- to create powerline-boundaries between the elements
            -- Note that we know the arrows will be of extension length (height/2)
            -- See: https://awesomewm.org/doc/api/libraries/gears.shape.html
            layout = wibox.layout.flex.horizontal
        },
        widget_template = {
            {
                -- We put this here to avoid clipping by the layout setting and create
                -- a true powerline effect at the start of each thing rather than one partially
                -- clipped at the end or top.
                boundary_powerline_generator(),
                {
                    {
                        {
                            {
                                id = "icon_role", -- Used by tasklist so it's easy to specify the chunks 
                                -- without manual stuff.
                                widget = wibox.widget.imagebox,
                            },
                            margins = dpi(2),
                            widget = wibox.container.margin
                        },
                        {
                            id = "text_role",
                            widget = wibox.widget.textbox
                        },
                        layout = wibox.layout.fixed.horizontal
                    },
                    -- Margins to prevent icons on the left overlapping the arrow stabbing
                    -- into the client on the taskbar
                    left = 0, 
                    -- again we know the arrow depth ^.^
                    right = dpi(taskbar_height_needs_dpi/2),
                    widget = wibox.container.margin
                },
                boundary_powerline_generator(1),
                layout = wibox.layout.align.horizontal
            },
            id = "background_role",
            widget = wibox.container.background
        }
    }

    -- Create the wibox
    s.mywibox = awful.wibar({
        position = "bottom", 
        screen = s, 
        ontop = true,
        visible = false,
        height = dpi(taskbar_height_needs_dpi)
    })

    s.audio_data = {}
    s.audio_data.is_muted = chval.chval(false)
    s.audio_data.volume_value = chval.chval(0, 1) -- match to ints.

    -- See: https://awesomewm.org/apidoc/widgets/wibox.widget.slider.html
    local audio_draggable_bar = wibox.widget {
        widget = wibox.widget.slider,
        
        handle_shape = gears.shape.rectangle,
        handle_color = beautiful.accent_orange,
        handle_margins = 0,
        handle_width = dpi(6),
        handle_border_width = 0,
        
        bar_border_width = 0,
        bar_margins = 0,
        bar_color = beautiful.menu_border_color,
        
        value = 0,
        minimum = 0,
        maximum = 150,
        forced_width = dpi(150),
    }
    
    -- property::value for some reason does not have the new value as an argument
    s.audio_data.volume_value:add_callback(audio_draggable_bar, function(adrbr, new_value) 
        audio_draggable_bar.value = new_value
    end)

    audio_draggable_bar:connect_signal("property::value", function(wdg) 
        s.audio_data.volume_value:set(audio_draggable_bar, audio_draggable_bar.value)
    end)
    
    -- https://awesomewm.org/apidoc/widgets/wibox.widget.checkbox.html
    local audio_muted_checkbox = wibox.widget {
        widget = wibox.widget.checkbox,
        forced_height = dpi(18)
    }
    
    audio_muted_checkbox:buttons(gears.table.join(
        awful.button ({}, 1, function() 
            audio_muted_checkbox.checked = not audio_muted_checkbox.checked 
        end)
    ))

    -- checked property does not actually pass whether or not we are checked >.<
    --audio_muted_checkbox:connect_signal("property::checked", function(widget) 
        --s.audio_data.is_muted:set(audio_muted_checkbox, audio_muted_checkbox.checked)
    --end)

    --s.audio_data.is_muted:add_callback(audio_muted_checkbox, function(amcbx, value) 
        --audio_muted_checkbox.checked = value
    --end)
    

    s.audio_volume_slider_box = awful.popup {
        border_color = beautiful.menu_border_color,
        border_width = dpi(2),
        visible = false,
        ontop=true,
        shape = gears.shape.rectangle,
        screen = s,
        bg = beautiful.bg_normal,
        widget =  {
            forced_width = dpi(18),
            layout = wibox.layout.fixed.vertical,
            {
                widget = wibox.container.margin,
                left = dpi(2),
                right = dpi(2),
                top = dpi(2),
                {
                    widget= wibox.container.rotate,
                    direction = "east",
                    audio_draggable_bar
                }
            },
            {
                widget = wibox.container.margin,
                top = dpi(2),
                left = dpi(2),
                right = dpi(2),
                bottom = dpi(-2), -- fixes a weird gap
                audio_muted_checkbox
            }
        }
    }
    
    if successful_pulse_load then
        s.pulseaudio_widget_internal = pulseaudio_widget_hack(pulse) or wibox.widget.base.make_widget()
        -- Clear out the buttons
        s.pulseaudio_widget_internal:buttons({})
    end
    
    s.pulseaudio_control_widget = wibox.widget {
        s.pulseaudio_widget_internal,
        max_value = 100,
        min_value = 0,
        start_angle = math.pi * 3/4,
        forced_width = dpi(taskbar_height_needs_dpi),
        forced_height = dpi(taskbar_height_needs_dpi),
        thickness = dpi(2),
        value = 0, -- initial
        widget = wibox.container.arcchart
    }
    
    if successful_pulse_load then
        s.pulseaudio_widget_internal:connect_signal("audio::volume", function(the_widget, volume) 
            s.audio_data.volume_value:set(s.pulseaudio_widget_internal, volume)
        end)
    
        s.pulseaudio_widget_internal:connect_signal("audio::muted", function(the_widget) 
            s.audio_data.is_muted:set(s.pulseaudio_widget_internal, true)
        end)
    
        s.pulseaudio_widget_internal:connect_signal("audio::unmuted", function(the_widget)
            s.audio_data.is_muted:set(s.pulseaudio_widget_internal, false)
        end)
    
        -- Callbacks to modify the main widget and change the value
        s.audio_data.is_muted:add_callback(s.pulseaudio_widget_internal, function(self, new_val) 
            s.pulseaudio_widget_internal:set_is_muted(new_val)
        end)
    
        s.audio_data.volume_value:add_callback(s.pulseaudio_control_widget, function(self, new_value)
            s.pulseaudio_control_widget.value = new_value
        end)
    
        s.audio_data.volume_value:add_callback(s.pulseaudio_widget_internal, function(self, new_value)
            s.pulseaudio_widget_internal:set_volume_by_percentage(new_value)
        end)
        
        s.pulseaudio_widget_internal:emit_refreshed_state()
        
        -- The popup will be next to the widget.
        --s.audio_volume_slider_box:bind_to_widget(s.pulseaudio_control_widget)
        -- Do not use this - it only shows on click, it does not do proper toggling
        
        -- We need the geometry to move 
        
        widget_util.bind_popup_to_widget(
            s.audio_volume_slider_box, 
            s.pulseaudio_control_widget,
            1,
            widget_util.behaviour.toggle
        )
    end

    -- We set the background colour to match the muted/slashed icon colour
    -- so the circle forms a "stop sign"
    
    -- Decided I prefer it to have pink outline lol
    --[[
    s.pulseaudio_widget_internal:connect_signal("audio::muted", function(the_widget) 
        s.pulseaudio_control_widget.border_color = beautiful.radialprogressbar_color
    end)

    s.pulseaudio_widget_internal:connect_signal("audio::unmuted", function(the_widget)
        s.pulseaudio_control_widget.color = beautiful.radialprogressbar_color
    end)
    ]]--

    -- Add widgets to the wibox
    s.mywibox:setup {
        {
            layout = wibox.layout.align.horizontal,
            { -- Left widgets
                layout = wibox.layout.fixed.horizontal,
                mylauncher,
                s.mytaglist,
                s.mypromptbox
            },
            {
                layout = wibox.layout.fixed.horizontal,
                boundary_powerline_generator(1), -- end of the above
                {
                    s.mytasklist, -- Middle widget
                    halign = "center",
                    valign = "center",
                    fill_horizontal = true,
                    content_fill_horizontal = true,
                    widget = wibox.container.place
                },
                spacing = dpi(-taskbar_height_needs_dpi/2) -- Shift to ensure powerline bounds 
                -- overlap :3
            },
             -- Clean up the taskbar powerline
            { -- Right widgets
                -- This shifts the given boundary powerline so it goes back far enough to cover the
                -- end of the last one. The calculated numerator is the length of the previous one
                -- which we shift by because the spacing between elements is 0 anyway (in the layout)
                boundary_powerline_generator(
                    -(0.5*taskbar_height_needs_dpi + powerline_taskbar_boundary_width)/powerline_taskbar_boundary_width
                ), 
                -- start of the below
                layout = wibox.layout.fixed.horizontal,
                {
                    layout = wibox.layout.fixed.horizontal,
                    mykeyboardlayout,
                    wibox.widget.systray(),
                    s.pulseaudio_control_widget,
                    mytextclock,
                    s.mylayoutbox
                },
                spacing = dpi(taskbar_height_needs_dpi/2)
            },
        },
        color = beautiful.menu_border_color,
        top = dpi(2),
        widget = wibox.container.margin
    }
end)
-- }}}

rules.init()

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c)
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    -- if not awesome.startup then awful.client.setslave(c) end

    if awesome.startup
        and not c.size_hints.user_position
        and not c.size_hints.program_position then
        -- Prevent clients from being unreachable after screen count changes.
        awful.placement.no_offscreen(c)
    end
    -- Struts have a top/left/bottom/right (all optional) members which
    -- determine the area the window will not take up along those sides of the screen
    -- (when maximised)
    -- We have no use for these right now but the AwesomeWM documentation for these
    -- is very very slim so I'm leaving these documents here.
    --c:struts()
    
    --border_merge(c)
    if c.maximized then
        util.toggle_window_maximized(c)
    end

end)

client.connect_signal("property::size", function(client) 
    --border_merge(client)
    
    -- Fix weird border deletion after resize
    client.border_width = beautiful.border_width
end)

-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)

    -- This is the actual, top titlebar
    
    local real_titlebar = awful.titlebar(c)

    local icon = awful.titlebar.widget.iconwidget(c)
    local title_string_widget = awful.titlebar.widget.titlewidget(c)
    
    -- True if we've just been clicked (i.e. there was a button press 
    title_string_widget.just_clicked = false
    -- The timer, if it exists, currently running.
    title_string_widget.just_clicked_timer = nil

    local last_click_time = DateTime.new_now_utc()

    -- buttons for the titlebar
    local buttons = gears.table.join(
        awful.button({ }, 1, function()
            c:emit_signal("request::activate", "titlebar", {raise = true})	    
            local this_click_time = DateTime.new_now_utc()
            local delta_us = DateTime.difference(this_click_time, last_click_time)
            if delta_us < double_click_delay_microseconds then
                title_string_widget:emit_signal("button::double_click")
            else
                if c.maximized and not (delta_us < double_click_delay_microseconds) then 
                    c.maximized = false
                end
                awful.mouse.client.move(c)
            end
            last_click_time = this_click_time
        end),
        awful.button({ }, 3, function()
            c:emit_signal("request::activate", "titlebar", {raise = true})
            awful.mouse.client.resize(c)
        end)
    )
    local maximisation_button = awful.titlebar.widget.maximizedbutton(c)

    title_string_widget : connect_signal("button::double_click", function(holder_widget)
        util.toggle_window_maximized(c)
        c:raise()
    end)

    real_titlebar : setup {
        { -- Left
            icon,
            buttons = buttons,
            layout  = wibox.layout.fixed.horizontal
        },
        { -- Middle
            { -- Title
                align  = "center",
                widget = title_string_widget
            },
            buttons = buttons,
            layout  = wibox.layout.flex.horizontal
        },
        { -- Right
            --awful.titlebar.widget.floatingbutton (c),
            awful.titlebar.widget.minimizebutton(c),
            maximisation_button,	    
            --awful.titlebar.widget.stickybutton   (c),
            --awful.titlebar.widget.ontopbutton    (c),
            awful.titlebar.widget.closebutton    (c),
            layout = wibox.layout.fixed.horizontal()
        },
        layout = wibox.layout.align.horizontal
    }

    

    -- Picks closest corner or side to the mouse.
    -- local resizing_buttons = gears.table.join(awful.button({}, 1, function() awful.mouse.client.resize(c) end))

    -- left/bottom/right micro-taskbars to enable dragging. 
    --awful.titlebar(c, {size=1,position="left"}) : setup {
--	{
--	    {},
--	    buttons = resizing_buttons
--	},
--	{
--	    {},
--	    buttons = resizing_buttons
--	},
--	{
--	    {},
--	    buttons = resizing_buttons
--	},
--	layout = awful.wibar.layout.align.vertical
--    }
	
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
    c:emit_signal("request::activate", "mouse_enter", {raise = false})
end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}

keys.set_keys()
