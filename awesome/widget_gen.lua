require("imports")
local widget_gen = {}

--[[
Generate a shifted powerline-type "widget-template"-table.

`powerline_boundary_width` is how thicc the boundary is.
`powerline_sep_forced_height` is the height of the boundary thing. The actual width of the generated
    widget table is calculated from this and powerline_boundary_width (using the default powerline setup
    where the arrow is of length = height/2.
`fractional_shift` is how much of it's boundary width it will be translated by.
    (default is `0`). `1` will, if at the end of an element, overlap the starting powerline boundary of the
    next widget if it has fractional shift 0
`colour` is the colour of the boundary (duh)
]]
function widget_gen.generate_taskbar_powerline_boundary(
        powerline_boundary_width, 
        powerline_sep_forced_height,
        fractional_shift, 
        colour
    )
    local x_delta = fractional_shift * powerline_boundary_width
    
    return { 
        wibox.widget.base.make_widget(),
        bg = colour,
        forced_width = dpi(
            powerline_sep_forced_height/2 + powerline_boundary_width
        ),
        forced_height = dpi(powerline_sep_forced_height),
        shape = gears.shape.transform(gears.shape.powerline) : 
            translate(x_delta, 0),
        shape_clip = false,
        widget = wibox.container.background
    }
end

return widget_gen