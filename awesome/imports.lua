-- File for doing all the importing crap.

-- Nicked my file splitting from https://gitlab.com/CraftedCart/dotfiles/tree/master/.config/awesome 

-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")

-- Vicious library
vicious = require("vicious") -- pacman -S vicious or yay -S vicious
freedesktop = require("freedesktop") -- pacman -S awesome-freedesktop or yay -S awesome-freedesktop
class = require("extern.classified.src.class") -- Alice's lua class library
require("table")


-- Standard awesome library
gears = require("gears")
awful = require("awful")
require("awful.autofocus")
-- Widget and layout library
wibox = require("wibox")
-- Theme handling library
beautiful = require("beautiful")
dpi = beautiful.xresources.apply_dpi
-- Notification library
naughty = require("naughty")
menubar = require("menubar")
hotkeys_popup = require("awful.hotkeys_popup")
-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
require("awful.hotkeys_popup.keys")
err = require("utils.errdefs")


glib = require("lgi").GLib
gobject = require("lgi").GObject
gio = require("lgi").Gio


DateTime = glib.DateTime


-- This is used later as the default terminal and editor to run.
terminal = "kitty"  -- pacman -S kitty
editor = os.getenv("EDITOR") or "vim"
editor_cmd = terminal .. " -e " .. editor

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"

double_click_delay_microseconds = 600000 --0.6s
taskbar_height_needs_dpi = 20
powerline_taskbar_boundary_width = 2

