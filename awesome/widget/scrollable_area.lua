require("imports")
local util = require("utils.util")
local set = require("utils.set")

local scrollable_area = {}

--Make an area to contain widgets that can be scrolled.

--[[

This scrollable area allows for vertical or horizontal (or both) scrolling, as well as
vertical or horizontal (or both) width and height fixed.

This is also a standard widget and you can do stuff.

`args` is a table containing the following data!
    - `vertical` - A table containing the following data:
        - `max_height` (optional)
        - `shrink_to_fit` - If `true`, the scrollable area will attempt to shrink to fit the inner widget
            if it is smaller than `max_height` (default: `false`)
        - `align` - When the widget is bigger the contained widget, where we should place the contents
            one of "start"(top), "end"(bottom) and "center". Default "center". If `shrink_to_fit` is `true`
            and the widget is actually allowed to shrink vertically, this has no effect.
        - `scrollable` - If `true`, we will allow scrolling in this direction (default: `true`)
    - `horizontal` - A table containing the following data:
        - `max_width` (optional)
        - `shrink_to_fit` - If `true` the scrollable area will attempt to shrink to firt the inner widget
            if it is smaller than `max_height` (default: `false`)
        - `align` - When the widget is bigger than the contained widget, where we should place
            the contents - one of "start"(left), "end"(right), and "center". Default "center".
            If `shrink_to_fit` is `true` and the widget is actually allowed to shrink horizontally, this has
            no effect.
        - `scrollable` - If `true`, we allow scrolling in this direction (default: `false`)
    - `[1]` - Child widget, default `nil`
    - `forced_width` and `forced_height`, which are optional standard widget args.
]]
function scrollable_area.new(args)
    local widget_output = wibox.widget.base.make_widget()
    widget_output._private._data = {}
    
    widget_output._private._data.vertical = args.vertical or {}
    local vertical = widget_output._private._data.vertical
    
    vertical.shrink_to_fit = util.to_bool(vertical.shrink_to_fit)
    vertical.align = util.default_selection(set.set{"start", "end", "center"}, "center", vertical.align)
    
    -- Default only vertically scrollable
    if vertical.scrollable == nil then
        vertical.scrollable = true
    end
    vertical.scrollable = util.to_bool(vertical.scrollable)
    
    widget_output._private._data.horizontal = args.horizontal or {}
    local horizontal = widget_output._private._data.horizontal
    
    horizontal.shrink_to_fit = util.to_bool(horizontal.shrink_to_fit)
    horizontal.align = util.default_selection(set.set{"start", "end", "center"}, "center", horizontal.align)
    
    if horizontal.scrollable == nil then
        horizontal.scrollable = false
    end
    horizontal.scrollable = util.to_bool(horizontal.scrollable)
    
    for fname, value in pairs(scrollable_area) do
        widget_output[fname] = value
    end
    widget_output:set_widget(args[1])
    
    widget_output:set_forced_height(args.forced_height)
    widget_output:set_forced_width(args.forced_width)
    
    return widget_output
end

function scrollable_area:set_widget(widg)
    local needs_update = (widg ~= self._private.child)
    if needs_update then
        self._private.child = widg
        self:emit_signal("widget::layout_changed")
    end
end

function scrollable_area:_get_scroll_data(h_or_v)
    if h_or_v == "h" then 
        return self._private._data.vertical
    elseif h_or_v == "v" then 
        return self._private.data.horizontal
    end
end

-- This widget only likes having one child widget ^.^
-- so we take the first.
function scrollable_area:set_children(children_arg)
    local children = children_arg or {}
    self:set_widget(children[1])
end

--[[
`scroll_table_id` is "v" or "h"
]]
function scrollable_area:_update_scroll_value(
        scroll_table_id, 
        member, 
        change_is_layout_change, 
        -- change_is_redrawing_change, -- since this widget draws nothing by itself only the children
        -- ever need redrawing, which is specified in layout_change.
        new_value
    )
    local old_value = self:_get_scroll_data(scroll_table_id)[member]
    if new_value ~= old_value then
        self:_get_scroll_data(scroll_table_id)[member] = new_value
        if change_is_layout_change then
            self:emit_signal("widget::layout_changed")
        end
    end
end



function scrollable_area:get_shrink_to_fit_v()
    return self:_get_scroll_data("v").shrink_to_fit
end

function scrollable_area:set_shrink_to_fit_v(new_value_arg) 
    self:_update_scroll_value("v", "shrink_to_fit", true, util.to_bool(new_value_arg))
end

function scrollable_area:get_shrink_to_fit_h()
    return self:_get_scroll_data("h").shrink_to_fit
end

function scrollable_area:set_shrink_to_fit_h(new_value_arg)
    self:_update_scroll_value("h", "shrink_to_fit", true, util.to_bool(new_value_arg))
end

--[[
All the max height/width stuff can be nil for none.
]]
function scrollable_area:get_max_height()
    return self:_get_scroll_data("v").max_height
end

function scrollable_area:get_max_width()
    return self:_get_scroll_data("h").max_width
end

function scrollable_area:set_max_height(new_max_height)
    self:_update_scroll_value("v", "max_height", true, new_max_height)
end

function scrollable_area:set_max_width(new_max_width)
    self:_update_scroll_value("h", "max_width", true, new_max_width)
end


-- Scrolling status
function scrollable_area:get_scrollable_v()
    return self:_get_scroll_data("v").scrollable
end

function scrollable_area:get_scrollable_h()
    return self:_get_scroll_data("h").scrollable
end

function scrollable_area:set_scrollable_v(scrollable_arg)
    self:_update_scroll_value("v", "scrollable", true, util.to_bool(scrollable_arg))
end

function scrollable_area:set_scrollable_h(scrollable_arg)
    self:_update_scroll_value("h", "scrollable", true, util.to_bool(scrollable_arg))
end

--[[
Note that the parent widget here should handle the forced width and height of this widget if it
calls `fit_widget` properly

Return requested width and height, which we will just make the given width and height or the max_width/
max_height if they are smaller.

Unless in each direction we shrink_to_fit, in which case we request the height/width of the contained item
unless it's bigger than max_height/max_width where we request that instead.
Effectively, the priority goes in this order:

- container height and width (if shrink_to_fit), bounded by
- smaller of max_width/max_height and provided width/height if they are present.

]]
function scrollable_area:fit(context, width, height)
    local child_width, child_height = wibox.widget.base.layout_widget(
        self, context, self._private.child, width, height
    )
    
    -- if max_width/height is nil we suck up all the space.
    local width_bound = math.min(width, self:get_max_width() or width)
    local height_bound = math.min(height, self:get_max_height() or height)
    
    
    local requested_width = width_bound
    local requested_height = height_bound
    -- If we shrink-to-fit then check the child's sizes.
    if self:get_shrink_to_fit_v() then
        requested_height = math.min(requested_height, child_height)
    end
    if self:get_shrink_to_fit_h() then
        requested_width = math.min(requested_width, child_width)
    end
    return requested_width, requested_height
end

function scrollable_area:layout()
    
end


function scrollable_area:get_children()
    return {self._private.child}
end

function scrollable_area:before_draw_children(context, cr, width, height)
    cr:push_group() -- Make the children write to a new group
end

function scrollable_area:after_draw_children(context, cr, width, height)
    cr:pop_group_to_source() -- They have now written, now we clip
    -- coordinates are already zeroed at topleft, we just have to make a rect path
    -- and clip to that.
    cr:rectangle(0, 0, width, height)
    cr:clip()
end

setmetatable(scrollable_area, {
    __call = function(self, arg) 
        return scrollable_area.new(arg)
    end
})

return scrollable_area