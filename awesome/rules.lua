local awful = require("awful")
local beautiful = require("beautiful")
local util = require("utils.util")
local keys = require("keys")
local rules = {}

function rules.init()
	-- {{{ Rules
	-- Rules to apply to new clients (through the "manage" signal).
	awful.rules.rules = {
	    -- All clients will match this rule.
	    {
            rule = { },
            properties = { 
                border_width = beautiful.border_width,
                border_color = beautiful.border_normal,
                focus = awful.client.focus.filter,
                raise = true,
                keys = keys.clientkeys,
                buttons = keys.clientbuttons,
                screen = awful.screen.preferred,
                placement = awful.placement.no_overlap+awful.placement.no_offscreen,
                size_hints_honor = false  -- For discord and other apps that
                -- try to tell us their size and ruin that sweet, sweet tiling
            }
        },
	    -- Floating clients.
        { 
            rule_any = {
                instance = {
                    "DTA",  -- Firefox addon DownThemAll.
                    "copyq",  -- Includes session name in class.
                    "pinentry"
                },
                class = {
                    "Arandr",
                    "Blueman-manager",
                    "Gpick",
                    "Kruler",
                    "MessageWin",  -- kalarm.
                    "Sxiv",
                    "Tor Browser", -- Needs a fixed window size to avoid fingerprinting by screen size.
                    "Wpa_gui",
                    "veromix",
                    "xtightvncviewer",
                },

                -- Note that the name property shown in xprop might be set slightly after creation of the client
                -- and the name shown there might not match defined rules here.
                name = {
                    "Event Tester",  -- xev.
                },
                role = {
                    "AlarmWindow",  -- Thunderbird's calendar.
                    "ConfigManager",  -- Thunderbird's about:config.
                    "pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
                }
            }, 
            properties = { 
                floating = true 
            }
        },
        
        --[[ Add titlebars to normal clients and dialogs
        { 
            rule_any = {
                type = { "normal", "dialog" }
            }, 
            properties = { titlebars_enabled = true }
        }, ]]--

        -- Popup CLI/GUI apps, setting the size and alignment (otherwise it just goes on the 
        -- side and is like super weird nya)
        --
        -- Also applies to some other things like blueman 
        {
            rule_any = {
                class = {
                    "cli_popup",
                    "gui_popup",
                    "blueman-manager",
                }
            },
            properties = {
                floating = true,
                focus = true,
                type = "dialog",
                placement = awful.placement.centered,
                titlebars_enabled = false,
                -- Honour size hints for basically popups nyaa
                size_hints_honor = true,
                ontop = true
            }
        },

            -- Set Firefox to always map on the tag named "2" on screen 1.
            -- { rule = { class = "Firefox" },
            --   properties = { screen = 1, tag = "2" } },
    }
	-- }}}
end

return rules

