local strutils = {}

function strutils.starts_with(prefix, str)
    return string.sub(str, 1, #prefix) == prefix
end

-- Split the string with the given pattern into chunks. Default is to split on spaces, with multiple spaces
-- being split only once (so `"a    b"` => `{"a", "b"}`
-- The pattern specifies what characters to suck up into each substring. e.g. to split on spaces 
-- use `"[^%s]+"`. The pattern is automatically captured for insertion.
function strutils.split_string(str, pattern_arg)
    local result = {}
    -- capture the pattern.
    local pattern = "(" .. (pattern_arg or "[^%s]+") .. ")"
    for substr in str:gmatch(str, pattern) do
        table.insert(result, substr)
    end
    return result
end

function strutils.join(the_table, sep)
    table.concat(the_table, sep)
end

--[[
Efficiently duplicate a given string a fixed number of times, using bitshifts rather than
multiplicative concatenations.
]]
function strutils.multiply_str(base_string, count)
    local curr_string = base_string
    local curr_count = count
    local curr_result = ""
    while curr_count > 0 do
        -- If the current bit is one, concatenate the current result with the
        -- current string multiplied by 2^current_bit_index
        
        if(curr_count & 1 > 0) then
            curr_result = curr_result .. curr_string
        end
        
        curr_string = curr_string .. curr_string
        curr_count = curr_count >> 1
    end
    return curr_result
end

--[[
Pad a chunk of content into a line of characters.
    `content` - The text to pad with other characters
    `character` - The characters to use in padding (default: `=`)
    `line_length` - The number of characters to pad to (default: `100`)
    `align` - "left", "right" or ("center" or "centre") (default: `"centre"`)

]]
function strutils.pad(content, character, line_length, align)
    character = character or "="
    line_length = line_length or 100
    align = align or "centre"
    if not set.set({"left", "right", "center", "centre"}):contains(align) then
        align = "centre"
    end
    if set.set({"center", "centre"}):contains(align) then
        -- We want to counter rounding errors by forcing the end to be of the appropriate
        -- length so we clamp to the # of chars afterwards and run it through left-padding
        local number_of_side_chars = math.floor((line_length - #content)/2)
        local pre_and_postfix = strutils.multiply_str(character, number_of_side_chars)
        local res = pre_and_postfix .. content .. pre_and_postfix
        -- :sub has inclusive bounds because OF COURSE IT DOES **LUA**
        res = res:sub(1, line_length)
        res = strutils.pad(res, character, line_length, "left")
        return res
    elseif align == "left" then
        return (content .. strutils.multiply_str(character, math.max(0, line_length - #content))):sub(1, line_length)
    elseif align == "right" then
        return (strutils.multiply_str(character, math.max(0, line_length - #content)) .. content):sub(1, line_length)
    end
end

--[[
Format a content of text such that it spans multiple lines, with a prefix allowed for the
first line and an indent for all the lines after (both calculated with the indent function)

This function returns a list of lines, where the indent is calculated based on linenumber (1-indexed),
by returning a string to glue on the line. 

The indent function should simply take the line number and return a table containing "value" (the string to
insert) and an optional "length_override" member to allow for stuff like formatting.

By default the character width is 100
By default the indent function indents by nothing.
]]
function strutils.indent_string(content_arg, character_width_arg, indent_func_arg)
    local content = content_arg
    local result = {}
    local lineno = 1
    local character_width = character_width_arg or 100
    local indent_func = indent_func_arg or function(lineno) return {value=""} end
    
    while #content > 0 do
        local this_indent = indent_func(lineno)
        local indent_length = this_indent.length_override or #(this_indent.value)
        local this_line_chunk = content:sub(1, character_width-indent_length)
        -- Chunk off that which was not used in this line
        content = content:sub(character_width-indent_length+1) 
        -- glue the indent and content up to the end of the text line.
        table.insert(result, this_indent.value .. this_line_chunk)
        lineno = lineno + 1
    end
    return result
end

return strutils