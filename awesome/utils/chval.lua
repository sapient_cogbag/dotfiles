require("imports")
local util = require("utils.util")
local registry = require("utils.registry")

local chval = {}

--[[ 
A value holder that triggers callbacks and updates only when it is written to by a large enough
change (default 0.001, to counter floating point error).

This also allows callbacks to be called on a change (if the value is set and not equal to current value),
and allows the object the change comes from to be specified such that the associated callback isn't called
on that object.

If the values are not numbers then they are just compared with equality/inequality.
Initial value defaults as 0
]]
chval.chval = class {
    _deltathreshold = class.NULL,
    _val = class.NULL,
    _change_cb = class.NULL,
    __init = function(self, initialvalue, delta_threshold)
        self._val = initialvalue or 0
        self._deltathreshold = delta_threshold or 0.001
        self._change_cb = {} -- Map of objects to callbacks.
    end,
    -- Add a callback to the given associated object.
    -- The callback takes the associated object and the new sufficiently changed value
    -- Returns an object-specific identifier for the callback
    add_callback = function(self, associated_object, cb)
        if self._change_cb[associated_object] == nil then
            self._change_cb[associated_object] = registry.registry()
        end
        return self._change_cb[associated_object]:register(cb)
    end,
    -- Remove a callback from an object using an identifier, returns the callback if it exists
    remove_callback = function(self, associated_object, callback_identifier)
        return self._change_cb[associated_object]:unregister(callback_identifier)
    end,
    _equal = function(self, value0, value1)
        if type(value0) == "number" and type(value1) == "number" then 
            return math.abs(value0 - value1) <= math.abs(self._deltathreshold)
        else
            return value0 == value1
        end
    end,
    -- Set a new value from an object. If this actually results in a value change, then the callbacks
    -- for all objects other than the one changing the value. If the object is nil of course, all 
    -- the callbacks for all objects will be called.
    set = function(self, object_from, new_value)
        if not self:_equal(new_value, self._val) then
            self._val = new_value
            for _associated_obj, callback_registry in pairs(self._change_cb) do
                if _associated_obj ~= object_from then
                    for _, cb in pairs(callback_registry) do
                        cb(_associated_obj, self._val)
                    end
                end
            end
        end
    end,
    -- Just gets the value lol
    get = function(self)
        return self._val
    end
}

return chval

