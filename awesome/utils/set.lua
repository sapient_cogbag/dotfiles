require("imports")

local set = {}

set.set = class.strict {
    elems_as_keys = class.NULL,
    __init = function(self, contents_arg)
        local contents = contents_arg or {}
        self.elems_as_keys = {}
        for _, val in pairs(contents) do
            self:insert(val)
        end
    end,
    insert = function(self, elem)
        self.elems_as_keys[elem] = true
    end,
    remove = function(self, elem)
        self.elems_as_keys[elem] = nil
    end,
    contains = function(self, elem)
        if elem == nil then return false end
        if self.elems_as_keys[elem] return true else return false end
    end,
    -- Returns self so you can easily chain union calls.
    union_in_place = function(self, other_set)
        for k, v in pairs(other_set.elems_as_keys) do
            if v then self:insert(k) end
        end
        return self
    end,
    --  Returns a proper clone of this set.
    clone = function(self)
        local other = set.set()
        return other:union_in_place(self)
    end,
    -- Allows iteration over each element in the set.
    -- The value condition is an optional internal thing to check the actual values of the keys.
    iterator = function(self, _value_condition) 
        _value_condition = _value_condition or function (v) return v end
        
        local v, in_set = next(self, nil)  -- get the first value in the set if there are any
        local function iter() 
            -- While there are more values in the set and the in_set indicator
            -- does not match the value condition...
            while v and not _value_condition(in_set) do
                -- Grab the next viable value
                v, in_set = next(self, v)
            end
            -- Either v is nil (no more set elements and this is the end of the iterator)
            -- or we have a new iterator output (the next element in the set with in_set
            -- matching the condition)
            return v
        end
        return iter
    end,
    -- Metatable injection lol
    __mt = {
        -- This does not filter internal elements.
        __pairs = function(self) 
            return pairs(self.elems_as_keys)
        end
    }
}

function set.intersection(set0, set1)
    local result = set.set()
    for e in set0:iterator() do
        if set1:contains(e) then result:insert(e) end
    end
    return result
end

function set.union(set0, set1)
    local result = set0:clone()
    result:union_in_place(set1)
    return result
end

return set