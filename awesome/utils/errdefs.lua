-- Simply contains more informative info for the `level` and `message` parameters
-- of the `error` function.

local err = {}
err.level = {}

-- Information about the location information stored in the error call 
-- which indicates how many levels "up the stack" we hold error information for
err.level.LOC_NONE = 0
err.level.LOC_ERR_CALL = 1
err.level.LOC_FUNCTION_CALL = 2

-- Simply do an assert and return val.
function err.level.LOC_STACK_LEVELS(count)
    assert(type(count) == "number", "Invalid Type")
    count = count or 2
    return count
end

-- Create an error about unimplemented abstract methods
function err.abc_message(class, method)
    error("Abstract method `"..class.."::"..method.."` not implemented by a child class")
end

-- Function that takes the class, the method (both as strings), and allows a table for args for documentation
-- and errors when the returned function is called.
-- (use {arg="doc"})
function err.abc(class, method, args)
    return function(self, ...)
        error(err.abc_message(class, method), err.level.LOC_FUNCTION_CALL)
    end
end

return err


