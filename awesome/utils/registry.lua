require("imports")
--[[
This is for general "register" and "unregister" operations with identifiers.
]]
local registry = {}

registry.registry = class.strict {
    _contents = class.NULL,
    __init = function(self) self._contents = {} end,
    -- Register a given object, returning an identifier table.
    register = function(self, object)
        local sentinel = {}
        self._contents[sentinel] = object
        return sentinel
    end,
    -- Unregister an object using it's identifier, returning the object if it existed.
    unregister = function(self, identifier)
        local result = self._contents[identifier]
        self._contents[identifier] = nil
        return result
    end,
    -- Get the object with the given identifier, nil if nothing registered with that.
    get = function(self, identifier)
        return self._contents[identifier]
    end,
    -- Metatable injection :3
    __mt = {
        __pairs = function(self)
            return pairs(self._contents)
        end
    }
}



return registry