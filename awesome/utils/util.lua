require("imports")

-- This is the actual imported table.
local util = {}

-- Toggle the "maximised" status of a window
function util.toggle_window_maximized(window_client)
    window_client.maximized = not window_client.maximized
end

--[[
Check if a value is in a set. If not, return the default value provided.
]]
function util.default_selection(valid_options, default_value, value)
    if not valid_options:contains(value) then
        return default_value
    end
    return value
end

--[[
Convert a bool-ish value to a boolean
]]
function util.to_bool(v)
    return (v and true) or false
end

--[[
Count the pairs in a table.

This does not do anything fancy and metatables can mess with this (to good effect)

Sadly it is also not very performant >.<
]]
function util.count_pairs(tbl)
    local cnt = 0
    for _, _ in pairs(tbl) do
        cnt = cnt + 1
    end
    return cnt
end

return util
