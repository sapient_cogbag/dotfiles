require("imports")
local util = require("utils.util")


local widget_util = {}

widget_util._private = {}

widget_util._private._data = {}
-- Private data for holding bound widget popups
-- map of parent_widget to various data
widget_util._private._data.bind_popup_to_widget = {}

widget_util.behaviour = {}

-- See the `awful.popup` code for roughly where this is from.
function widget_util.behaviour.show(popup, widget, geometry)
    popup:move_next_to(geometry)
    if not popup.visible then
        popup.visible = true
    end
end

function widget_util.behaviour.hide(popup, widget, geometry)
    if popup.visible then popup.visible = false end
end

function widget_util.behaviour.toggle(popup, widget, geometry)
    if not popup.visible then
        popup:move_next_to(geometry)
        popup.visible = true
    else
        popup.visible = false
    end
end


local function _popup_button_binding_function(the_widget, _, _, button, _, curr_widget_geo)
    for popup, popup_data in pairs(widget_util._private._data.bind_popup_to_widget[the_widget].per_popup) do
        local cb = popup_data.per_button_callbacks[button]
        if cb then cb(popup, the_widget, curr_widget_geo) end
    end
end

--[[
Like awful.popup.bind_to_widget but it toggles the popop on click instead of merely always showing.

Takes a "button" argument. Can be any standard mouse button, default is `1`

Takes a "behaviour" argument. Can be a member of `widget_util.behaviour`, or any arbitrary function taking 
the popup, widget, and geometry (which next_to can take to move next to the widget). 
Default is `widget_util.behaviour.toggle`

Note that only one of these behaviours can be bound at once for each button per associated popup
]]
function widget_util.bind_popup_to_widget(popup, parent_widget, button, behaviour) 
    button = button or 1
    
    -- Holds data for each widget
    widget_util._private._data.bind_popup_to_widget[parent_widget] = 
        widget_util._private._data.bind_popup_to_widget[parent_widget] or {
            has_bound_to_press_event = false, -- If the press event has actually been bound
            per_popup = {} -- per-popup data
        }
    
    local wdata = widget_util._private._data.bind_popup_to_widget[parent_widget]
    -- holds per-popup data, initialise for this popup
    wdata.per_popup[popup] =
        wdata.per_popup[popup] or {
            per_button_callbacks = {} -- indexed by button, to set the callbacks.
        }
        

    local wdata_popup = wdata.per_popup[popup]
        
    if not wdata.has_bound_to_press_event then
        parent_widget:connect_signal("button::press", _popup_button_binding_function)
    
        wdata.has_bound_to_press_event = true
    end
    
    behaviour = behaviour or widget_util.behaviour.toggle
    wdata_popup.per_button_callbacks[button] = behaviour
end


-- Unbind the behaviour for the given button
-- if all buttons have been unbound the private data related to this bit of the widget_util data is
-- erased to prevent leaks from frequent binding and unbinding.
function widget_util.unbind_popup_from_widget(popup, parent_widget, button)
    local wdata = widget_util._private._data.bind_popup_to_widget[parent_widget]
    if wdata then
        local wdata_popup = wdata.per_popup[popup]
        
        if wdata_popup then
            wdata_popup.per_button_callbacks[button] = nil
            -- Nothing left, clean up per-popup data
            if util.count_pairs(wdata_popup.per_button_callbacks) == 0 then
                wdata.per_popup[popup] = nil
            end
        end
        
        -- Nothing left, clean up widget data
        if util.count_pairs(wdata.per_popup) == 0 then
            widget_util._private._data.bind_popup_to_widget[parent_widget] = nil
            parent_widget:disconnect_signal("button::press", _popup_button_binding_function)
        end
    end
end

return widget_util