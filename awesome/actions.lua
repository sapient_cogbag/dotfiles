require("imports")


local actions = {}

-- Make a temporary file securely using the `mktemp` command nya
--
-- Call a callback with the temporary file filename as the only parameter
function actions.make_temporary_file(callback)
    awful.spawn.easy_async("mktemp", function (stdout, _stderr, exitreason, exitcode) 
        callback(stdout)
    end)
end

function actions.lock_screen()
    awful.spawn("light-locker-command -l")
end

function actions.window_selector()
    awful.spawn("rofi -show window")
end

function actions.audio_popup() 
    awful.spawn("kitty --class cli_popup --title 'Audio Config' pulsemixer")
end

function actions.bluetooth_popup()
    awful.spawn("blueman-manager")
end

function actions.unicode_input_basic_popup()
    -- We need to set Kitty's size, or it eats the whole screen nya
    -- This means we cannot simply use the easy_async spawning functions and pull stdout
    -- because they have no easy way to set size rules 
    -- (see: https://awesomewm.org/doc/api/libraries/awful.spawn.html#easy_async)
    -- 
    -- The kitty instance here uses a session thing, so that we reduce startup times 
    -- for entering unicode nya
    --
    -- This runs a shell that:
    --  * Writes the output of the unicode kitten to a temporary file nya
    --
    -- It sets the window size as well.
    --
    -- awful.spawn returns a PID (of the root kitty), so we then spawn another command 
    -- that waits for that PID and reads data back out from the file, passing it back
    -- to `xdotool type` - then removing the file to avoid clutter in /tmp
    --
    -- This is also necessary (as opposed to bundling the xdotool in the outer kitty instance)
    -- because if we did not wait for kitty to close, the active window would just be the kitty 
    -- unicode entry window, which defeats the point nya
    --
    -- Nasty hacks for waiting:
    --  https://unix.stackexchange.com/questions/427115/listen-for-exit-of-process-given-pid
    --
    -- Essentially, bash's `wait` builtin can *only wait for child processes*. Fortunately,
    --  GNU `tail` (why `tail`, idk, but still), enables doing this with 
    --  `tail --pid='$PID' -f /dev/null`
    -- 
    -- Seriously, what the fuck. Anyway! nya
    actions.make_temporary_file(function (tmpfile)
        pid, _ = awful.spawn({
            "kitty",
            "--class", "cli_popup",
            "--single-instance",
            "--override", "remember_window_size=no",
            "--title", 'Single Character Unicode Entry',
            "bash", "-c", 'kitty +kitten unicode_input >' .. tmpfile ..';'
        }, {
            width = 640,
            height = 480,
            -- We need to set this again here since the resize seems to happen after
            -- placement? nya
            placement = awful.placement.centered
        })

        awful.spawn({
            "bash",
            "-c",
            -- "echo " .. pid " > " .. tmpfile .. ";" ..
            -- "wait -f " .. pid .. ";"
            "tail -s 0.05 --pid=" .. pid .. " -f /dev/null;"
             .. "xdotool type \"$(cat " .. tmpfile .. ")\";"
             .. "rm " .. tmpfile .. ";"
        })
    end)
end

return actions
