---------------------------
-- Default awesome theme --
---------------------------

local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

local gfs = require("gears.filesystem")
local themes_path = gfs.get_themes_dir()
local gears = require("gears")


local theme = {}

theme.font          = "sans 8"

-- Same as terminal background :p
local very_dark_purple = "#080015"
local dark_purple = "#17003b"
local light_purple = "#200060"
local light_magenta = "#cc00ff"
local dark_pink = "#570460"
local light_pink = "#ff00bf"
local neon_pink_red = "#ff0051"
local accent_col = "#3b00a0"
local button_bgcol = "#29003f"
local medium_pink = "#890095"

local accent_orange_gtk_theme_style = "#ff5e00"

theme.accent_orange = accent_orange_gtk_theme_style
theme.bg_normal     = very_dark_purple
theme.bg_focus      = dark_purple

-- Titlebars blend with the windows
theme.titlebar_bg_focus = very_dark_purple

theme.bg_urgent     = accent_col
theme.bg_minimize   = light_purple
theme.bg_systray    = very_dark_purple

-- Title text and similar.
theme.fg_normal     = "#aaaaaa"
theme.fg_focus      = "#ffffff"
theme.fg_urgent     = "#ffffff"
theme.fg_minimize   = "#ffffff"

theme.useless_gap   = 0
theme.border_width  = dpi(2)
theme.border_normal = dark_pink
theme.border_focus  = medium_pink
theme.border_marked = dark_pink

-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- taglist_[bg|fg]_[focus|urgent|occupied|empty|volatile]
-- tasklist_[bg|fg]_[focus|urgent]
-- titlebar_[bg|fg]_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- mouse_finder_[color|timeout|animate_timeout|radius|factor]
-- prompt_[fg|bg|fg_cursor|bg_cursor|font]
-- hotkeys_[bg|fg|border_width|border_color|shape|opacity|modifiers_fg|label_bg|label_fg|group_margin|font|description_font]
-- Example:
--theme.taglist_bg_focus = "#ff0000"

-- Generate taglist squares:
local taglist_square_size = dpi(4)
theme.taglist_squares_sel = theme_assets.taglist_squares_sel(
    taglist_square_size, theme.fg_normal
)
theme.taglist_squares_unsel = theme_assets.taglist_squares_unsel(
    taglist_square_size, theme.fg_normal
)


-- Menu item stuff - see: https://awesomewm.org/doc/api/libraries/awful.menu.html#beautiful.menu_font
theme.menu_border_width = dpi(2)
theme.menu_border_color = medium_pink

-- Variables set for theming notifications:
-- notification_font
-- notification_[bg|fg]
-- notification_[width|height|margin]
-- notification_[border_color|border_width|shape|opacity]

-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_submenu_icon = themes_path.."default/submenu.png"
theme.menu_height = dpi(18)
theme.menu_width  = dpi(130)

theme.menu_bg_normal = very_dark_purple
theme.menu_bg_focus = dark_purple

-- Tasklist colours

-- Unfocused + unminimised
theme.tasklist_bg_normal = theme.menu_bg_normal
theme.tasklist_bg_minimize = theme.menu_bg_norma
theme.tasklist_bg_focus = light_purple
theme.tasklist_bg_urgent = "#ff0000"


-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
--theme.bg_widget = "#cc0000"

-- Define the image to load
theme.titlebar_close_button_normal = themes_path.."default/titlebar/close_normal.png"
theme.titlebar_close_button_focus  = themes_path.."default/titlebar/close_focus.png"

theme.titlebar_minimize_button_normal = themes_path.."default/titlebar/minimize_normal.png"
theme.titlebar_minimize_button_focus  = themes_path.."default/titlebar/minimize_focus.png"

theme.titlebar_ontop_button_normal_inactive = themes_path.."default/titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive  = themes_path.."default/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active = themes_path.."default/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active  = themes_path.."default/titlebar/ontop_focus_active.png"

theme.titlebar_sticky_button_normal_inactive = themes_path.."default/titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive  = themes_path.."default/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active = themes_path.."default/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active  = themes_path.."default/titlebar/sticky_focus_active.png"

theme.titlebar_floating_button_normal_inactive = themes_path.."default/titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive  = themes_path.."default/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active = themes_path.."default/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active  = themes_path.."default/titlebar/floating_focus_active.png"

theme.titlebar_maximized_button_normal_inactive = themes_path.."default/titlebar/maximized_normal_inactive.png"
theme.titlebar_maximized_button_focus_inactive  = themes_path.."default/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_active = themes_path.."default/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_active  = themes_path.."default/titlebar/maximized_focus_active.png"

--theme.wallpaper = themes_path.."default/background.png"
theme.wallpaper = "~/.wallpaper"

-- You can use your own layout icons like this:
theme.layout_fairh = themes_path.."default/layouts/fairhw.png"
theme.layout_fairv = themes_path.."default/layouts/fairvw.png"
theme.layout_floating  = themes_path.."default/layouts/floatingw.png"
theme.layout_magnifier = themes_path.."default/layouts/magnifierw.png"
theme.layout_max = themes_path.."default/layouts/maxw.png"
theme.layout_fullscreen = themes_path.."default/layouts/fullscreenw.png"
theme.layout_tilebottom = themes_path.."default/layouts/tilebottomw.png"
theme.layout_tileleft   = themes_path.."default/layouts/tileleftw.png"
theme.layout_tile = themes_path.."default/layouts/tilew.png"
theme.layout_tiletop = themes_path.."default/layouts/tiletopw.png"
theme.layout_spiral  = themes_path.."default/layouts/spiralw.png"
theme.layout_dwindle = themes_path.."default/layouts/dwindlew.png"
theme.layout_cornernw = themes_path.."default/layouts/cornernww.png"
theme.layout_cornerne = themes_path.."default/layouts/cornernew.png"
theme.layout_cornersw = themes_path.."default/layouts/cornersww.png"
theme.layout_cornerse = themes_path.."default/layouts/cornersew.png"


-- Generate Awesome icon:
--theme.awesome_icon = theme_assets.awesome_icon(
    --theme.menu_height, theme.bg_focus, theme.fg_focus
--)
theme.awesome_icon = theme_assets.awesome_icon(
    theme.menu_height, button_bgcol, theme.fg_focus
)


-- Define the icon theme for application icons. If not set then the icons
-- from /usr/share/icons and /usr/share/icons/hicolor will be used.
theme.icon_theme = nil

-- See: https://awesomewm.org/doc/api/classes/wibox.container.radialprogressbar.html
theme.radialprogressbar_color = accent_orange_gtk_theme_style --foreground

theme.radialprogressbar_border_color = theme.menu_border_color --background
theme.radialprogressbar_border_width = dpi(2) -- This is the width which makes standard
-- icons visible in the task bar. Larger widths break things.


-- Arccharts are like the above but with outlines
-- so now thickness here is what "border" was above.
-- https://awesomewm.org/doc/api/classes/wibox.container.arcchart.html

-- border colour
theme.arcchart_border_color = "#000000"
-- Default single-value property has this first-value colour be the only one set >.<
theme.arcchart_colors = {accent_orange_gtk_theme_style}
theme.arcchart_border_width = 0
theme.arcchart_paddings = 0
theme.arcchart_thickness = dpi(2)
-- Background of the radial bar itself.
theme.arcchart_bg= theme.menu_border_color


-- Checkmark thing: https://awesomewm.org/apidoc/widgets/wibox.widget.checkbox.html
theme.checkbox_border_width = theme.menu_border_width
theme.checkbox_bg = theme.bg_focus
theme.checkbox_border_color = theme.accent_orange

-- Checked bit has no border colour or width (instead we have a little gap
theme.checkbox_check_border_color = theme.checkbox_bg
theme.checkbox_check_border_width = dpi(1)
theme.checkbox_check_color = theme.accent_orange

-- Shapes are just rectangles :3
theme.checkbox_shape = gears.shape.rectangle
theme.checkbox_check_shape = gears.shape.rectangle

theme.checkbox_paddings = 0
-- checkbox_color is overridden by checkbox_check_border_color and checkbox_check_color



return theme

-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80
