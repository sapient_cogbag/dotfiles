require("imports")
local util = require("utils.util")
local set = require("utils.set")
local strutils = require("utils.strutils")
local registry = require("utils.registry")

-- Module table
local console = {}

-- Base class for an interpreter
-- This allows 
console.interpreter = class.strict {
    waiting_on = class.NULL, -- holds identifiers as keys with values of true.
    done_callbacks = class.NULL,
    __init = function(self) 
        self.waiting_on = set.set()
        -- Maps identifiers to callbacks for when the interpreter is done processing a chunk of code.
        self.done_callbacks = {}
    end,
    --[[
    Provide a new input text to the interpreter.
    Returns the running-code identifier.
    ]]
    provide_input = function(self, input_string) 
        local sentinel_identifier = {}
        self.waiting_on:insert(sentinel_identifier)
        self:start_input(sentinel_identifier, input_string)
        return sentinel_identifier
    end,
    --[[
    An overrideable method that should start off the processing.
    
    You can also do synchronous processing here and call finish_processing_input
    from this function if you want.
    ]]
    start_input = err.abc("console.interpreter", "start_input", {
        self = "",
        identifier = "An identifier object for the request",
        input_string = "The multiline string which is input for this request"
    }),

    --[[
    The method you should call when your asynchronous processing is done.
    identifier: The identifier passed to start_input and returned by provide_input
    that maps input and output together. This should also optionally take something to
    indicate an error occurred.
    output: The output produced by your processing system
    errored: Whether or not there was an error.
    ]]
    finish_processing_input = function(self, identifier, output, errored) 
        self.waiting_on:remove(identifier)
        for cb_iden, callback in pairs(self.done_callbacks) do
            callback(identifier, output, errored)
        end
    end,
    --[[
    Get a clone of the set of all currently waited-upon inputs, where the
    elements are the identifiers.
    ]]
    get_currently_processing = function(self) 
        return self.waiting_on:clone()
    end,
    --[[
    Add a callback to call when the output is done.
    This will return a callback identifier you can use to delete it later.
    
    The callback should take 3 arguments in the following order:
        - identifier - The identifying table returned by provide_input and passed to start_input
        - output - The output produced by your implementation processor
        - errored - Whether or not there was an error.
    ]]
    add_callback = function(self, callback)
        local cb_iden = {}
        self.done_callbacks[cb_iden] = callback
        return cb_iden
    end,
    --[[
    Delete the callback added by the call to add_callback, using the identifier returned from it.
    Returns the callback itself, or `nil` if it doesn't exist.
    ]]
    remove_callback = function(self, callback_identifier)
        local cb = self.done_callbacks[callback_identifier]
        self.done_callbacks[callback_identifier] = nil
        return cb
    end,
    --[[
    Abstract method to clear the interpreter environment entirely (should be equivalent to starting it from
    scratch. Note that if you don't want to be able to do this you can just make it do nothing :3
    ]]
    reset = err.abc("console.interpreter", "reset", {
        self = ""
    })
}

console.FINAL_OUTPUT_TYPE = {}
-- Markers for types of output from an output_parser
-- The first is plain text, the second means the output is
-- pongo markup: https://developer.gnome.org/pygtk/stable/pango-markup-language.html
console.FINAL_OUTPUT_TYPE.TEXT_OUTPUT = {}
console.FINAL_OUTPUT_TYPE.MARKUP_OUTPUT = {}

--[[
Converter of the raw output of an interpreter to markup or text.

This is an abstract base class.
]]
console.output_parser = class.strict {
    __init = function(self) end,
    --[[
    Get the processed result of the output parser for the console.
    
    Current specification is that you return a table with an element "result"
    containing the text (either plain or pango markup depending on output_type)
    and an element "type_v" which is one of 
    `console.FINAL_OUTPUT_TYPE.TEXT_OUTPUT`
    `console.FINAL_OUTPUT_TYPE.MARKUP_OUTPUT`
    indicating whether it's markup or plain text.
    
    It also should have an `errored` element which is true if there was an error and false otherwise.
    ]]
    get_output = err.abc("console.output_parser", "get_output", {
        self = "",
        interpreter_output_data = "The output produced by the interpreter",
        errored = "True if the interpreter indicates there was an error"
    })
}

console.immediate_commands = class.strict {
    prefix = class.NULL, -- The prefix is always case-sensitive. It's meant to be something like a colon anyhow
    commands = class.NULL, -- Map of command-sets to a table containing cmd_function and help_str
    -- Commands cannot in fact contain spaces. If you try they simply won't be found when you 
    __init = function(self, prefix, case_sensitive) 
        self.prefix = prefix or ":"
        self.commands = {}
        self.case_sensitive = case_sensitive or false
    end,
    -- Conditionally lowercase the string to search for it in command-key-sets depending on the setting
    normalise_alias = function(self, alias_str) 
        local res = alias_str
        if not self.case_sensitive then 
            res = res:lower()
        end
        return res
    end,
    --[[
    `command_strings` is a list of commands which can be added to call `command_function` with
    this immediate command object, the command function itself, the help string, and the
    full command string (including prefix).
    ]]
    add_command = function(self, command_strings_arg, command_function, help_string)
        local command_strings = set.set()
        for i, v in ipairs(command_strings_arg) do
            command_strings:insert(self:normalise_alias(v))
        end
        self.commands[command_strings] = {cmd_function = command_function, help_str = help_string}
    end,
    --[[
    Attempt to locate a table (that is a key) with the command string. This will return the first one it finds
    and take into account the case sensitivity. Returns nil if it cannot be found
    ]]
    find_command = function(self, command_str_arg) 
        local actual_command_str = self:normalise_alias(command_str_arg)
        
        local actual_key = nil
        for k, v in pairs(self.commands) do
            if k:contains(actual_command_str) then 
                actual_key = k
                break
            end
        end
        return actual_key
    end,
    --[[
    Find the first command set added with the given command string and remove all aliases to
    the command.
    ]]
    remove_command = function(self, command_str)
        local command_key = self:find_command(command_str)
        if command_key ~= nil then -- delete
            self.commands[command_key] = nil
        end
    end,
    --[[
    Remove an alias for a given command. This allows you to remove one of several command aliases while
    keeping the rest.
    ]]
    remove_alias = function(self, alias_str) 
        local command_key = self:find_command(alias_str)
        if command_key ~= nil then -- delete single alias
            command_key:remove(self:normalise_alias(alias_str))
        end
    end,
    --[[
    Add a new alias to an already existing one, where `current_alias` is the existing command alias
    and `new_alias` is the new alias to add for that command, which will be automatically normalised.
    
    If the existing alias doesn't exist or the new alias already exists in the set for the first 
    found command, it will do nothing.
    ]]
    add_alias = function(self, current_alias, new_alias) 
        local normalised = self:normalise_alias(new_alias)
        local command_key = self:find_command(self:normalise_alias(current_alias))
        if command_key ~= nil then
            command_key:insert(normalised)
        end
    end,
    --[[
    Return whether or not the given command string is an immediate command for this object.
    Note that this does not mean it is a real immediate command, but that the prefix exists on the string.
    ]]
    is_immediate_command = function(self, command_string) 
        return strutils.starts_with(self.prefix, command_string)
    end,
    --[[
    Scan the given string for a command and if it is found, process it and return `"true"`
    If it was not a real command, return false.
    ]]
    process_immediate_command = function(self, command_string)
        local spliced = strutils.split_string(command_string)
        if #spliced == 0 then return false end
        local command_str_undeprefixed = spliced[1] -- eww 1-based arrays >.<
        -- Cut off the prefix
        local command_str_deprefixed = string.sub(command_str_undeprefixed, #(self.prefix) + 1)
        local command_key = self:find_command(command_str_deprefixed)
        if command_key == nil then
            return false
        end
        -- Do actual processing
        local command_function = self.commands[command_key].cmd_function
        local help_str = self.command[command_key].help_str
        command_function(self, command_function, help_str, command_string)
        return true
    end,
    --[[
    Get help for the commands in this immediate command system.
    The output system is the same as `console.output_parser.get_output`.
    If both colours are nil, then we use plaintext, else we use pongo markup
    Consoles are assumed to use a monospace font and this will put the help in the 
    given number of characters (default is 80).
    
    `args` is a table for arguments: 
        `cmd_colour` is the colour of the command in the help (default `nil`)
        `help_colour` is the colour of the actual help messages for the commands (default `nil`)
        `charwidth` is the number of characters in width to calculate the text output (default `100`)
        `ident` is the number of spaces to indent each new line after the first (default `4`)
        `prelude` is the paragraph before listing commands. If nil, it will be omitted.
    ]]
    get_help = function(self, args)
        args = args or {}
        
        local cmd_colour = args.cmd_colour
        local help_colour = args.help_colour
        local charwidth = args.charwidth or 100
        local ident = args.ident or 4
        local prelude = args.prelude
        
        local is_plaintext = (cmd_colour == nil) and (help_colour == nil)
        
        local title = "Help"
        local title_pre = "= " .. title .. " "
        local extension_of_title = strutils.pad(title_pre, "=", charwidth, "left")
        
        local result_unpacked = {}
        result_unpacked.append = function(self, str) 
            table.insert(self, str)
        end
        
        result_unpacked:append(extension_of_title)
        if prelude then 
            result_unpacked:append(prelude)
            result_unpacked:append("")
        end
        
        
        local function wrap_cmdname(cmdname) 
            if cmd_colour ~= nil then
                return "<span foreground=\""..cmd_colour.."\">" .. cmdname .. "</span>"
            else
                return cmdname
            end
        end
        
        local helptext_prefix
        local helptext_postfix
        if help_colour ~= nil then
            helptext_prefix = "<span foreground=\""..help_colour.."\">"
            helptext_postfix = "</span>"
        else
            helptext_prefix = ""
            helptext_postfix = ""
        end
        
        local ident_str = strutils.multiply_str(" ", ident)
        
        for alias_set, command_data in pairs(self.commands) do 
            local cmd_names = {}
            for alias in alias_set:iterator() do
                table.insert(cmd_names, self.prefix .. alias)
            end
            
            local cmd_name_str = table.concat(cmd_names, " ") .. " "
            local cmd_help_str = command_data.help_str or ""
            local full_paragraph_table = strutils.indent_string(
                cmd_help_str,
                charwidth,
                function(lineno) 
                    if lineno == 1 then
                        return {
                            -- Has the helptext prefix in anticipation of it being added :3
                            value = wrap_cmdname(cmd_name_str)..helptext_prefix, 
                            length_override=#cmd_name_str
                        }
                    else 
                        return {value = ident_str}
                    end
                end
            )
            
            -- Glue the end of the help text wrapper on it.
            full_paragraph_table[#full_paragraph_table] = 
                full_paragraph_table[#full_paragraph_table] .. helptext_postfix
                
            for _, v in ipairs(full_paragraph_table) do
                table.insert(result_unpacked, v)
            end
        end
        table.insert(result_unpacked, strutils.multiply_str("=", charwidth)) 
        
        local type_v = console.FINAL_OUTPUT_TYPE.TEXT_OUTPUT
        if not is_plaintext then
            type_v = console.FINAL_OUTPUT_TYPE.MARKUP_OUTPUT
        end
        return {
            type_v = type_v,
            result = table.concat(result_unpacked, "\n"),
            errored = false
        }
    end
}


-- Class for internal (nongui) console representation. 
-- Can be persistent across views or transient each view depending on preference.
console.console = class.strict {
    interpreter = class.NULL,
    output_parser = class.NULL,
    immediate_command_processors = class.NULL,
    output_feed_callbacks = class.NULL,
    __init = function(
        self,
        interpreter, -- A console.interpreter to run with
        output_processor -- A console.output_parser to run with
    )
        self.interpreter = interpreter
        self.output_parser = output_processor
        self.immediate_command_processors = {}
        self.output_feed_callbacks = registry.registry()
        
        self.interpreter:add_callback(function(parse_id, result, errored) 
            self:processing_done(parse_id, result, errored)
        end)
    end,
    resort_immediate_command_processors = function(self) 
        table.sort(self.immediate_command_processors, function(proc_0, proc_1) 
            return #(proc_0.prefix) < #(proc_1.prefix)
        end)
    end,
    --[[
    Add an immediate command processor to this object.
    `immediate_command_processor` is a `console.immediate_commands`
    
    The command processors are sorted in such a way that those with more specific 
    prefixes are processed first (e.g `::` will be checked before commands in `:`).
    
    Two processors with the same prefix are evaluated in an undefined order.
    ]]
    add_immediate_command_processor = function(self, immediate_command_processor) 
        table.insert(self.immediate_command_processors, immediate_command_processor)
        self:resort_immediate_command_processors()
    end,
    --[[
    Add callbacks for all produced outputs.
    `callback` should take 3 arguments:
        - `output_data` : an object in the format specified by `console.output_parser.get_output`
        - `errored` : a boolean specifying whether the output indicates an interpreter error
        - `input_identifier` : An identifier for the command processed by the interpreter. If a
            command was an immediate command, or otherwise unassociated with interpreter input,
            this will be `nil`
    ]]
    register_output_callback = function(self, callback)
        return self.output_feed_callbacks:register(callback)
    end,
    unregister_output_callback = function(self, identifier)
        return self.output_feed_callbacks:unregister(identifier)
    end,
    --[[
    Do The Stuff for a given command.
    If it turns out to be an interpreter command rather than an immediate command,
    then we return the identifier for the given command being run.
    ]]
    run = function(self, str) 
        for idx, immediate_processor in ipairs(self.immediate_command_processors) do
            -- See if we start with the prefix
            if immediate_processor:is_immediate_command(str) then
                local did_run = immediate_processor:process_immediate_command(str)
                -- Break out if we found one that ran
                if did_run then 
                    return nil
                end
            end
        end
        -- Pass the command off to the real interpreter.
        return self.interpreter:provide_input(str)
    end,
    --[[
    Provide raw output to this console. Params:
        - `output_data` : an object in the format specified by `console.output_parser.get_output`
        - `errored` : a boolean specifying whether the output indicates an interpreter error
        - `input_identifier` : An identifier for the command processed by the interpreter. If a
            command was an immediate command or otherwise unassociated with interpreter input, 
            this should be `nil`
    ]]
    output = function(self, output_data, errored, input_identifier)
        for idx, callback in pairs(self.output_feed_callbacks) do
            callback(output_data, errored, input_identifier)
        end
    end,
    --[[
    A callback for the processor to call when it is done processing a given chunk of code.
    
    `parse_id` is an identifier that was passed to the interpreter and allows matching requests
        and results. It is usually a sentinel table.
    `result` is the output of the interpreter. It will be parsed by the output parser into a standardised
        format
    `errored` is should be true if the result is error, false otherwise
    ]]
    processing_done = function(self, parse_id, result, errored) 
        local output_data = self.output_parser:get_output(result, errored)
        self:output(output_data, errored, parse_id)
    end,
}

return console
