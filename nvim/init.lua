-- Initial lua module.
--
-- `reload` is the only module that never gets touched for reloading. Unfortunately there is not a good way to get the root module
-- so we just cheat by putting everything in `cfg`

local cfg = require("cfg")
cfg.init()

