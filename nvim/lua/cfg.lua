-- Actual entry point - which is reloadable
local cfg = {}

-- Initialise log before everything else. 
--
-- Avoids issues before log.init() is called, in epoch pre-initialisers nya
cfg.log = require("cfg.log")
cfg.log.init()

cfg.clean_plugin_pkgs = require("cfg.clean_plugin_pkgs")

cfg.async = require("cfg.async")
cfg.libuv_async = require("cfg.libuv_async")
cfg.vim_async = require("cfg.vim_async")

cfg.keybinds = require("cfg.keybinds")
cfg.audebouncer = require("cfg.audebouncer")
cfg.theming = require("cfg.theming")
cfg.utils = require("cfg.utils")
cfg.ui = require("cfg.ui")
cfg.lsp = require("cfg.lsp")

cfg.plugins = require("cfg.plugins")

function cfg.init()
    cfg.clean_plugin_pkgs.init()

    cfg.vim_async.init()
    cfg.audebouncer.init()
    cfg.keybinds.init()
    cfg.theming.init()
    cfg.utils.init()
    cfg.ui.init()
    cfg.lsp.init()

    cfg.plugins.init()
end

return cfg
