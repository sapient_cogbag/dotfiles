--- Implementation of some libuv filesystem stuff.
local fs = {}
local uv = vim.loop
local async = require("cfg.async")

---Run `uv.fs_stat` asynchronously.
---@param path string
---@return Future<{err: string?, result: table?}>
function fs.stat(path)
    return async.future.new(function()
        --- @param outer_fut Future
        --- @param outer_bundle any
        local output_or_err, _bundle = coroutine.yield(function(outer_fut, outer_bundle)
            uv.fs_stat(path, function(err, stat)
                outer_fut:resume({ err = err, result = stat }, outer_bundle)
            end)
        end)
        return output_or_err
    end)
end

return fs
