--- File for libuv streams and stream-ish stuff nya
--- Default libuv streams are bidirectional. However, that's weird for us since
--- we use one-way streams in most cases nya.
--- So we wrap with a userdata indicating direction, and mirror the stream interface
--- we ourselves have defined.
---
--- @alias libuv_async.stream.Sender.write_data string | string[]
local stream = {}
local uv = vim.loop
local async = require("cfg.async")

--- @class libuv_async.stream.Sender: async.stream.spsc.AsyncSender
--- @field private inner_stream unknown uv_stream_t handle
stream.sender = {}

--- @param uv_stream unknown # a uv_stream_t handle that should be write-only.
--- @return libuv_async.stream.Sender tx async stream with items of type
---   libuv_async.stream.Sender.write_data, which is either a single string or an
---   array of strings nya
function stream.sender.new_from_uv(uv_stream)
    return setmetatable({ inner_stream = uv_stream }, { __index = stream.sender })
end

--- @param message libuv_async.stream.Sender.write_data Data to send, in the format
---    expected by libuv's uv.write - that is, either a string, or a list of strings.
--- @param allow_failure bool? Whether or not to allow failure. Default false (aka throw with
---    error().
--- @return Future<string?> err If errors are actually allowed, then this function yields a
---    string on error, nil otherwise nya
function stream.sender:send(message, allow_failure)
    return async.future.new(function()
        --- @param outer_fut Future
        --- @param outer_bundle any
        local maybe_err = coroutine.yield(function(outer_fut, outer_bundle)
            self.inner_stream:write(message, function(maybe_err)
                outer_fut:resume(maybe_err, outer_bundle)
            end)
        end)

        if maybe_err and not allow_failure then error(maybe_err) end
        return maybe_err
    end)
end

--- Close this stream, asynchronously. This will wait for any pending write requests.
--- If there was an error while shutting down, it will return a non-nil value that is
--- a string (note that for default in-memory streams, there will never be an error nya
--- @async
--- @return Future<string?> maybe_err Non-nil if there was an error.
function stream.sender:close()
    return async.future.new(function()
        ---@param outer_fut Future
        ---@param outer_bundle any
        local maybe_err, _ =  coroutine.yield(function(outer_fut, outer_bundle)
            self.inner_stream:shutdown(function(maybe_err)
                outer_fut:resume(maybe_err, outer_bundle)
            end)
        end)
        return maybe_err
    end)
end

return stream
