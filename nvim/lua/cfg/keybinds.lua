local reload = require("reload")
local edit_mode = require("cfg.modes").edit_mode
local dmode = require("cfg.modes").detected_mode
local log = require("cfg.log").with_prefix("keybinds")
local plugins = require("cfg.plugins")
local if_nil = require("cfg.utils").if_nil

local keybinds = {}

-- list of things to do on unload - registration just extends a function
local on_unload_hooks = {}
local function on_unload(hook)
    table.insert(on_unload_hooks, hook)
end

-- Calls vim.keymap.set, but also returns a function to delete the mapping nya
local function keymap(mode, lhs, rhs, opts)
    vim.keymap.set(mode, lhs, rhs, opts)
    return function()
        -- Buffer is the only option from set opts that is
        -- accepted by del nya
        --
        -- This makes sure that the buffer, if specified, is valid at time of deletion.
        if opts.buffer ~= nil then
            if not vim.api.nvim_buf_is_valid(opts.buffer) then return end
        end
        local ok, res_or_err = pcall(vim.keymap.del, mode, lhs, { buffer = opts.buffer })
        if not ok then 
            log.info("Failed to delete keybinding: %s", res_or_err)
        end
    end
end

function keybinds.nnoremap(source, target, desc)
    local opts = nil
    if desc ~= nil then
        opts = { desc = desc }
    end
    vim.keymap.set(edit_mode.NORMAL, source, target, opts)
    on_unload(function()
        vim.keymap.del(edit_mode.NORMAL, source, {})
    end)
end

-- Must be called before all mapping.
--
-- Sets the leader keys (vim.g.mapleader and maybe vim.g.maplocalleader)
function keybinds.leaders()
    vim.g.mapleader = ','
end

-- See: https://neovim.io/doc/user/lua.html#vim.keymap.set()
-- Important: defaults to `noremap`

function keybinds.reload()
    log.info("Loading config reload mechanism")
    keybinds.nnoremap("<leader>lv", function()
        reload.reinit()
    end)
end

-- Edit our neovim config
--  Fuzzy select for *which* vim config lua file to edit nya
function keybinds.configopen(fzf_lua)
    log.info("Adding vim config edit mechanism")
    keybinds.nnoremap("<leader>ev", function()
        fzf_lua.files({
            cwd = "~/.config/nvim",
            actions = { ["default"] = fzf_lua.actions.file_switch_or_edit }
        })
    end, "Search for a vim configuration file to edit")
    keybinds.nnoremap("<leader>ep", function()
        fzf_lua.files({
            cwd = plugins.plugin_location(),
            actions = { ["default"] = fzf_lua.actions.file_switch_or_edit }
        })
    end, "Search for plugin files and look at them. Useful for ideas and etc.")
end

-- Keybinds for window navigation and management
--
-- Global mappings
function keybinds.window_navigation()
    log.info("Loading window navigation")
    local function arrowkey(arrow)
        keybinds.nnoremap("<S-" .. arrow .. ">", "<C-w><" .. arrow .. ">")
    end

    arrowkey("Right")
    arrowkey("Left")
    arrowkey("Up")
    arrowkey("Down")


    -- Resizing horizontally
    log.info("Loading resizing shortcuts")
    keybinds.nnoremap("<S-o>", ":resize +2<CR>", "Increase window height")
    keybinds.nnoremap("<S-p>", ":resize -2<CR>", "Decrease window height")

    -- Resizing vertically
    keybinds.nnoremap("+", ":vertical resize +2<CR>", "Increase window width")
    keybinds.nnoremap("-", ":vertical resize -2<CR>", "Decrease window width")

    log.info("Loading split shortcuts")
    keybinds.nnoremap("<leader>v", ":vsplit<CR>", "Split vertically")
    keybinds.nnoremap("<leader>s", ":split<CR>", "Split horizontally")

    log.info("Configuring vim to vertically split right rather than left")
    vim.o.splitright = true
end

-- Yes I know i should just use the defaults but I've had this keybinding for ages
function keybinds.folds()
    log.info("Loading fold shorthands")
    -- this should toggle folds
    keybinds.nnoremap("<leader>h", "za")

    -- enables or disables all folds.
    -- We're going to try without this for a bit
    -- s.nnoremap("<leader>hh", "zi")
end

function keybinds.fileopen(fzf_lua)
    log.info("Making keybindings for fzf file opening and buffer switching")
    -- Note that ideally we'd do enter for edit and shift-enter for
    -- vsplit. However shift-enter is not supported by fzf
    keybinds.nnoremap("<leader>ef", function()
        fzf_lua.files({ actions = { ["default"] = fzf_lua.actions.file_switch_or_edit } })
    end, "Fuzzy-search a file to edit (or switch to if it exists)")
    -- Legacy binding
    keybinds.nnoremap("<leader>of", function()
        fzf_lua.files({ actions = { ["default"] = fzf_lua.actions.file_switch_or_edit } })
    end, "Fuzzy-search a file to edit (or switch to if it exists) (legacy binding, same as `,ef`)")

    keybinds.nnoremap("<leader>eb", function()
        fzf_lua.buffers({ actions = { ["default"] = fzf_lua.actions.buf_switch_or_edit } })
    end, "Fuzzy search a buffer to edit (or switch to if it exists)")
end

function keybinds.nvim_cmp(cmp)
    local mp = cmp.mapping
    -- returns a function that handles normal falling back
    local function wrap_with_visibility_check(normal_mapping)
        return function(fb)
            if cmp.visible() then
                normal_mapping(fb)
            else
                fb()
            end
        end
    end

    return {
        mapping = {
            -- behaviour = "select" means it doesn't fill in the text and
            -- risk fucking up the buffer
            --
            -- In the case of right arrow, this select = true means that the first will
            -- be auto selected if nothing is nya
            -- Force completion to happen
            ['<C-Space>'] = mp.complete(),
            ['<CR>'] = wrap_with_visibility_check(mp.confirm()),
            -- ['<S-Tab>'] = wwf(mp.select_prev_item({behavior = "select"})),
            -- ['<Tab>'] = wwf(mp.select_next_item({behavior = "select"})),

            ['<Up>'] = wrap_with_visibility_check(mp.select_prev_item({ behavior = "select" })),
            ['<Down>'] = wrap_with_visibility_check(mp.select_next_item({ behavior = "select" })),

            ['<S-Up>'] = wrap_with_visibility_check(mp.scroll_docs(-3)),
            ['<S-Down>'] = wrap_with_visibility_check(mp.scroll_docs(3)),

            -- Left is an abort but also an indication to go back
            ['<Left>'] = function(fb)
                if cmp.visible() then
                    cmp.abort()
                    fb()
                else
                    fb()
                end
            end,
            --
            ['<Right>'] = wrap_with_visibility_check(function(fb)
                -- If we have not selected anything actively, we want to abort and move
                -- right (apply fb).
                if not cmp.get_active_entry() then
                    cmp.abort()
                    fb()
                else
                    cmp.confirm({ select = true })
                    -- I *want* to do this but it deletes things afterwards, probably
                    -- due to cmp internals nya
                    -- fb()
                end
            end),

            -- Autoselect with shift-right nya
            -- We tried C-Right but it conflicts with using control for the purposes of navigating text nya
            -- This will select the first one even if it is not autoselected.
            ['<S-Right>'] = wrap_with_visibility_check(function(fb)
                cmp.confirm({ select = true })
                -- fb() -- I want to do this but it breaks things :(
            end),

            -- Unnecessary since we use `select`, and the buffer is unmodified until
            -- enter or ->.
            --
            -- We did try with this keybinding but it interfered with fast esc + :w/q/etc.
            -- commands nya
            --
            -- Instead we require active eff
            -- ['<Esc>'] = wwf(mp.abort())
        }
    }
end

function keybinds:__unload()
    for idx, hook in ipairs(on_unload_hooks) do
        hook()
    end
    on_unload_hooks = {}
end

-- LSP Keybindings with event
function keybinds.lsp()
    ---@module "cfg.lsp"
    local lsp = require("cfg.lsp")


    local mapping = {
        ['<leader>ca'] = {
            function()
                plugins.loaded_or_nil("fzf-lua").lsp_code_actions()
            end,
            desc = "See LangServer code actions at the current position or selection"
        },
        ['<leader>cf'] = {
            function()
                vim.lsp.buf.format()
            end,
            desc = "Format the code in this file (either the selection or the whole lot)"
        },
        ['<leader>ce'] = {
            function()
                plugins.loaded_or_nil("fzf-lua").lsp_document_diagnostics()
            end,
            desc = "List errors in the document"
        },
        ['<leader>cE'] = {
            function()
                plugins.loaded_or_nil("fzf-lua").lsp_workspace_diagnostics()
            end,
            desc = "List errors in the workspace"
        },
        ["<leader>ci"] = {
            "<cmd>LspHover<CR>",
            desc = "Manually request an LSP hover ^.^"
        },
        ['<leader>or'] = {
            function()
                plugins.loaded_or_nil("fzf-lua").lsp_references()
            end,
            desc = "Search for references to the current thing."
        },
        ['<leader>os'] = {
            function()
                plugins.loaded_or_nil("fzf-lua").lsp_document_symbols()
            end,
            desc = "Search all symbols in document (some lang servers will not include doc symbols in workspace symbols)"
        },
        ['<leader>oS'] = {
            function()
                plugins.loaded_or_nil("fzf-lua").lsp_workspace_symbols()
            end,
            desc = "Search workspace symbols"
        },
        ['<leader>r'] = {
            function()
                vim.lsp.buf.rename()
            end,
            desc = "Rename symbol"
        }
    }



    local epoch = reload.epoch:get(keybinds)
    lsp.submod.autocmds.register_lsp_buffer_action({
        on_enter_lsp = function(bufnr)
            -- Construct buffer-local keybinds
            ---@type table[]
            local deletion_hooks = {}

            -- Wraps keymap with a table and defaults nya
            -- Also adds the deletion function to the deletion_hooks
            local function buf_keymap(lhs, specification)
                local mode = vim.deepcopy(if_nil(specification.mode, edit_mode.DEFAULT))
                local rhs = vim.deepcopy(specification[1])
                local opts = vim.deepcopy(if_nil(specification.opts, {}))
                opts.buffer = bufnr
                opts.desc = if_nil(specification.desc, opts.desc)
                table.insert(deletion_hooks, keymap(mode, lhs, rhs, opts))
            end

            for lhs, specification in pairs(mapping) do
                buf_keymap(lhs, specification)
            end

            -- Destructors are simply called in the detach area :) nya
            return deletion_hooks
        end,
        on_exit_lsp = function(bufnr, deletion_hooks)
            if not vim.api.nvim_buf_is_valid(bufnr) then return end
            vim.schedule(function()
                for _idx, deletion_hook in ipairs(deletion_hooks) do
                    deletion_hook()
                end
            end)
        end,
        desc = "LSP keybinds"
    }, epoch)
end

---Create hop keybinds.
---@param hop table hop module
---@param epoch Epoch Current epoch nya
local function jumparound(hop, epoch)
    ---@module "hop"
    hop = hop
    if epoch:in_epoch() then
        local function map(lhs, rhs, opts)
            vim.keymap.set('', lhs, rhs, opts)
            epoch:cleanup(function()
                vim.keymap.del('', lhs, { buffer = opts.buffer })
            end)
        end

        -- Method that retrieves a regex-escaped char from the user using the same method as hop nya
        -- Acts to make it case-insensitive
        ---@param message string message for the user nya
        ---@return string?
        local function get_one_char_escaped(message)
            -- Note that this is not stable - we use an internal function `hop.get_input_pattern` - to
            -- obtain characters to then plug in to the jump target which uses the regex to pick matches.
            --
            local char = hop.get_input_pattern("Hop 1 char: ", 1)
            if not char then return end
            -- Taken from some stuff in hop somewhere nya
            local bare_escaped_char = vim.fn.escape(char, '\\/.$^~[]')
            -- Make uppercase
            local uppercase = vim.fn.toupper(bare_escaped_char)
            -- Then, combine the patterns such that when the input is lowercase
            -- we end up case-insensitive, but if it's uppercase, then it's not nya)
            --
            -- %( is non-capturing group ^.^
            local pattern_merged = "\\%(" .. bare_escaped_char .. "\\|" .. uppercase .. "\\)"
            return pattern_merged
        end

        -- \<\> ==> match start and end of word without including the character afterward in the match nya

        local function regex_word_starting_with(escaped_start)
            return "\\<" .. escaped_start .. "\\k*"
        end

        local function regex_word_ending_with(escaped_end)
            return "\\k*" .. escaped_end .. "\\>"
        end

        local jump_target = require "hop.jump_target"
        local hop_hints = require "hop.hint"
        local default_opts = require "hop.defaults"
        local function override_opts(opts)
            return setmetatable(opts, { __index = default_opts })
        end

        map("f", function()
            local i = get_one_char_escaped("First character of word:")
            if i == nil then return end

            -- I think there's a bug in hop where it only overrides options when the passed option
            -- is nil.. rather than when it isn't. So we cheat and use the default options for all
            -- the random stuff we don't want to set nya
            --
            -- TODO: when hop is fixed, make this a normal table :)
            local opts = override_opts({
                hint_position = hop_hints.HintPosition.BEGIN,
                multi_windows = true
            })


            -- Not a plain search, we are using an actual regex, hence the false nya
            local regex_object = jump_target.regex_by_case_searching(regex_word_starting_with(i), false, opts)
            hop.hint_with(jump_target.jump_targets_by_scanning_lines(regex_object), opts)
        end, { desc = "jump to the start of a word in the buffer, after typing in a filter character" })

        map("t", function()
            local i = get_one_char_escaped("Last character of word:")
            if i == nil then return end

            -- I think there's a bug in hop where it only overrides options when the passed option
            -- is nil.. rather than when it isn't. So we cheat and use the default options for all
            -- the random stuff we don't want to set nya
            --
            -- TODO: when hop is fixed, make this a normal table :)
            local opts = override_opts({
                hint_position = hop_hints.HintPosition.END,
                multi_windows = true,
                hint_offset = 1
            })

            -- Not a plain search, we are using an actual regex, hence the false nya
            local regex_object = jump_target.regex_by_case_searching(regex_word_ending_with(i), false, opts)
            hop.hint_with(jump_target.jump_targets_by_scanning_lines(regex_object), opts)
        end, { desc = "jump to one-after the end of a word in the buffer, after typing a filter character" })
    end
end

function keybinds.init()
    keybinds.leaders()
    keybinds.reload()
    keybinds.window_navigation()
    keybinds.folds()
    keybinds.lsp()
    local epoch = reload.epoch:get(keybinds)

    plugins.register_callback("fzf-lua", function(fzf_lua)
        keybinds.fileopen(fzf_lua)
        keybinds.configopen(fzf_lua)
    end)

    plugins.register_callback("nvim-cmp", function(nvim_cmp)
        return keybinds.nvim_cmp(nvim_cmp)
    end)

    plugins.register_onloaded("hop", function(hop)
        ---@module "hop"
        hop = hop
        hop.setup({})
        -- Link multi-sequence chars to the first char highlight.
        -- I have difficulty with them when the 2nd and further chars are low contrast
        -- so just put it to max nya
        -- Also, make it so that the second and after characters are the same colour as second
        -- chars, so that when the first in a sequence is pressed all the next keys are clearly
        -- marked nya
        vim.api.nvim_set_hl(0, "HopNextKey2", { link = "HopNextKey" })
        jumparound(hop, epoch)
    end)

    log.info("keybindings loaded")
end

return keybinds
