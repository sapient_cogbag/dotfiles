--- @alias async.future.ResumeScheduler fun(outer_future: Future, bundle: any) Means of scheduling an arbitrary outer future to be
---     resumed with an appropriate value for the current innermost future, asynchronously.
--- @class async.future.MultiResumeElement
---  One element in a multi-element scheduler function + bundle array
--- @field sched async.future.ResumeScheduler
--- @field bundle any?
---
--- @alias async.future.ResumeSchedulerOrMulti async.future.ResumeScheduler|async.future.MultiResumeSchedulerList
--- | # Either a resume scheduler (bundle is separate), or the multi resume scheduler + bundle list nya
---
--- @alias async.future.ContinuationMode # How to run the resume scheduler function. "jumpstart" means immediately, otherwise
--- | # we provide a function that could e.g. add it to a runtime queue. This must be able to be run more than once simultaneously
--- | # for a given outer future with different resume mechanisms, as multi-scheduled resumers can use this nya
--- | "jumpstart" Jumpstart mode - immediately run the scheduler.
--- | fun(resume_scheduler: async.future.ResumeScheduler, current_outer_future: Future, bundle: any)
---
--- @generic T 
--- @type table|fun(async_body: fun(...): `T`): fun(...): Future<T>
--- Our own little async implementation.
--- Lua has a very flexible coroutine infrastructure for near-arbitrary call inversion.
--- This is pretty awesome, but works more as a grounds from which to build rather then an immediately
--- useable async module nya.
--- async.lua is my own (of many that exist) implementation of some asynchronous structures for lua.
local async = {}

local co = coroutine

--- Contains some useful constants for async
async.constants = {}
--- When yielding multiple resume scheduler, bundle pairs, you must tag it with this metatable. This avoids more complex tests of the callability
--- of resume_scheduler.
--- @class async.future.MultiResumeSchedulerList: async.future.MultiResumeElement[]
async.constants.MULTI_RESUME_SCHED_METATABLE = {}


--- Module for generating async functions (that is, functions returning a future) from function bodies with coroutine
--- or futures internally nya, as well as some useful transformations and utility functions for building futures from
--- scratch.
async.fn = {}

--- @alias future_yield_args # The structure of the data that should be yielded from futures' coroutines in the async protocol
---  | nil # Immediately resume with no data/bundle/etc..
---  | async.future.ResumeScheduler, any # Emit a resume scheduler and a second value that is the bundle.
---  | async.future.MultiResumeSchedulerList # List of resume scheduler and bundle pairs.
---
---@generic Output
---@class Future<Output>
---@field protected _inner_coro thread
---@field protected _started bool Once this is true, any function that starts the future will error
---@field protected _done bool Once this is true, the future has returned a value and further resumes are errors.
---@field protected _native_continuation_mode async.future.ContinuationMode The default continuation mode for this when it's the outermost
---  future - default is jumpstart
---@field protected _wrap_bundle? fun(inner_bundle: any): any Dictate how our bundle should be produced from an inner bundle. If nil, this will save lots of effort on multis
---@field protected _unwrap_bundle? fun(our_bundle: any): any Extract the bundle to be passed inward from our bundle. If nil, this will save lots of effort on multis.
---
--- Futures represent a hypothetical future value along with all computation required to derive it.
--- In a lot of lua libraries, futures are implemented as described [here](https://ms-jpq.github.io/lua-async-await/)
--- We don't quite do that, but we don't do something *completely* out there.
--- When awaiting a future, firstly, there is an initial resume with no arguments - this just serves to start the function
--- going and e.g. get to the first yield or return immediately.
---
--- If the coroutine is complete, then the output is returned as the result of the future into the outer coroutine.
---
--- If the coroutine is incomplete, then it must yield data in the pattern specified with `future_yield_args`:
--- @see future_yield_args
---     * If this is nil, then the coroutine is resumed again immediately.
---     * If the first yielded value is marked via async.constants.MULTI_RESUME_SCHED_METATABLE, then it is treated as an array of pairs of
---         resume_scheduler functions and bundles for each. Each of these is handled in the same way as the typical method of yielding
---         one resume_scheduler and a second value that is the bundle to pass upward and attach it to.
---         @see async.fn.mark_multi_resume_scheduler_table
---     * Else, the inner future coroutine must yield two values.
---
---       The first is a single resume_scheduler function, which must schedule an asynchronous call to Future:resume(), with the next input for
---       this inner function, on an *arbitrary* outer future with an *arbitrary* outermost bundle, each given as an
---       argument to the resume_scheduler function nya
---
---       The second yielded value is a bundle that gets passed up through the chain of futures awaiting on the current one - on the way upward, a future can
---       provide further modifications and wrappings - as long as it can always re-obtain an inner bundle to pass back down into the futures it is waiting on.
---       This bundle mechanism is of primary use for tagging futures that need to combined or otherwise run in parallel, as
---
--- Resume schedulers are an interesting feature of our type of future. In particular, *something* has to call the callback that
--- is yielded with the outermost future as a parameter nya. There are two different ways this can be done:
--- * Jumpstart - the first resume scheduler is called at the initial point at which the future is executed, but Future:resume, after resuming
---   the inner coroutine and receiving the next resume scheduler + bundle, calls said resume mechanism itself. This is called "jumpstart" because
---   the first resume starts a "chain" of callback registration and further resume calls nya.
--- * Runtime - In this case, Future:resume does not automatically call the next resume mechanism, instead, it is dumped on a queue for some
---   internal runtime to call. Or, for instance, it could be called via vim.schedule or something.
--- These are mostly non-relevant for futures in the middle of the stack, which are usually going to be called by the :await() function and
---  have values transferred up and down nya. However, it's important to understand for futures that compose other futures ^.^. These
---  modes are specified as a parameter to Future:resume().
---
--- Note that our internal mechanisms only really support single arguments and results, so use tables! nya However you can also pack up bundles for composition.
--- When scheduling and resuming a future, the outermost future's wrapping and unwrapping of bundles still occurs just fine.
---
--- For direct implementors of futures:
--- * The futures protocol is composable without wrapping inner methods as a future. That is, bare implementation bodies can theoretically be
---   used as long as you do an initial resume, and as long as the yields follow the futures protocol (i.e. contain either nil or a resume mechanism +
---  optional second bundle yielded, and obtain the value and the bundle returned back). However this is very manual and not recommended nya.
--- * If integrating with other async or synchronous apis, channels may be the easiest mechanism - as opposed to, say, trying to manage their coroutines
---   yourself. Though really, any will work nya
--- * For composing futures, the `start_without_calling_mechanism` and `resume_without_calling_mechanism` functions are likely to be very useful, as they provide
---   the ability to granularly progress a future, with less risk of forgetting things like cycling away nil resume mechanisms, marking futures as started/done,
---   wrapping/unwrapping bundles, and providing errors for invalid operation - while still letting you control exactly how a future (or multiple) can or should be resumed
---   and progressed. Functions like `Future:await`, `Future:schedule` and `Future:resume` are built using these and it vastly simplifies all of them.
async.future = {}


---@param elements async.future.MultiResumeElement[] Raw list of multiple resume scheduler, bundle pairs
---@return async.future.MultiResumeSchedulerList # Tagged list for multiple resume schedulers nya
function async.fn.mark_multi_resume_scheduler_table(elements)
    return setmetatable(elements, async.constants.MULTI_RESUME_SCHED_METATABLE)
end

---If this is not a multi resume scheduler
---@param returned_resume_thing async.future.ResumeSchedulerOrMulti|nil
---@return nil|"single"|"multi"
function async.fn.classify_resume_type(returned_resume_thing)
    if returned_resume_thing == nil then return nil end
    if getmetatable(returned_resume_thing) == async.constants.MULTI_RESUME_SCHED_METATABLE then return "multi" end
    return "single"
end

--- Apply the continuation_mode specified to the resume scheduler(s) specified (whether they are single or multi)
---@param continuation_mode async.future.ContinuationMode Continuation mode to use to start the resume schedulers.
---@param outer_future Future future upon which the continuation mode should be applied with the given bundles and resume scheduler(s) nya
---@param resume_scheduler_fn_or_multi async.future.ResumeSchedulerOrMulti Actual scheduler as retrieved from some outermost Future. May be multi
---@param maybe_next_bundle any? Bundle associated with the resume scheduler function if it is just one function nya. Not used when the resume scheduler
---   fn is actually multi
function async.fn.apply_continuation_mode(continuation_mode, outer_future, resume_scheduler_fn_or_multi,
                                          maybe_next_bundle)
    local classification = async.fn.classify_resume_type(resume_scheduler_fn_or_multi)
    if classification == nil then error("Cannot apply continuation mode to nil resume scheduler(s)") end
    if classification == "multi" then
        if continuation_mode == "jumpstart" then
            ---@type any, async.future.MultiResumeElement
            for _idx, element in ipairs(resume_scheduler_fn_or_multi) do
                element.sched(outer_future, element.bundle)
            end
        else
            ---@type any, async.future.MultiResumeElement
            for _idx, element in ipairs(resume_scheduler_fn_or_multi) do
                continuation_mode(element.sched, outer_future, element.bundle)
            end
        end
    else
        if continuation_mode == "jumpstart" then
            resume_scheduler_fn_or_multi(outer_future, maybe_next_bundle)
        else
            continuation_mode(resume_scheduler_fn_or_multi, outer_future, maybe_next_bundle)
        end
    end
end

---@generic Output
---@param implementation_function fun(): `Output` Raw implementation function. Must require no arguments.
---@param native_continuation_mode async.future.ContinuationMode? Default is "jumpstart", defines what should happen when this future is top-level.
---@async
---@return Future<Output>
function async.future.new(implementation_function, native_continuation_mode)
    ---@type Future
    local fut = {
        _inner_coro = co.create(implementation_function),
        _started = false,
        _done = false,
        _native_continuation_mode = native_continuation_mode or "jumpstart",
        _wrap_bundle = nil,
        _unwrap_bundle = nil
    }

    return setmetatable(fut, { __index = async.future })
end

---@generic Output
---@param self Future<`Output`>
---@param wrap_bundle? fun(inner_bundle: any): any | nil Specify how to produce our bundle from an inner bundle
---@param unwrap_bundle? fun(our_bundle: any): any | nil Specify how to retrieve the inner bundle from our bundle.
--- Attach functions that modify events as they propagate out from this future (and any contained within).
--- wrap is applied to the bundle before it is yielded
--- unwrap is applied to the bundle passed in from the resume mechanism before being injected.
--
--- Any call to this function overrides any existing wrap/unwrap. Calling this function after awaiting or scheduling the future
--- is not allowed and will error. Passing nil as the arguments will while any wrap/unwrap previously set. If you aren't modifying the bundles,
--- then make sure to do this rather than using something like `function(a) return a end`, because for multi-resume-scheduler futures this will
--- prevent the need to iterate over a table.
function async.future:wrap_messages(wrap_bundle, unwrap_bundle)
    if self._started then error("Cannot wrap a future after it's started") end
    self._wrap_bundle = wrap_bundle
    self._unwrap_bundle = unwrap_bundle
end

--- Apply an (optionally nil) wrapper function to a resume scheduler function (that might be a multi) and a single next bundle (which will
--- only be used if the resume scheduler is not multi), and returns the two last arguments again but with wrapping applied.
---@param wrapper? fun(raw_bundle: any): any
---@param resume_scheduler_fn_or_multi async.future.ResumeSchedulerOrMulti
---@param next_bundle any?
---@return async.future.ResumeSchedulerOrMulti # The wrapped resume scheduler functions.
---@return any? next_bundle wrapped next bundle (if resume scheduler is not multi)
local function wrap_bundles_for_resume_schedulers(wrapper, resume_scheduler_fn_or_multi, next_bundle)
    if wrapper ~= nil then
        local classification = async.fn.classify_resume_type(resume_scheduler_fn_or_multi)
        if classification == "multi" then
            -- map every element of the table by applying the wrapper nya
            ---@param element async.future.MultiResumeElement
            return vim.tbl_map(function(element)
                element.bundle = wrapper(element.bundle)
                return element
            end, resume_scheduler_fn_or_multi)
        else
            return resume_scheduler_fn_or_multi, wrapper(next_bundle)
        end
    else
        return resume_scheduler_fn_or_multi, next_bundle
    end
end

---@generic T, Q
--- Apply the function if it is non-nil, else just return the value
---@param func? fun(v: T): `Q`
---@param val `T`
---@return T|Q
local function apply_if_not_nil(func, val) if func then return func(val) else return val end end

---@generic Output
---@param self Future<`Output`>
---@param next_innermost_input_value any Value yielded in the innermost future, that was waiting on async input.
---@param bundle any The arbitrary bundle that was passed into the resume mechanism. Just like normal resume, this is unwrapped automatically.
---@return bool did_return Whether or not this resulted in a final value of the future
---@return Output|async.future.ResumeSchedulerOrMulti # If the future returned, this will be the output value. If it didn't return, this will be the resume mechawism
---@return any? next_bundle If the future didn't return, this is the next bundle that must be passed up the stack. This is already wrapped as defined
---  by the future, so you don't need to worry about doing that. If this is a multi-resume-scheduler future, then  this might be nil or otherwise useless.
---
--- This allows callers to resume a future and then call the next generated resume mechanism in a custom manner as appropriate. This will still automatically
--- run through `nil` resume mechanisms until either an actual resume mechanism is yielded, or the future directly returns. This will also perform standard
--- error checking, and marking the future as done if it is, in fact, done.
function async.future:resume_without_calling_mechanism(next_innermost_input_value, bundle)
    if not self._started then error("Cannot resume non-started future") end
    if self._done then error("Cannot resume a future that has already returned") end

    --- Provide the raw components to the innards.
    ---@type bool, async.future.ResumeSchedulerOrMulti?, any?
    local ok, result_or_resume_scheduler_fn, next_bundle = co.resume(
        self._inner_coro,
        next_innermost_input_value,
        apply_if_not_nil(self._unwrap_bundle, bundle)
    )
    if not ok then error(result_or_resume_scheduler_fn) end

    -- Clean out nil mechanisms until we either return or yield a real mechanism nya
    while co.status(self._inner_coro) ~= "dead" and result_or_resume_scheduler_fn == nil do
        ok, result_or_resume_scheduler_fn, next_bundle = co.resume(self._inner_coro)
        if not ok then error(result_or_resume_scheduler_fn) end
    end

    if co.status(self._inner_coro) ~= "dead" then
        -- The coroutine is suspended or in a weird loopy state, and we have a resume mechanism nya. If it was dead,
        -- it'd be finished ^.^ nya.
        --
        -- Automatically wrap the bundle, too.
        return false, wrap_bundles_for_resume_schedulers(self._wrap_bundle, result_or_resume_scheduler_fn, next_bundle)
    else
        -- The future is done! mark as such and also return the result value (even if it happens to be nil nya)
        self._done = true
        return true, result_or_resume_scheduler_fn
    end
end

---@generic Output
---@param self Future<`Output`>
---@return bool did_return If the future immediately returned, this is true
---@return Output|async.future.ResumeSchedulerOrMulti # If the future immediately returned, this will be the output value. Else, it will be the resume mechanism.
---@return any? next_bundle If the future didn't return, this is the next bundle that must be passed up the stack. This is a pre-wrapped bundle
--- as defined by the future, so that is all handled nya. If this was a multi-resume scheduler, then this should not be used.
---
--- This function allows callers to start a future and handle the resume mechanism and bundle as appropriate. This runs through `nil` mechanisms as appropriate,
--- and does standard error checking nya, plus any marking of the future as done.
function async.future:start_without_calling_mechanism()
    if self._started then error("Can't schedule a future that's already started!") end
    self._started = true

    -- First call, so do an initial resume with no args
    ---@type bool, async.future.ResumeSchedulerOrMulti|nil, any?
    local ok, result_or_resume_scheduler_fn, maybe_first_raw_bundle = co.resume(self._inner_coro)
    if not ok then error(result_or_resume_scheduler_fn) end
    -- Spin away nil resume mechanisms nya
    while co.status(self._inner_coro) ~= "dead" and result_or_resume_scheduler_fn == nil do
        ok, result_or_resume_scheduler_fn, maybe_first_raw_bundle = co.resume(self._inner_coro)
        if not ok then error(result_or_resume_scheduler_fn) end
    end

    if co.status(self._inner_coro) ~= "dead" then
        -- We are paused - wrap the bundle, return false and the resume scheduling mechanism nya
        return false,
            wrap_bundles_for_resume_schedulers(self._wrap_bundle, result_or_resume_scheduler_fn, maybe_first_raw_bundle)
    else
        -- Future is done
        self._done = true
        return true, result_or_resume_scheduler_fn
    end
end

---@generic Output
---@param self Future<`Output`>
---@async Await the result of the future! nya
---@return Output
function async.future:await()
    ---@type bool, async.future.ResumeSchedulerOrMulti, any?
    --- Automatically handles wrapping and multischedule futures nya
    local did_return, result_or_resume_scheduler_fn, next_wrapped_bundle = self:start_without_calling_mechanism()
    while not did_return do
        --- The bundle received here should be the same as that which we yielded. In particular,
        --- when the coroutine awaiting this future is resumed, it must be resumed with the bundle we passed
        --- outwards as the second value. We yield a wrapped bundle, so we must receive one back (which is then
        --- unwrapped in the `Future:resume_without_calling_mechanism` function and passed to our own internal
        --- coroutine with a resume nya, and any futures inside THAT will then, in their own :await(), unwrap any wrapping
        --- that they did nya).
        ---
        --- Basically, the future/coroutine calling this's "raw" bundle, is our "wrapped" bundle, because Future:await() is
        --- not within our coroutine but inside another coroutine implementation *waiting* on our coroutine nya.
        local next_input_for_innermost_future, our_bundle = co.yield(
            result_or_resume_scheduler_fn,
            next_wrapped_bundle
        )
        -- Get the next resume mechanism, or the result nya. Passes our bundle back into the inner
        -- coroutine implementation. This automatically handles multis/wrapping/etc.
        did_return, result_or_resume_scheduler_fn, next_wrapped_bundle = self:resume_without_calling_mechanism(
            next_input_for_innermost_future, our_bundle
        )
    end
    --- By this point it is done :) so this is the result.
    return result_or_resume_scheduler_fn
end

---@generic Output
---@param self Future<`Output`>
---@param mode async.future.FutureContinuationModeSpecifier? Run the first resume scheduler callback with the given mode (default: native)
---@return bool did_return If the future immediately returned, this is true. If false, no second value is returned nya
---@return Output? result If the future immediately returned this is it's value. If it didn't, then it will always be nil
---  (of course if the future returns nil you need to rely on the boolean anyhow ^.^)
function async.future:schedule(mode)
    mode = mode or "native"
    if mode == "native" then mode = self._native_continuation_mode end
    ---@type bool, async.future.ResumeSchedulerOrMulti, any?
    local did_return, result_or_resume_scheduler_fn, maybe_next_bundle = self:start_without_calling_mechanism()

    if did_return then
        -- Finished nya
        return true, result_or_resume_scheduler_fn
    else
        async.fn.apply_continuation_mode(mode, self, result_or_resume_scheduler_fn, maybe_next_bundle)
        return false, nil
    end
end

---@generic Output
---@alias async.future.FutureContinuationModeSpecifier
--- | "native" Use the native specification in this (the outermost) future - the default
--- | async.future.ContinuationMode Specify a continuation mode
---@param self Future<`Output`>
---@param inner_future_input any The value that will be yielded in the innermost future.
---@param bundle any The arbitrary bundle that was passed into the resume mechanism
---@param continuation_mode async.future.FutureContinuationModeSpecifier? Default is to use the native value for the outermost function.
---@return bool did_return Whether or not this resulted in a final value of the future
---@return Output? return_val Return value of the future if it did return, nil if not
--- Resume the innermost coroutine/future with data. Should primarily be called in *resume scheduler* callbacks.
---
--- Errors if the internal coroutine is not in a suspend state. If the inner coroutine suspends, then that is interpreted
--- as a resume mechanism, and is called with self. If the coroutine *returns*, then, well, the outermost future is done nya.
---
--- The inner coroutine of this has to have already been resumed from "initial" state, and hence passing in values is valid nya.
--- The yield statements inside the function definition of this future cannot be reached until after the first initial resume with no arguments,
--- and those yield statements are what are used to generate the values anyhow.
---
--- If the future resuming causes an error, then it is re-thrown.
function async.future:resume(inner_future_input, bundle, continuation_mode)
    continuation_mode = continuation_mode or "native"
    if continuation_mode == "native" then continuation_mode = self._native_continuation_mode end

    ---@type bool, async.future.ResumeSchedulerOrMulti, any?
    local is_finished, result_or_resume_scheduler_fn, maybe_next_bundle = self:resume_without_calling_mechanism(
        inner_future_input,
        bundle)

    if not is_finished then
        async.fn.apply_continuation_mode(continuation_mode, self, result_or_resume_scheduler_fn, maybe_next_bundle)
        return false, nil
    else
        return true, result_or_resume_scheduler_fn
    end
end

---Return if this future has been started at all (e.g. if it's awaited, or scheduled).
---@generic Output
---@param self Future<`Output`>
---@return bool # True if it's been started, false otherwise nya
function async.future:has_started()
    return self._started
end

---Return if this future is already done and has returned a value (if so, it won't accept further resumes nya)
---@generic Output
---@param self Future<`Output`>
---@return bool # True if this future has already returned, false it its still waiting for more resume data ^.^
function async.future:has_returned()
    return self._done
end

--- Apply a synchronous transform to the output of this future.
--- This will preserve the native continutation mode of the original future. However, this process solidifies the
--- event bundle transforms as an inner future so any new ones will be applied on top of the old ones..
--- Cannot be applied to a future already in progress or complete.
---@generic OriginalOut, NewOut
---@param self Future<`OriginalOut`>
---@param transform fun(orig: OriginalOut): `NewOut`
---@return Future<NewOut>
function async.future:and_then(transform)
    if self:has_started() then error("Can't map an in-progress future") end
    return async.future.new(function()
        local result = self:await()
        return transform(result)
    end, self._native_continuation_mode)
end

--- Construct an async function (one that produces a future) from a body containing any number of future awaits
--- and coroutine.yield calls following the futures protocol nya
---
---@generic Output
---@param future_body fun(...): `Output` Function that implements the future via future awaits and such, or coroutine.yield
--- futures protocol nya.
---@param continuation_mode async.future.ContinuationMode? Default is jumpstart, but specifies the native means of applying a resume mechanism nya
---@return fun(...): Future<Output> # Async function that produces a future ^.^
function async.fn.from_async_body(future_body, continuation_mode)
    local future_wrapper = function(...)
        -- Pack up args for capture
        local saved_args = pack(...)
        return async.future.new(function()
            local res = future_body(unpack(saved_args))
            return res
        end, continuation_mode)
    end
    return future_wrapper
end


--- In many cases, async functions just transform some arguments, pass them to another function
--- to construct a future, then process the result of that future in some way. That is, they 
--- use just one future in a very simple manner. 
---
--- This wraps the input and output  
---
function async.fn.io_wrap_async_fn(iwrapper, inner_future_builder, owrapper)
    return async.fn.from_async_body(function(...) 
        return owrapper(inner_future_builder(iwrapper(...)):await()) 
    end) 
end

--- Like async.fn.io_wrap_async_fn, but only modifies the input processing, returning the
--- raw output of the future. 
function async.fn.i_wrap_async_fn(iwrapper, inner_future_builder)
    return async.fn.from_async_body(function (...)
        return inner_future_builder(iwrapper(...)):await() 
    end) 
end

--- Like async.fn.o_wrap_async_fn, but only modifies the output processing, constructing 
--- the future from the input unmodified.
function async.fn.o_wrap_async_fn(inner_future_builder, owrapper)
    return async.fn.from_async_body(function (...)
        return owrapper(inner_future_builder(...):await())
    end)
end

-- Make async callable to produce functions from coro bodies
setmetatable(async, { __call = async.fn.from_async_body })


--- Wait for all futures in the table simultaneously, producing a future of the map
--- of keys to the corresponding result of the future (note that if your future returns nil,
--- then of course the value will be empty in the result nya)
---
--- This function relies on setting the bundle transform for the futures passed in, and hence will override them. This function
--- *will* clobber the input table. In particular, futures are sometimes deleted once their result is obtained, which should save memory
--- used by their presumably large stored inner coroutines nya
--- @type fun(futures: table<any, Future>): Future<table<any, any>>
async.fn.all = async(function(futures)
    ---@type table<any, any?>
    --- Table of results for the future with the given key nya
    --- We don't need to keep a list of futures that
    local return_value_map = {}


    -- How many futures are left to finish. When this is zero, we return
    local remaining_futures = 0

    ---@type async.future.ResumeSchedulerOrMulti
    --- List of resume functions to be spat out the first time.
    --- This requires flattening any inner multi-resume lists for the inner futures to build
    --- one, gigantic one.
    local resume_scheduler_spec_list = {}
    for key, fut in pairs(futures) do
        -- Wrapping tags the bundle with the key, unwrapping extracts the inner bundle.
        fut:wrap_messages(function(inner_bundle)
            return {
                -- Used to track which inner future this resume comes from
                multi_future_routing_key = key,
                inner_bundle = inner_bundle
            }
        end, function(outer_bundle)
            if inner_bundle.multi_future_routing_key ~= key then
                error("Incorrect routing for future in multi-future with key: " .. tostring(key))
            end
            return outer_bundle.inner_bundle
        end)

        -- Start up each future nya
        ---@type bool, async.future.ResumeSchedulerOrMulti|any, any?
        local did_return, result_or_resume_scheduler_fn, maybe_bundle = fut:start_without_calling_mechanism()
        if not did_return then
            remaining_futures = remaining_futures + 1

            -- extend the large resume bundle thing nya
            local inner_resume_type = async.fn.classify_resume_type(result_or_resume_scheduler_fn)
            if inner_resume_type == "multi" then
                -- No, I can't find a function that does a single level flatten, even in vim (it has a recursive flatten, though nya)
                for _idx, element in ipairs(result_or_resume_scheduler_fn) do
                    table.insert(resume_scheduler_spec_list, element)
                end
            else
                table.insert(resume_scheduler_spec_list, { sched = result_or_resume_scheduler_fn, bundle = maybe_bundle })
            end
        else
            return_value_map[key] = result_or_resume_scheduler_fn
        end
    end

    -- When yielding from the future, we only have space for *one* resume function, and *one* bundle. The problem is, that
    -- we need to schedule the resume of *n* different futures, which actually each have distinct bundles. That is, yielding one
    -- resume scheduler once to start all the futures only allows for one single bundle to be passed back into each of the different
    -- resume calls within the resume scheduler, with no way to distinguish them nya.
    --
    -- If we were to go future by future at first, only continuing to emit more future resume schedulers to call when one of the existing
    -- futures finally provided an input, it wouldn't be very async anymore now would it? nya.
    --
    -- The solution: Modify the futures protocol to emit a collection of (resume scheduler, raw bundle) pairs, and call all of them, rather than just one
    -- bundle and one resume scheduler.

    --- used internally each time, but for the first time we want to use the one that schedules everything :)
    ---@type async.future.ResumeSchedulerOrMulti|any
    local result_or_resume_scheduler_fn_or_multi = async.fn.mark_multi_resume_scheduler_table(resume_scheduler_spec_list)
    --- Next bundle to emit. Obtained from futures, but for now it's nil (we're doing multi anyway so it's cool ;3
    local next_bundle = nil
    --- Preallocate this one to avoid using local within the loop declaration and then causing issues where
    --- the outer variable for the remaining 2nd/3rd results are not updated because they are shadowed nya
    local did_return = nil
    while remaining_futures > 0 do
        --- Because a reference to the outermost future is held by likely more than one place, whenever we ae resumed, it may be from *any* future
        --- rather than only the one we just yielded with. But that's fine, as long as we route things towards the right future.
        local input, unrouted_bundle = co.yield(result_or_resume_scheduler_fn_or_multi, next_bundle)
        local curr_key = unrouted_bundle.multi_future_routing_key
        -- Route to correct future via checking the bundle for multi_future_routing_key
        ---@type Future
        local fut = futures[curr_key]

        -- We now routed this bundle lol, step the relevant future and obtain the resume scheduling mechanism(s) (or final result) to
        -- emit.
        did_return, result_or_resume_scheduler_fn_or_multi, next_bundle = fut:resume_without_calling_mechanism(input,
            unrouted_bundle)

        -- If we didn't return, nothing special needs to be done. We just advanced the future and placed it's resume scheduler(s) to be
        -- forwarded upward :)
        if did_return then
            -- The value is in fact a return, delete the future
            futures[curr_key] = nil
            -- Make sure we decrease the # of waiting futures nya
            remaining_futures = remaining_futures - 1
            return_value_map[curr_key] = result_or_resume_scheduler_fn_or_multi
            -- Return an empty resume scheduler that is NOT nil. The internal future is finished, which means
            -- that it should not be resumed in future. However, if the overall `all` future is not finished, then
            -- we don't want to return. So instead, we provide an empty function, since that *route* of resumption
            -- is no longer necessary. If we have finished, of course, then we do nothing and fall out of the loop, waiting
            -- for any more unfinished futures' resume schedulers to enter again at the yield point above.
            if remaining_futures >= 1 then
                --- deliberately do nothing here so we can go up.
                result_or_resume_scheduler_fn_or_multi = function(_outer_fut, _outer_bundle)
                end
            end
        end
    end

    return return_value_map
end)


async.stream = {}

---@alias async.stream.Queue.OnNotifierCallback fun(was_finalised: bool, message_if_any: any?) Function to call when waiting for a new value or for the stream
---   queue to be marked as finished nya. If was_finalised is true, the message is not a meaningful value, and the stream is finished.
---
--- Inner queue of items designed for use by streams.
---@class async.stream.Queue<Message>
---@field private received_last bool If true, no more items can be received.
---@field private next_item_index integer The index of the first message in the queue
---@field private one_after_last_index integer One-after the last item in the queue. If same as "next_item_index" there is nothing left.
---@field private next_notifier async.stream.Queue.OnNotifierCallback? Should only be non-nil if the queue is empty. Provides a means of notifying that:
---    * the queue has finalised and nothing more will come through
---    * a new item has come through.
---@field private queue_storage table<integer, any> Array of queue items sequential. Only the keys [next_item_index, end) should contain messages.
async.stream.queue = {}

---@return async.stream.Queue
function async.stream.queue.new()
    return setmetatable({
        received_last = false,
        next_item_index = 1,
        one_after_last_index = 1,
        next_notifier = nil,
        queue_storage = {}
    }, { __index = async.stream.queue })
end

--- Get the total # of items in the queue remaining.
function async.stream.queue:len()
    return self.one_after_last_index - self.next_item_index
end

--- If this can receive more items.
function async.stream.queue:can_receive_more()
    return not self.received_last
end

--- Get the next value or an indication that no more values are coming, if that information is available.
---@return bool had_result If true then the queue either had another message or it is empty and finalised
---@return bool? was_closed Only meaningful if an immediate result was available.
---   If false, indicates that a valid message was returned. If true, no message was returned and no
---   more messages can be obtained from this queue nya
---@return any? next_message Only meaningful if an immediate result was available and the queue was not closed/finalised.
function async.stream.queue:maybe_next()
    --- in this case, there are still more items to extract.
    if self:len() > 0 then
        local next_message = self.queue_storage[self.next_item_index]
        self.queue_storage[self.next_item_index] = nil
        self.next_item_index = self.next_item_index + 1
        return true, false, next_message
    elseif self.received_last then
        -- We have been closed and there were no more buffered items.
        return true, true
    else
        -- Nothing immediately available
        return false
    end
end

--- Get the next value or an indication that no more values are coming. If it was not immediately available, then
--- set up a notifier function to be called once, when the information becomes available. The notifier function will be
--- deleted after being called. Any existing notifier function will be overwritten.
---
---@param notifier async.stream.Queue.OnNotifierCallback The notifier function to set.
---@return bool had_result If true then the queue either had another message or it is empty and finalised.
---   If not true, no immediate value has been set up, and the notifier function has been configured.
---@return bool? was_closed Only meaningful if an immediate result was available.
---   If false, indicates that a valid message was returned. If true, no message was returned and no
---   more messages can be obtained from this queue nya
---@return any? next_message Only meaningful if an immediate result was available and the queue was not closed/finalised.
function async.stream.queue:next_or_notify(notifier)
    local had_result, was_closed, next_message = self:maybe_next()
    if not had_result then
        self.next_notifier = notifier
    end
    return had_result, was_closed, next_message
end

--- Get the next value or an indication that no more values are coming, and run the notifier function with it immediately.
--- If those data points were not available immediately, then set the notifier to run when they are.
--- @param notifier async.stream.Queue.OnNotifierCallback Notifier function to immediately run once if data is available, or to set to be
---    called later once, when new items or finalisation is pushed to the queue nya
function async.stream.queue:immediately_apply_notifier_or_set(notifier)
    local had_result, was_closed, next_message = self:maybe_next()
    if not had_result then
        self.next_notifier = notifier
    else
        notifier(was_closed, next_message)
    end
end

--- Get the next value or an indication that no more values are coming. If it was not immediately available, then
--- this provides a function that can be called to set the notifier in a safe manner i.e. it will recheck things.
---
---@return bool had_result If true then the queue either had another message or it is empty and finalised.
---   If not true, then the returned function
---@return bool? was_closed Only meaningful if an immediate result was available.
---   If false, indicates that a valid message was returned. If true, no message was returned and no
---   more messages can be obtained from this queue nya
---@return any? next_message Only meaningful if an immediate result was available and the queue was not closed/finalised.
---@return fun(notifier: async.stream.Queue.OnNotifierCallback)? notifier_setter Means of setting the notifier. Should be
---   called exactly once. Will call the notifier function if, between this call and the next, a new value has been put in the stream.
---   note that if another function extracts from the stream between the time of this call and the time this returned function is called,
---   you may miss some values.
---   This is *primarily* intended for people who want to work with futures.
function async.stream.queue:next_or_return_function_to_set_notifier()
    local had_result, was_closed, next_message = self:maybe_next()
    if not had_result then
        return false, nil, nil, function(n) self:immediately_apply_notifier_or_set(n) end
    else
        return true, was_closed, next_message
    end
end

--- Push a new message onto the queue. If there is a waiting notifier then the queue will never be modified and the notifier will be
--- called immediately.
--- @generic Message
--- @param message `Message`
--- @param allow_failure bool? Default false. If false, then this will error if queue was closed/finished and you try to push
---    again. If true, then return a failure status instead.
--- @return bool succeeded Returns whether or not we successfully pushed onto the queue. Only useful in allow_failure mode, because
---   by default an error is thrown on failure.
function async.stream.queue:push_message(message, allow_failure)
    -- Error state
    if self.received_last then
        if not allow_failure then
            error("Can't push to already-finished streamqueue")
        end
        return false
    elseif self.next_notifier ~= nil then
        self.next_notifier(false, message)
        self.next_notifier = nil
        return true
    else
        -- There was a new value. The queue must be extended.
        self.queue_storage[self.one_after_last_index] = message
        self.one_after_last_index = self.one_after_last_index + 1
        return true
    end
end

--- Tell the queue that no more items can be pushed onto it.
--- Must be called on every streamqueue at it's end.
function async.stream.queue:close()
    self.received_last = true
    -- If this is not nil, then len() should be zero anyhow nya
    if self.next_notifier ~= nil then
        self.next_notifier(true)
        self.next_notifier = nil
    end
end

async.stream.spsc = {}


--- The side of a stream capable of sending messages/input chunks. This is meant for single-producer and single-consumer streams.
--- Users MUST call :close() when they are done with it.
--- @class async.stream.spsc.Sender<Message>
--- @field private shared_queue async.stream.Queue Internal queue shared between the sender and receiver end.
async.stream.spsc.sender = {}

--- Send a message down this stream.
--- @param message Message Message to push along.
--- @param allow_failure bool? Whether or not to allow failure. If failure is allowed, then the error status is returned. If not, an error is thrown. (default false).
--- @return bool success Only true if the message was sent on the stream nya
function async.stream.spsc.sender:send(message, allow_failure)
    return self.shared_queue:push_message(message, allow_failure)
end

--- Indicate that no more messages will come from this stream.
--- Must be called on all senders eventually.
function async.stream.spsc.sender:close()
    return self.shared_queue:close()
end

--- The side of a stream capable of sending message/input chunks. Sometimes,
--- stream/stream-like interfaces are used for things other than a mere queue,
--- and it is desirable to do sending asynchronously.
--- You must call :close() on this.
--- @class async.stream.spsc.AsyncSender<Message>
--- @field private inner_stream async.stream.spsc.Sender
async.stream.spsc.async_sender = {}

--- Send a message down this stream, asynchronously.
--- @param message Message message to send
--- @param allow_failure bool? Whether or not to allow failure, default false (aka throw with error())
---    If allowed, then return nil for success, or a string for an error, in the future.
--- @async
--- @return Future<string?> maybe_err
function async.stream.spsc.async_sender:send(message, allow_failure)
    return async.future.new(function()
        local ok = self.inner_stream:send(message, allow_failure)
        if ok then
            return nil
        else
            return "stream closed"
        end
    end)
end

--- Close this stream, asynchronously. This will wait for any pending write requests.
--- If there was an error while shutting down, it will return a non-nil value that is
--- a string (note that for default in-memory streams, there will never be an error nya
--- @async
--- @return Future<string?> maybe_err Non-nil if there was an error.
function async.stream.spsc.async_sender:close()
    return async.future.new(function()
        self.inner_stream:close()
        return nil
    end)
end

--- The side of a stream capable of receiving messages/input chunks. This is meant for single-producer and single-consumer streams.
--- @class async.stream.spsc.Receiver<Message>
--- @field private shared_queue async.stream.Queue Internal queue shared between the sender and receiver end nya
async.stream.spsc.receiver = {}

--- Create a sender, receiver pair to use with some form of IO.
--- @generic Message
--- @return async.stream.spsc.Sender<`Message`> tx The sender of this stream. You must call :close() on this when you are done with it.
--- @return async.stream.spsc.Receiver<`Message`> rx The receiver of this stream.
function async.stream.spsc.new()
    local shared_queue = async.stream.queue.new()
    local sender = setmetatable({ shared_queue = shared_queue }, { __index = async.stream.spsc.sender })
    local receiver = setmetatable({ shared_queue = shared_queue }, { __index = async.stream.spsc.receiver })
    return sender, receiver
end

--- Create an async-sender, receiver pair to use.
--- @generic Message
--- @return async.stream.spsc.AsyncSender<`Message`> tx The sender of this stream. You must call :close() on this and await, when you are
---  done with it nya
--- @return async.stream.spsc.Receiver<`Message`> rx The receiving end of this stream.
function async.stream.spsc.async_new()
    local sync_tx, rx = async.stream.spsc.new()
    local tx = setmetatable({ inner_stream = sync_tx }, { __index = async.stream.spsc.async_sender })
    return tx, rx
end

---@async
---@generic Message
---@return Future<{closed: bool, maybe_message: Message?}> # If the stream was not closed, then maybe_message is the next message. If it was closed, then closed is true and
---  there is no message nya
function async.stream.spsc.receiver:next()
    return async.future.new(function()
        local had_immediate_result, is_done, maybe_message, notifier_setting_fn =
            self.shared_queue:next_or_return_function_to_set_notifier()
        if had_immediate_result then
            return { closed = is_done, maybe_message = maybe_message }
        else
            if notifier_setting_fn == nil then
                error(
                    "Notifier setting fn should never be nil when calling next_or_return_function_to_set_notifier and not getting results immediately")
            end

            ---@param outermost_future Future
            ---@param outer_bundle any
            local result_input = co.yield(function(outermost_future, outer_bundle)
                notifier_setting_fn(function(was_finalised, message_if_not_finalised)
                    outermost_future:resume({ closed = was_finalised, maybe_message = message_if_not_finalised },
                        outer_bundle)
                end)
            end)
            return result_input
        end
    end)
end

--- Discard the receiver, saying you want no more input from it.
--- Unlike senders, you are not required to call this. However, if you do, it will close the stream and prevent receiving more stuff from it nya,
--- and it will prevent input SENDING more stuff TO it. If your input does not check for closedness, then this could cause errors.
function async.stream.spsc.receiver:discard()
    self.shared_queue:close()
end

return async
