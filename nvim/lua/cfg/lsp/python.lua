--- For the horrors of python
--- Lots of credit goes to lsp_config here: https://github.com/neovim/nvim-lspconfig/blob/master/lua/lspconfig/server_configurations/pylyzer.lua
---
local python = {}
local reload = require("reload")

local exepath = require("cfg.utils").exepathnil
local discovery = require("cfg.utils").discovery
local log = require("cfg.log").with_prefix("LSP").with_prefix("python")

-- Note that currently, pylyzer just crashes. Hence, we will disable for now >.<
local function pylyzer(lsp, epoch)
    --- @module 'cfg.lsp'
    lsp = lsp
    local pylyzer_exe = exepath("pylyzer")
    if pylyzer_exe == nil then
        log.warn("Did not find pylyzer executable")
        return
    end
    log.info("found pylyzer.")
    lsp.register("python", {
        name = "pylyzer",
        cmd = { "bash", "-c", "RUST_BACKTRACE=1 " .. pylyzer_exe .. " --server " .. " --verbose 2 2>/tmp/pylyzerlog.txt" },
        root_dir = function(bufnr, ftype, fname)
            fname = discovery.maybe_resolve(bufnr, fname)
            if fname == nil then return nil end
            local root_structure_any_of = {
                { 'setup.py' },
                { 'tox.ini' },
                { 'requirements.txt' },
                { 'Pipfile' },
                { 'pyproject.toml' },
                discovery.patterns.GIT
            }
            return discovery.root_pattern(fname, root_structure_any_of)
        end,
        -- Again taken from lsp_config https://github.com/neovim/nvim-lspconfig/blob/master/lua/lspconfig/server_configurations/pylyzer.lua
        init_options = {
            diagnostics = true,
            inlayHints = true,
            smartCompletion = true,
        }
    }, nil, epoch)
end

local function jedi(lsp, epoch)
    --- @module 'cfg.lsp'
    lsp = lsp
    local jedi_exe = exepath("jedi-language-server")
    if jedi_exe == nil then
        log.warn("Did not find jedi-language-server executable")
        return
    end
    log.info("Found jedi-language-server")
    lsp.register("python", {
        name = "jedi-language-server",
        cmd = { jedi_exe },
        root_dir = function(bufnr, ftype, fname)
            fname = discovery.maybe_resolve(bufnr, fname)
            if fname == nil then return nil end
            local root_structure_any_of = {
                { 'pyproject.toml' },
                { 'setup.py' },
                { 'setup.cfg' },
                { 'requirements.txt' },
                { 'Pipfile' },
                discovery.patterns.GIT
            }
            return discovery.root_pattern(fname, root_structure_any_of)
        end
    }, nil, epoch)
end


function python.init()
    local lsp = require("cfg.lsp")
    local epoch = reload.epoch:get(python)
    -- pylyzer(lsp, epoch)
    jedi(lsp, epoch)
end

return python
