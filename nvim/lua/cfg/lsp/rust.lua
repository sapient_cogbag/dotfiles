local rust = {}
local reload = require("reload")
local exepath = require("cfg.utils").exepathnil
local bool = require("cfg.utils").bool
local discovery = require("cfg.utils").discovery
local log = require("cfg.log").with_prefix("LSP").with_prefix("rust")
local async = require("cfg.async")

-- local lsp = mod("lsp")
-- Note that we can't do mod("lsp") out here because it creates a loop
-- when LSP loads ourselves nya

-- Returns the exepath of rustup if it is present, else nil
local function rustup_installed()
    return exepath("rustup")
end

-- Obtain a list of installed rust components (not prefixed) asynchronously.
--
-- Takes a callback that is passed either a table-array of installed components with their
-- raw names including e.g. -nightly and -arch-flavour-os or similar, or nil in case of errors
-- running rustup
local function rustup_all_components(rustup_exe_path, toolchain, callback)
    local lines = { '' }
    -- This uses example code from within the help.
    local jid = vim.fn.jobstart({ rustup_exe_path, "component", "list", "--toolchain", toolchain, "--installed" }, {
        on_stdout = function(_jobdata, stdout_partial_lines)
            -- Extend last line with start of the partial lines nya
            lines[#lines] = lines[#lines] .. stdout_partial_lines[1]
            vim.list_extend(lines, vim.list_slice(stdout_partial_lines, 2))
        end,
        -- _evt_type is just exit nya
        on_exit = function(_job_id, _exit_code, _evt_type)
            callback(lines)
        end,
        stdin = "null"
    })

    if jid == 0 then
        error("invalid job params")
        return
    end
    if jid == -1 then callback() end
end

-- Takes a list of components as output from `rustup_all_components`, and returns true if
-- installed, false otherwise.
local function rustup_component_installed(component_list, component)
    log.trace("All installed components: " .. vim.inspect(component_list))
    -- vim.tbl_filter doesn't document it's own callback signature, but it just takes a value :)
    -- nya
    local matching_components = vim.tbl_filter(function(value)
        log.debug("Looking at" .. value)
        return vim.startswith(value, component)
    end, component_list)
    log.debug("Found " .. vim.inspect(matching_components) .. " when looking for rustup component " .. component)
    return not vim.tbl_isempty(matching_components)
end

-- Returns the exepath of the system rust analyzer or nil
local function system_rustanalyzer_installed()
    return exepath("rust-analyzer")
end

--- Returns the exepath of the system clippy or nil
local function system_clippy_installed()
    return exepath("cargo-clippy")
end

-- Identify:
-- * ["command"] The command to run rust-analyzer (as a list)
-- * ["clippy"] Whether to use clippy or not
--
-- This will first go for rustup and then if that fails will check system.
--
-- Even if a system clippy exists, if no clippy is present in a rustup component it will not
-- be used.
--
-- Takes a callback that will asynchronously be called when all the jobs required to identify the
-- rust install are done - the parameter is either nil or a table of the form
--  {
--      command = <string or table indicating a command path and arguments>,
--      clippy = bool
--  }
-- (nil means that no rust install could be identified :( nya)
local function identify_rust_install(callback)
    local rustup = rustup_installed()

    local function system_ra(callback)
        local system_rustanalyzer = system_rustanalyzer_installed()
        if not system_rustanalyzer then return nil end
        local clippy = system_clippy_installed()
        -- Always do list since it avoids shell nya
        callback({ command = { system_rustanalyzer }, clippy = bool(clippy) })
    end

    if rustup then
        rustup_all_components(rustup, "nightly", function(all_components)
            if rustup_component_installed(all_components, "rust-analyzer") then
                -- Now attempt to determine clippy
                local clippy = rustup_component_installed(all_components, "clippy")
                callback({ command = { rustup, "run", "nightly", "rust-analyzer" }, clippy = bool(clippy) })
            else
                system_ra(callback)
            end
        end)
    else
        system_ra(callback)
    end
end

function rust.init()
    local lsp = require("cfg.lsp")
    local epoch = reload.epoch:get(rust)
    log.info("Identifying rust install...")
    identify_rust_install(function(rust_install)
        if rust_install == nil then
            log.warn({ schedule = true, "No rust install detected!" })
            return
        end

        local check_cmd
        if rust_install.clippy then
            check_cmd = "clippy"
        else
            check_cmd = "check"
        end

        lsp.register("rust", {
            name = "rust-analyzer",
            cmd = rust_install.command,
            init_options = {
                cargo = {
                    -- Can't find in manual
                    ["loadOutDirsFromCheck"] = true,
                    features = "all"
                },
                -- This also isn't in the manual but i'd be really surprised if
                -- this stopped being a thing nya
                check = {
                    command = check_cmd
                },
                checkOnSave = {
                    command = check_cmd
                },
                imports = {
                    prefer = {
                        no = {
                            std = true
                        }
                    },
                    granularity = { enforce = true }
                },
                inlayHints = {
                    lifetimeElisionHints = {
                        enable = true
                    },
                    closureReturnTypeHints = {
                        enable = "with-block"
                    }
                },
                hover = {
                    memoryLayout = {
                        niches = true
                    }
                },
                diagnostics = {
                    enable = true,
                    experimental = {
                        -- Try this in a few months again
                        enable = false
                    }
                }
            },
            root_dir = function(bufnr, ftype, fname)
                -- Nil means unnamed buf, we just use it to check tho because
                -- the parent thing needs full path
                fname = discovery.maybe_resolve(bufnr, fname)
                if fname == nil then return nil end
                local root_structure_any_of = {
                    { "Cargo.toml" },
                    { "rust-project.json" },
                    discovery.patterns.GIT
                }

                return discovery.root_pattern(fname, root_structure_any_of)
            end

        }, {
            -- Rust-analyzer I *think* can do multiproject stuff so we can use one instance
            -- for everyone :) nya
            reuse_client = function(client, our_cfg)
                return client.name == our_cfg.name
            end
        }, epoch)
    end)
end

return rust
