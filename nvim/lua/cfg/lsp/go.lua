-- This module is for Golang LSP nya
local go = {}
local reload = require("reload")

local utils = require("cfg.utils")
local exepath = utils.exepathnil
local discovery = utils.discovery

local log = require("cfg.log").with_prefix("LSP").with_prefix("go")

local function gopls_lsp(lsp, epoch)
    --- @module "cfg.lsp"
    lsp = lsp

    local gopls_lsp_exe = exepath("gopls")
    if gopls_lsp_exe == nil then
        log.warn("Did not find gopls executable")
        return
    end
    log.info("Found gopls")
    -- See: https://github.com/neovim/nvim-lspconfig/blob/master/lua/lspconfig/server_configurations/gopls.lua
    lsp.register({ "go", "gomod", "gowork", "gotmpl" }, {
        name = "gopls",
        cmd = { gopls_lsp_exe },
        root_dir = function(bufnr, ftype, fname)
            fname = discovery.maybe_resolve(bufnr, fname)
            if fname == nil then return nil end
            local root_structure_any_of = {
                { "go.work" },
                discovery.patterns.merge({ "go.mod" }, discovery.patterns.GIT)
            }

            return discovery.root_pattern(fname, root_structure_any_of)
        end
    }, {
        -- We need to do some special things to work with go module store.
        --
        -- See: https://github.com/neovim/nvim-lspconfig/issues/804
        --   We have to hack up something to enable go module weirdness to work nya
        reuse_client = function(client, full_config, bufnr, filetype, filename)
            -- Check the name and root directory
            if client.name == full_config.name and client.config.root_dir == full_config.root_dir then
                return true
            end

            -- The gomodcache stuff is only needed for gopls.
            if client.name ~= "gopls" then return false end

            local client_raw_dirs = {}
            for _idx, workspace_folder_with_uri in ipairs(client.workspace_folders or {}) do
                client_raw_dirs[#client_raw_dirs + 1] = vim.uri_to_fname(workspace_folder_with_uri.uri)
            end
            if client_raw_dirs.go_cache_data == nil then
                client_raw_dirs.go_cache_data = {}
            end
            -- We want to go through the directories that the given client is workspacing in.
            --
            -- If they haven't had their `GOMODCACHE` retrieved in cache, then it's retrieved.
            for _, workspace_directory_to_check_go_module_cache_dir in ipairs(client_raw_dirs) do
                if client_raw_dirs.go_cache_data[workspace_directory_to_check_go_module_cache_dir] == nil then
                    -- Get the GOMODCACHE value for the given directory (associated with the project nya)
                    local did_start, process_object = pcall(vim.system, { "go", "env", "GOMODCACHE" }, {
                        cwd = workspace_directory_to_check_go_module_cache_dir,
                        text = true
                    })

                    if not did_start then
                        log.warn(
                            "Could not get go environment variable 'GOMODCACHE' with `go env GOMODCACHE` in LSP server workspace directory %s",
                            workspace_directory_to_check_go_module_cache_dir)
                    else
                        local sysoutput = process_object:wait()
                        if sysoutput.stdout ~= nil then
                            local stripped_path = utils.string.strip(sysoutput.stdout)
                            client_raw_dirs.go_cache_data[workspace_directory_to_check_go_module_cache_dir] =
                                stripped_path
                        end
                    end
                end

                -- If we actually found GOMODCACHE, then we check if it is contained in the workspace directory
                if client_raw_dirs.go_cache_data[workspace_directory_to_check_go_module_cache_dir] ~= nil then
                    local gomodcache = client_raw_dirs.go_cache_data[workspace_directory_to_check_go_module_cache_dir]
                    -- Check if the gomodcache is a prefix of the filename like in the lsp_config impl nya
                    if string.sub(filename, 1, #gomodcache) == gomodcache then return true end
                end

                return false
            end
        end,
    }, epoch)
end

function go.init()
    local lsp = require("cfg.lsp")
    local epoch = reload.epoch:get(go)

    gopls_lsp(lsp, epoch)
end

return go
