--- Fancy markdown navigation using marksman
local markdown = {}
local reload = require("reload")
local log = require("cfg.log").with_prefix("LSP").with_prefix("markdown")
local discovery = require("cfg.utils.discovery")
local exepath = require("cfg.utils").exepathnil

local function marksman(epoch)
    local lsp = require("cfg.lsp")
    vim.schedule(function()
        local marksman_exe = exepath("marksman")
        if marksman_exe == nil then
            log.warn("Did not find marksman executable")
            return
        end

        log.info("Found marksman langserver executable")
        lsp.register({ "markdown", "pandoc" }, {
            name = "marksman",
            cmd = { marksman_exe, "server" },
            root_dir = function(bufnr, ftype, fname)
                fname = discovery.maybe_resolve(bufnr, fname)
                if fname == nil then return nil end
                return discovery.root_pattern(fname, {
                    { ".marksman.toml" },
                    discovery.patterns.GIT
                })
            end
        }, nil, epoch)
    end)
end

function markdown.init()
    local epoch = reload.epoch:get(markdown)
    marksman(epoch)
end

return markdown
