-- Module for vimscript
--
-- Even with our config in lua, being able to navigate the source of other vim plugins
-- is highly useful nya

-- `vim` is the standard api table for neovim, so we use vim_lsp as the name instead nya
local vim_lsp = {}

local reload = require("reload")
local mod = reload.mod
local exepath = require("cfg.utils").exepathnil
local discovery = require("cfg.utils").discovery
local log = require("cfg.log").with_prefix("LSP").with_prefix("vim")

-- Register the vim language server nya
local function vim_language_server(lsp, epoch)
    local vim_language_server_exe = exepath("vim-language-server")
    if vim_language_server_exe == nil then
        log.warn("Did not find vim-language-server executable")
        return
    end
    log.info("Found vim-language-server")

    lsp.register("vim", {
        name = "vim-language-server",
        cmd = { vim_language_server_exe, "--stdio" },
        -- No rootdir - runtime path is used nya
        init_options = {
            vimruntime = vim.env.VIMRUNTIME,
            runtimepath = vim.o.rtp
        }
    }, nil, epoch)
end

function vim_lsp.init()
    local lsp = require("cfg.lsp")
    local epoch = reload.epoch:get(vim_lsp)
    -- Highly SUSPICIOUS log message from this LSP...
    --
    -- '/etc/credstore.encrypted tried to be read but it failed because access denied....'
    -- vim_language_server(lsp, epoch)
    --
    -- Might well be a configuration error on my end, however, and it also seems to be in 
    -- the `scandir` syscall - though that does specifically search for *matching* entries.
end

return vim_lsp
