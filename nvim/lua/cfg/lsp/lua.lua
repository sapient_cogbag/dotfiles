local lua = {}
local reload = require("reload")
local exepath = require("cfg.utils").exepathnil
local discovery = require("cfg.utils").discovery
local plugins = require("cfg.plugins")
local log = require("cfg.log").with_prefix("LSP").with_prefix("lua")


function lua.init()
    local lsp = require("cfg.lsp")
    local epoch = reload.epoch:get(lua)
    -- Neodev is a requirement - we register the language server once we have a
    -- neodev table to do so with nya
    --
    -- However, neodev does not add .lsp as a subtable so we have to independently
    -- require() it :'( nya
    plugins.register_onloaded("neodev", function(_neodev)
        log.info("Neodev plugin loaded - looking for lua LSP asynchronously")
        vim.schedule(function()
            local sumneko_lua = exepath("lua-language-server")
            if sumneko_lua ~= nil then
                -- Register
                lsp.register("lua", {
                    name = "sumneko-lua",
                    cmd = { sumneko_lua },
                    -- Merge config with neodev config
                    before_init = function(params, config)
                        -- This seems to be some kind of indicator... but we can force it :p
                        if config.settings == nil then config.settings = {} end
                        require("neodev.lsp").before_init(params, config)
                    end,
                    root_dir = function(bufnr, ftype, fname)
                        local fname = discovery.maybe_resolve(bufnr, fname)
                        if fname ~= nil then
                            -- Based on https://github.com/neovim/nvim-lspconfig/blob/master/lua/lspconfig/server_configurations/sumneko_lua.lua
                            -- but adapted to ourselves.
                            local any_root_files_of = {
                                '.luarc.json',
                                '.luarc.jsonc',
                                '.luacheckrc',
                                '.stylua.toml',
                                'stylua.toml',
                                'selene.toml',
                                'selene.yml',
                                -- Add .git/HEAD - a lot of lua projects have no special stuff at all
                                '.git/HEAD'
                            }

                            -- Some lua projects - like, e.g. neovim cfg files & plugins -
                            -- use a `lua/` directory nya
                            --
                            -- This takes priority over the root files thing, with the
                            -- lua/ directory found being the project directory (and not
                            -- its parent). This to a degree matches with neodev impl as well
                            local luadir = discovery.root_locate("lua", fname, false, "directory")

                            local patterns = {}
                            for _, single_pattern_item in ipairs(any_root_files_of) do
                                table.insert(patterns, { single_pattern_item })
                            end
                            local root_dir = luadir or discovery.root_pattern(fname, patterns)
                            if root_dir then return root_dir end
                        end
                    end,
                }, {
                    always_force_stop = false
                }, epoch)
            end
        end)
    end)
end

return lua
