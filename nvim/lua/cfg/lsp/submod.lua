-- Inside the directory of this, there are modules that aren't langservers
-- but are relevant to LSP
local submod = {}

submod.autocmds = require("cfg.lsp.submod.autocmds")
submod.commands = require("cfg.lsp.submod.commands")
submod.diagnostics = require("cfg.lsp.submod.diagnostics")
submod.ui = require("cfg.lsp.submod.ui")
submod.handlers = require("cfg.lsp.submod.handlers")
submod.inline_hints = require("cfg.lsp.submod.inline_hints")
submod.lsp_logging = require("cfg.lsp.submod.lsp_logging")

function submod.init(lsp)
    submod.lsp_logging.init(lsp)
    submod.autocmds.init(lsp)
    submod.commands.init(lsp)
    submod.diagnostics.init(lsp)
    submod.ui.init(lsp)
    submod.handlers.init(lsp)
    submod.inline_hints.init(lsp)
end

return submod
