-- Module for hover stuff. Auto or manual. Commands for this are done in the command module nya
local hover = {}
local reload = require("reload")
local log = require("cfg.log").with_prefix("LSP").with_prefix("submod").with_prefix("ui").with_prefix("hover")

local initial_load_epoch = reload.epoch:get(hover)

initial_load_epoch:in_each_epoch(function(new_current_epoch)
    -- Construct the autohover toggle, start as true.
    new_current_epoch:data().autohover = true
end)

---Get the autohover state for the given epoch or epoch id, or current if unspecified
---@param original_epoch_id Epoch|number?
---@return bool
function hover.is_autohover_enabled(original_epoch_id)
    original_epoch_id = reload.epoch:get(hover, original_epoch_id)
    return original_epoch_id:data().autohover
end

--- Set autohover on or off, in the given epoch (if any nya)
---@param original_epoch_id Epoch|number?
---@param new_state bool
function hover.set_autohover(original_epoch_id, new_state)
    original_epoch_id = reload.epoch:get(hover, original_epoch_id)
    original_epoch_id:data().autohover = new_state
end

--- Toggle autohover in the given epoch (if any)
---@param original_epoch_id Epoch|number?
function hover.toggle_autohover(original_epoch_id)
    original_epoch_id = reload.epoch:get(hover, original_epoch_id)
    original_epoch_id:data().autohover = not original_epoch_id:data().autohover
end

---@param curr_epoch Epoch
local function autohover(autocommands, lsp, curr_epoch)
    ---@module "cfg.lsp.submod.autocmds"
    local autocommands = autocommands
    ---@module "cfg.lsp"
    local lsp = lsp
    local audebouncer = require("cfg.audebouncer")

    -- Used for hover windows and similar things nya
    -- (e.g. function signatures). Acts to reduce how much they
    -- are closed and prevent multiple calls from moving the user into
    -- that window (which is really strange default behaviour but oh well,
    -- that's why neovim is awesome, you can fix it 😂)
    --
    -- The available options are seen in
    -- vim.lsp.util.open_floating_preview(_, _, opts <= here)
    local hover_opts = {
        -- Default is:
        -- { 'CursorMoved', 'CursorMovedI', 'InsertCharPre' }
        close_events = { "InsertLeavePre", "InsertEnter" },
        -- Stop repeated calls to hover() from moving the user INSIDE the window nya
        focus = false,
        border = "rounded",
        max_height = 20
    }

    lsp.submod.handlers.register("textDocument/hover", function(_original_handler)
        return function(...)
            return vim.lsp.with(vim.lsp.handlers.hover, hover_opts)(...)
        end
    end, "Basic hover callback with config", curr_epoch)

    lsp.submod.handlers.register("textDocument/signatureHelp", function(_original_handler)
        return function(...)
            -- original
            return vim.lsp.with(vim.lsp.handlers.signature_help, hover_opts)(...)
        end
    end, "Basic signature help callback with config", curr_epoch)


    autocommands.register_lsp_buffer_action({
        on_enter_lsp = function(bufnr)
            local callback = audebouncer.conditional(function()
                    -- No silent in nvim_create_autocmd, so we do it here.
                    vim.lsp.buf.hover()
                end)
                :with_debounce_timer(audebouncer.get_hover_window_debouncer(curr_epoch))
                :and_requires(function()
                    return hover.is_autohover_enabled(curr_epoch)
                end)

            local autocmd_id =
                vim.api.nvim_create_autocmd({ "CursorHold", "CursorMovedI" }, {
                    buffer = bufnr,
                    desc = "Autocommand for continuous presentation of the hover window.",
                    callback = callback:as_bare_function(),
                    -- Avoid spamming of not available-ness when on non-hover bits nya
                    -- NOTE - nvim_create_autocmd has no silent key RIP
                })

            return {
                -- For cleanup nya
                callback = callback,
                autocmd_id = autocmd_id
            }
        end,
        on_exit_lsp = function(_bufnr, data)
            -- Cleanup autocmd nya
            vim.api.nvim_del_autocmd(data.autocmd_id)
            data.callback:__cleanup()
        end,
        desc = "Autohover autocmd creation"
    }, curr_epoch)
end

function hover.init(lsp)
    ---@module "cfg.lsp"
    lsp = lsp
    local curr_epoch = reload.epoch:get(hover)
    log.info("Loading autohover")
    autohover(lsp.submod.autocmds, lsp, curr_epoch)
end

return hover
