-- Command definitions
local commands = {}
local reload = require("reload")
local iter = require("cfg.utils").iter
local log = require("cfg.log").with_prefix("LSP").with_prefix("commands")

local echobuilder = require("cfg.utils.echobuilder")
local set = require("cfg.utils.set")
local unpack = require("cfg.utils").unpack

local function cmd(name, command, opts, epoch)
    if epoch:in_epoch() then
        vim.api.nvim_create_user_command(name, command, opts)
        epoch:cleanup(function()
            vim.api.nvim_del_user_command(name)
        end)
    end
end

-- Produce the status of a single LSP client. Optionally, provide the current buffer
--  and it will be marked as such in the list of attached buffers nya. This returns
--  a list of elements to be attached into vim.api.nvim_echo and combined with other
--  messages by appending to a long table list
--
-- First argument is echobuilder.
local function single_client_status(eb, lsp_client, local_bufnr)
    -- String for the status of an lsp client, along with it's highlight group,
    -- in a little table pair ^.^
    local function statusfmtstring(client)
        if client.is_stopped() then
            return { "Stopped", "WarningMsg" }
        else
            return { "Running", "MoreMsg" }
        end
    end

    -- String to use for the given buffer number when referring to it in the list nya
    --
    -- This also works in the case that the bufnr is invalid.
    local function bufnr_string(bufnr)
        if not vim.api.nvim_buf_is_valid(bufnr) then return "[INVALID BUFFER]" end
        local filename = vim.api.nvim_buf_get_name(bufnr)
        if string.len(filename) == 0 then
            filename = nil
        end

        return filename or "[No Name]"
    end

    eb:msg({
        { lsp_client.name, "WarningMsg" }, (" [ID: %s]"):format(lsp_client.id)
    })
    -- List of the various properties of this client nya
    eb:with_inner_list(" ", function()
        -- Each of these is an element in the list of client properties ^.^
        local title_hl = "ModeMsg"
        local value_hl = "None"
        local list_marker = " - "
        eb:msg { { "Status: ", title_hl } }
        -- This just acts as a trivial 1-element list for nice fmt :)
        eb:with_inner_list(list_marker, function()
            eb:msg({ statusfmtstring(lsp_client) })
        end)
        local root_dir = lsp_client.config.root_dir or "(none)"
        eb:msg { { "Root Directory (config): ", title_hl } }
        eb:with_inner_list(list_marker, function()
            eb:msg { { root_dir, value_hl } }
        end)

        eb:msg { { "Workspace Directories (initial config): ", title_hl } }

        local workspace_dirs = lsp_client.config.workspace_folders or {}
        eb:with_inner_list(list_marker, function()
            if #workspace_dirs < 1 then
                eb:msg { { "(none)", value_hl } }
            else
                for _idx, ws_dir in ipairs(workspace_dirs) do
                    eb:msg { { ws_dir, value_hl } }
                end
            end
        end)

        local live_workspace_dirs = lsp_client.workspace_folders or {}
        eb:msg({ { "Workspace Directories (live): ", title_hl } })
        eb:with_inner_list(list_marker, function()
            if #live_workspace_dirs < 1 then
                eb:msg { { "(none)", value_hl } }
            else
                for _idx, ws_dir in ipairs(live_workspace_dirs) do
                    eb:msg { { ws_dir.uri, value_hl } }
                end
            end
        end)

        -- Buffer list nya
        local attached_buffers = vim.lsp.get_buffers_by_client_id(lsp_client.id)
        eb:msg({ { "Attached Buffers", title_hl } })
        eb:with_inner_list(list_marker, function()
            if #attached_buffers < 1 then
                eb:msg("(none)")
            else
                for _idx, bufnr in ipairs(attached_buffers) do
                    -- MoreMsg is a nice alt colour
                    if local_bufnr ~= nil and local_bufnr == bufnr then
                        eb:msg({
                            ("[%s] "):format(bufnr),
                            bufnr_string(bufnr),
                            " (current)"
                        }, nil, "MoreMsg")
                    else
                        eb:msg({ ("[%s] "):format(bufnr), bufnr_string(bufnr) }, nil, value_hl)
                    end
                end
            end
        end)
    end)
end

-- LspStatus command
function commands.status(lsp, curr_epoch)
    curr_epoch = curr_epoch:for_module(commands)
    cmd("LspStatus", function(args)
        local is_global = args.bang
        local language = args.args
        local curr_bufnr = vim.api.nvim_get_current_buf()
        if language and string.len(language) == 0 then
            language = nil
        end
        local clientset = set.new()
        if is_global then
            if language then
                clientset = lsp.get_current_managed_client_ids_per_language()
            else
                clientset = lsp.get_current_managed_client_ids() or set.new()
            end
        else
            vim.lsp.for_each_buffer_client(curr_bufnr, function(client, client_id, bufnr)
                if language then
                    if (lsp.get_current_managed_client_ids_per_language()[language] or set.new())[client_id] then
                        clientset[client_id] = true
                    end
                else
                    -- Ensure we use this epoch ^.^ nya
                    clientset[client_id] = lsp.get_current_managed_client_ids()[client_id]
                end
            end)
        end

        local echobuilder = echobuilder:new()
        local created_cnt = 0
        for client_id in iter(clientset) do
            created_cnt = created_cnt + 1
            -- Seems to be nil sometimes
            local lspclient = vim.lsp.get_client_by_id(client_id)
            if lspclient then
                single_client_status(echobuilder, lspclient, curr_bufnr)
            else
                echobuilder:msg(("Client ID [%s] did not have a client!"):format(client_id), false, "WarningMsg")
            end
        end
        if created_cnt == 0 then
            echobuilder:msg("No LSP Clients Attached")
        end
        echobuilder:echo_bulk()
    end, {
        bang = true,
        nargs = '?',
        desc = "View status of LSP for local buffer or all buffers (!), and for all languages or just one",
        complete = function(_arglead, _cmdline, _cursorpos)
            -- No need to manually filter as per documentation nya
            return lsp.running_langs()
        end
    }, curr_epoch)
end

-- LspStop
function commands.stop(lsp, curr_epoch)
    curr_epoch = curr_epoch:for_module(commands)
    cmd("LspStop", function(args)
        local is_force = args.bang
        local languages_if_any = args.fargs
        if #languages_if_any < 1 then
            languages_if_any = nil
        else
            languages_if_any = set.new(unpack(languages_if_any))
        end
        lsp.stop_all(languages_if_any, is_force)
    end, {
        bang = true,
        nargs = '*',
        desc = "Stop all language servers. Arguments provided allow filtering for languages."
            .. " With `!`, will force-stop all clients in the set",
        complete = function(_arglead, _cmdline, _cursorpos)
            return lsp.running_langs()
        end
    }, curr_epoch)
end

-- LspScan
function commands.scan(lsp, curr_epoch)
    curr_epoch = curr_epoch:for_module(commands)
    cmd("LspScan", function(args)
        local is_global = args.bang
        local language = args.args
        if language and string.len(language) == 0 then
            language = nil
        end

        local buflist
        if is_global then
            buflist = {}
            for _, bufnr in ipairs(vim.api.nvim_list_bufs()) do
                if vim.api.nvim_buf_is_valid(bufnr) then
                    local buffer_ft = vim.bo[bufnr].ft
                    -- Without checking ft we get warnings for stuff like help buffers >.< nya
                    if (language == nil and set.new(unpack(lsp.langs()))[buffer_ft]) or buffer_ft == language then
                        table.insert(buflist, bufnr)
                    end
                end
            end
        else
            buflist = { vim.api.nvim_get_current_buf() }
        end
        -- Rescan
        for _, bufnr in ipairs(buflist) do
            if is_global then
                -- If language isn't nil that was used to filter bufs
                lsp.scan_registered_servers_for_buffer(bufnr, nil, nil, curr_epoch)
            else
                -- If language was not nil then we forcefully reload the current
                -- buffer as if it was that language nya
                lsp.scan_registered_servers_for_buffer(bufnr, language, nil, curr_epoch)
            end
        end
    end, {
        bang = true,
        nargs = '?',
        desc = "Scan LSP for just this buffer or all buffers (!). Will reuse existing LSP without :LspStop" ..
            "If done for one buffer, then specifying a language forces that buffer to be scanned as-if it's that language."
            ..
            "If done for all buffers, then specifying a language restricts the rescan to buffers of the given filetype",
        complete = function(_arglead, _cmdline, _cursorpos)
            return lsp.running_langs()
        end
    }, curr_epoch)
end

-- :LspReroot
---@param lsp any
---@param curr_epoch Epoch
function commands.reroot(lsp, curr_epoch)
    ---@module 'cfg.lsp'
    lsp = lsp
    curr_epoch = curr_epoch:for_module(commands)
    cmd("LspReroot", function()
        local curr_buf = vim.api.nvim_get_current_buf()
        -- wrap changing directories to check and throw errors - empty string return of 
        -- vim.fn.chdir means errors nya
        --
        -- Else returns 
        local luawrap_cd = function (new_dir)
            -- I think chdir actually throws errors itself, rather than doing the vimscript behaviour
            -- nya
            local result = vim.fn.chdir(new_dir)
            if result == "" then error("Could not change to (perhaps invalid) directory: " .. new_dir) end
            return result
        end

        --- Either switch to the given path and notify the user (returning true),
        --- or return false.
        --- Important because it checks for failed switch nya
        ---@param filepath string
        ---@return bool # if the switch actually worked nya
        local function switch_or_false(filepath)
            local did_switch, err = pcall(luawrap_cd, filepath)
            if did_switch then
                vim.notify("[LspReroot] Switched to directory: " .. filepath, vim.log.levels.INFO)
                return true
            else
                return false
            end
        end

        --- Attempt to identify a directory URI and switch to it nya
        --- If it is not a file URI or switch failed, notify the user and return
        --- false nya. Else return true
        --- @param maybe_file_uri string
        --- @return bool # if there was actually a valid uri and it was a valid directory
        ---  that was successfully cd'd to nya
        local function switch_uri_or_false(maybe_file_uri)
            if vim.startswith(maybe_file_uri, "file://") then
                local raw_path = vim.uri_to_fname(maybe_file_uri)
                return switch_or_false(raw_path)
            else
                return false
            end
        end

        -- Priority:
        -- * live workspace locations
        -- * If none of them are filepaths, then look at root_dir specified in the config
        -- * Then, warn that we can't reroot nya
        -- Usually the first workspace folder is going to be for the actual project, so it's 
        -- what we use :) nya
        --
        -- These are checked for each client in sequence.
        ---@type table<string, any>[]
        local clients = vim.lsp.get_clients { bufnr = curr_buf }
        if #clients == 0 then vim.notify("[LspReroot] No attached LSP clients for this buffer!", vim.log.levels.WARN); return end

        for _, client in ipairs(clients) do
            -- Live workspace folders nya
            for _idx, folder_uri in ipairs(client.workspace_folders or {}) do
                -- Live workspace directories have both names and URIs as a table
                if switch_uri_or_false(folder_uri.uri) then return end
            end

            -- Config workspace folders
            for _idx, folder_uri in ipairs(client.config.workspace_folders or {}) do
                if switch_uri_or_false(folder_uri) then return end
            end

            --- Root directory. This one isn't a uri
            if client.config.root_dir then
                if switch_or_false(client.config.root_dir) then return end
            end
        end
        vim.notify("[LspReroot] could not switch successfully to root directory for buffer", vim.log.levels.WARN)
    end,
        { desc = "Identify the most accurate root directory of the LSP client(s) for the current buffer, and set the current directory to it" }
        , curr_epoch)

end

-- :LspHover/Auto/etc. nya
---@param curr_epoch Epoch
function commands.hover(curr_epoch)
    local hover = require("cfg.lsp.submod.ui.hover")
    cmd("LspHoverToggle", function()
        hover.toggle_autohover(curr_epoch)
        local autohover = hover.is_autohover_enabled(curr_epoch)
        if autohover then
            -- vim.notify or echo is for actual messages - logging is for
            -- logging, even if we like lots of it nya
            vim.notify("LSP auto-hover turned on")
        else
            vim.notify("LSP auto-hover turned off")
        end
    end, { desc = "Toggle if autohover is on or off" }, curr_epoch)

    cmd("LspHover", function()
        vim.lsp.buf.hover()
    end, { desc = "LSP show hover manually" }, curr_epoch)

    cmd("LspHoverStatus", function()
        if hover.is_autohover_enabled(curr_epoch) then
            vim.notify("LSP auto-hover is enabled")
        else
            vim.notify("LSP auto-hover is disabled")
        end
    end, { desc = "Print out whether or not hover is enabled" }, curr_epoch)


    cmd("LspSignatureHelp", function()
        vim.lsp.buf.signature_help()
    end, { desc = "Manually request LSP signature help" }, curr_epoch)

    cmd("LspDiagnosticHover", function()
        vim.diagnostic.open_float()
    end, { desc = "Manually request to look at diagnostic hover information" }, curr_epoch)
end

---@param lsp any
---@param curr_epoch Epoch
function commands.force_gc(lsp, curr_epoch)
    ---@module "cfg.lsp"
    lsp = lsp
    cmd("LspPrune", function ()
        lsp.gc_clients()
    end, { desc = "Force closure of LSPs that are not attached to any buffers." }, curr_epoch)
end

function commands.init(lsp)
    local curr_epoch = reload.epoch:get(commands)

    commands.status(lsp, curr_epoch)
    commands.stop(lsp, curr_epoch)
    commands.scan(lsp, curr_epoch)
    commands.hover(curr_epoch)
    commands.reroot(lsp, curr_epoch)
    commands.force_gc(lsp, curr_epoch)
end

return commands
