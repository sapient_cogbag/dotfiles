-- Module for the construction of autocmds and management of autocmd state with
-- relation to LSP
local autocmds = {}
local reload = require("reload")
local log = require("cfg.log").with_prefix("LSP").with_prefix("submod").with_prefix("autocmds")


-- This area is the original reason we decided to use epochs.
--
-- Overlaps were causing serious problems with LspAttach/Detach nya
reload.epoch:get(autocmds):in_each_epoch(function(epoch)
    -- Map from the bufnr to the current count of LSP servers on it.
    epoch:data().lsp_buffer_counts = {}

    -- Map from augid -> (bufnr -> data returned by the
    -- lsp_attach callback) from lsp.lsp_buffer_configuration nya
    epoch:data().lsp_buffer_callback_data = {}
end)

reload.epoch:get(autocmds):in_each_epoch(function(epoch)
    -- This autocommand ensures that if a buffer is attached to a client, then
    -- when the buffer is deleted it will get the client to detach from the buffer first.
    --
    -- As far as I can tell, this isn't actually done at all by the lsp implementation.
    --
    -- This is important, because whenever a language server reinitializes, the LSP client
    -- will attempt to reattach to all buffers inside it's internal table. If any buffers
    -- have become invalid without being detached, the implementation of `_on_attach` (the
    -- internal component of `lsp.buf_attach_client`) is called on all of them, and it does
    -- not handle invalid `bufnr`s.
    --
    -- So we add our own LspAttach callback (which is called on all clients), that can then
    -- make it so that if a buffer is deleted it is automatically detached from the client.
    --
    -- The autocmd group is deleted at the end of the epoch, but that is fine because all
    -- clients are killed too lol
    --
    -- This still doesn't fix it though? it's very bizzare nya (and quite frustrating...).

    local augid = vim.api.nvim_create_augroup("lsp-client-detach-on-buffer-deletion", { clear = true })

    vim.api.nvim_create_autocmd({ "LspAttach" }, {
        group = augid,
        desc = "Add autocmds to attached buffers so their deletion detaches them from the LSP client",
        callback = function(event)
            local bufnr = event.buf
            local client = vim.lsp.get_client_by_id(event.data.client_id)
            if client ~= nil then
                -- Create the buffer-local autocmd
                --
                -- `BufUnload` is not something ok for this as unloading seems to be quite common and will result in weird duplication.
                vim.api.nvim_create_autocmd({ "BufDelete" }, {
                    group = augid,
                    buffer = bufnr,
                    desc = "Detach this buffer from LSP client - [" .. client.name .. "] - when the buffer is deleted",
                    callback = function(buflocal_event)
                        -- only detach if the buffer is actually still attached to the client.
                        if client.attached_buffers[bufnr] then
                            -- If the buffer is invalid, buf_detach_client won't work, so we just delete it from the array 
                            -- directly.
                            --[[if not vim.api.nvim_buf_is_valid(bufnr) then
                                client.attached_buffers[bufnr] = nil
                            else]]--
                            vim.lsp.buf_detach_client(bufnr, client.id)
                            -- end
                        end
                    end
                })
            end
        end
    })

    epoch:cleanup(function()
        vim.api.nvim_del_augroup_by_id(augid)
    end, "[lsp - clean out autocmd that ensures deleted buffers are detached from lsp clients]")
end)

-- Note that currently, there are issues with defining autocmds inside buffers, where something weird
-- happens with fzf-lua selection that results in them just not being a thing for some reason
--
-- As such, we are redoing this system, providing a mechanism for cleanup on epoch end vs on LSP Attach/Detach.
-- Issues seem to be related to the fact that the ordering of epoch cleanup is (intentionally) not defined,
-- so closing LSP clients on reload can cause detaches before or after
reload.epoch:get(autocmds):in_each_epoch(function(epoch)
    ---@type table<number, number>
    --- Map of bufnr to the number of LSP clients attached to it.
    ---
    --- Initialised with existing values nya
    epoch:data().buffer_clients_attached = {}
    for _idx, bufnr in ipairs(vim.api.nvim_list_bufs()) do
        epoch:data().buffer_clients_attached[bufnr] = #(vim.lsp.get_clients({ bufnr = bufnr }))
    end

    ---@type table<number, cfg.lsp.autocmds.lsp_buffer_action_registration_data>
    epoch:data().lsp_buffer_actions = {}
    epoch:data().next_lsp_buffer_action_id = 1

    local augid = vim.api.nvim_create_augroup("lsp-buffer-attached-count-maintainance", { clear = true })

    vim.api.nvim_create_autocmd({ "LspAttach" }, {
        group = augid,
        desc = "Used to maintain LSP buffer counts on attach, and run lsp buffer actions",
        callback = function(event)
            local bufnr = event.buf
            if epoch:in_epoch() then
                local buffer_clients_attached = epoch:data().buffer_clients_attached
                if buffer_clients_attached[bufnr] == nil then
                    buffer_clients_attached[bufnr] = 0
                end

                local previous_attached_client_count = buffer_clients_attached[bufnr]
                buffer_clients_attached[bufnr] = previous_attached_client_count + 1

                -- Often, the bufnr is not valid for one reason or another. This might be to do with ephemeral buffers in fzf
                -- or similar nya. But either way, it's annoying. So check!
                if not vim.api.nvim_buf_is_valid(bufnr) then
                    return
                end

                -- This was the first one, run the callback!
                if previous_attached_client_count == 0 then
                    ---@type table<number, cfg.lsp.autocmds.lsp_buffer_action_registration_data>
                    local lsp_buffer_actions = epoch:data().lsp_buffer_actions
                    for _action_id, buffer_action in pairs(lsp_buffer_actions) do
                        buffer_action.buffer_data[bufnr] = {
                            data = buffer_action.action.on_enter_lsp(bufnr),
                            cleaned = false
                        }
                    end
                end
            end
        end
    })

    vim.api.nvim_create_autocmd({ "LspDetach" }, {
        group = augid,
        desc = "Used to maintain LSP buffer counts on detach, and run lsp buffer actions",
        callback = function(event)
            local bufnr = event.buf
            if epoch:in_epoch() then
                local buffer_clients_attached = epoch:data().buffer_clients_attached
                if buffer_clients_attached[bufnr] == nil or buffer_clients_attached[bufnr] <= 0 then
                    log.warn(
                        "Attempt to detach from a buffer #%s with no count or 0 or -ive count. Aborting due to inconsistency"
                        , bufnr)
                    return
                end

                local previous_attached_client_count = buffer_clients_attached[bufnr]
                buffer_clients_attached[bufnr] = previous_attached_client_count - 1

                -- Often, the bufnr is not valid for one reason or another. This might be to do with ephemeral buffers in fzf
                -- or similar nya.
                --
                -- `on_exit_lsp` implementations must deal with this because they might need
                -- to clean up a buffer that has "become" invalid.
                --[[
                if not vim.api.nvim_buf_is_valid(bufnr) then
                    return
                end]]
                --

                -- This was the last one, run the cleanups! nya
                if previous_attached_client_count == 1 then
                    ---@type table<number, cfg.lsp.autocmds.lsp_buffer_action_registration_data>
                    local lsp_buffer_actions = epoch:data().lsp_buffer_actions
                    for action_id, buffer_action in pairs(lsp_buffer_actions) do
                        if not buffer_action.buffer_data[bufnr] then
                            log.warn("No buffer data on detach for (action:#%s, bufnr:%s)", action_id, bufnr); return
                        end
                        if not buffer_action.buffer_data[bufnr].cleaned then
                            local data = buffer_action.buffer_data[bufnr].data
                            buffer_action.buffer_data[bufnr].data = nil
                            buffer_action.buffer_data[bufnr].cleaned = true
                            buffer_action.action.on_exit_lsp(bufnr, data)
                        end
                    end
                end
            end
        end
    })

    epoch:cleanup(function()
        -- Dispose of the autogroup for maintaining counts nya
        vim.api.nvim_del_augroup_by_id(augid)

        ---@type table<number, cfg.lsp.autocmds.lsp_buffer_action_registration_data>
        local lsp_buffer_actions = epoch:data().lsp_buffer_actions
        local buffer_client_counts = epoch:data().buffer_clients_attached

        for bufnr, cnt in pairs(buffer_client_counts) do
            -- Any buffers that have more than zero clients need to have their associated actions
            -- cleaned up
            if cnt > 0 then
                for action_id, buffer_action in pairs(lsp_buffer_actions) do
                    if not buffer_action.buffer_data[bufnr].cleaned then
                        local data = buffer_action.buffer_data[bufnr].data
                        buffer_action.buffer_data[bufnr].data = nil
                        buffer_action.buffer_data[bufnr].cleaned = true
                        -- Now run on_exit_lsp
                        buffer_action.action.on_exit_lsp(bufnr, data)
                    end
                end
            end
        end
    end, "[lsp - lsp buffer action cleanup]")
end)

---@generic BufLocalData Data this callback can produce that is passed to it's cleanup
---@class cfg.lsp.autocmds.lsp_buffer_action Action to be executed when a buffer has gained at least
---  one LSP client, or this epoch has started and it still has one or more, along with actions to cleanup
---  either when the buffer loses it's last attached LSP client, or the epoch ends.
---@field on_enter_lsp fun(bufnr: number): BufLocalData Called when the buffer has gone from
---   zero to one LSP client, or on all clients that have more than one at time of registration
---   Returns data that is passed to the cleanup.
---
---   This will never be invoked on a `bufnr` that was invalid at time of LSP attachment. However, valid
---   `bufnr` values may become invalid by the time `on_exit_lsp` has been called.
---@field on_exit_lsp fun(bufnr: number, data: BufLocalData) Called when the buffer has gone from
---   one to zero LSP client, or when the epoch is ending even if there are still LSP clients on the buffer.
---
---   `bufnr` may not be a valid neovim buffer. This should be checked with `vim.api.nvim_buf_is_valid` if
---   any operations need a buffer to be valid (which is very likely).
---   Further, remember that a previously-valid buffer passed into `on_enter_lsp` may have become invalid
---   by the time `on_exit_lsp` is called.
---@field desc string? Description (for debugging purposes).
---
---@param buffer_action cfg.lsp.autocmds.lsp_buffer_action Buffer action to register
---@param epoch_id Epoch|number? Epoch it should be registered in. Stuff simply will not be run
---  outside of the current epoch.
function autocmds.register_lsp_buffer_action(buffer_action, epoch_id)
    local epoch = reload.epoch:get(autocmds, epoch_id)
    if epoch:in_epoch() then
        local epoch_data = epoch:data()
        local id = epoch_data.next_lsp_buffer_action_id
        epoch_data.next_lsp_buffer_action_id = id + 1
        log.info("registering LSP buffer action #%s (%s)", id, buffer_action.desc or "[no desc]")

        ---@class cfg.lsp.autocmds.lsp_buffer_action_buffer_local_data
        ---@field data BufLocalData the actual data value
        ---@field cleaned bool Used to prevent duplicate calls to the cleanup function in the
        --- case where finalisation - which doesn't decrement cnt - leaves exactly one, and
        --- then detach occurs a little bit later before further epoch cleanup happens and
        --- calls are duplicated nya
        ---@class cfg.lsp.autocmds.lsp_buffer_action_registration_data<BufLocalData>
        ---@field action cfg.lsp.autocmds.lsp_buffer_action<BufLocalData> the action itself
        ---@field buffer_data table<number, cfg.lsp.autocmds.lsp_buffer_action_buffer_local_data>
        --- data for buffers that have entered "LSP mode"
        ---@type table<number, cfg.lsp.autocmds.lsp_buffer_action_registration_data>
        epoch_data.lsp_buffer_actions[id] = {
            ---@type cfg.lsp.autocmds.lsp_buffer_action
            action = buffer_action,
            buffer_data = {}
        }
        -- Run on all the buffers that currently have counts > 0,
        for bufnr, cnt in pairs(epoch_data.buffer_clients_attached) do
            if cnt > 0 and vim.api.nvim_buf_is_valid(bufnr) then
                local resulting_data = buffer_action.on_enter_lsp(bufnr)
                epoch_data.lsp_buffer_actions[id].buffer_data[bufnr] = {
                    data = resulting_data,
                    cleaned = false
                }
            end
        end
    end
end

-- Callback for all attach events for a group.
---@generic Data
---@param augid number
---@param lsp_attach fun(event: object, bufnr: number): `Data`?
---@param event object
---@param original_epoch Epoch
local function lsp_buffer_on_attach(augid, lsp_attach, event, original_epoch)
    if original_epoch:in_epoch() then
        local bufnr = event.buf or vim.api.nvim_get_current_buf()
        original_epoch:data().lsp_buffer_counts[bufnr] = (original_epoch:data().lsp_buffer_counts[bufnr] or 0) + 1
        if original_epoch:data().lsp_buffer_counts[bufnr] == 1 then
            local attach_results = lsp_attach(event, bufnr)
            original_epoch:data().lsp_buffer_callback_data[augid] = original_epoch:data().lsp_buffer_callback_data[augid
            ] or {}
            original_epoch:data().lsp_buffer_callback_data[augid][bufnr] = attach_results
        end
    end
end

-- Callback for all detach events for a group. Last argument is the buffer id,
-- so that nil events are fine, in the case of iterating for a cleanup.
---@generic Data
---@param augid number
---@param lsp_detach fun(event: object?, attach_results: Data, bufnr: number)
---@param event object?
---@param original_epoch Epoch
---@param inbuf number? Must be set if event is nil
---@param finalising bool? If event is nil this is probably true nya
local function lsp_buffer_on_detach(augid, lsp_detach, event, original_epoch, inbuf, finalising)
    local bufnr = (inbuf ~= nil and inbuf) or (event.buf or vim.api.nvim_get_current_buf())
    if original_epoch:in_epoch() then
        original_epoch:data().lsp_buffer_counts[bufnr] = (original_epoch:data().lsp_buffer_counts[bufnr] or 1) - 1
        -- Last lsp just got dropped
        if original_epoch:data().lsp_buffer_counts[bufnr] == 0 or finalising then
            local attach_results = (original_epoch:data().lsp_buffer_callback_data[augid] or {})[bufnr]
            -- Reset the buffer callback data just in case
            original_epoch:data().lsp_buffer_callback_data[augid] = original_epoch:data().lsp_buffer_callback_data[augid
            ] or {}
            original_epoch:data().lsp_buffer_callback_data[augid][bufnr] = nil
            lsp_detach(event, attach_results, bufnr)
        end
    end
end

-- Function that creates autocmd groups for LSP buffers, but ensures that the
-- autocmd callbacks for attach/detach are only called when going from
-- no language servers to one on a buffer, or going from one to zero, respectively nya
--
-- Only registers in the case that we are actually in the specified epoch (if any nya)
---
---@generic Data
---@param autogroup string Name of the autocmd group to use. Will be cleared.
---@param lsp_attach fun(event: object, bufnr: number): `Data`? takes the same argument as the callback
--  for nvim_create_autocmd, called when attaching the first LSP client to a buffer.
--  Can return data that will be passed as a second argument to lsp_detach when needed.
--  Data is stored per buffer and per autogroup id. The buffer ID is passed as the last
--  argument even for nil events (though in attach, event should never be nil)
--
---@param lsp_detach fun(event: object?, data: Data, bufnr: number)
--  takes the same argument as the callback for
--  nvim_create_autocmd, called when the last language client on a buffer is detached.
--  A second argument, containing any data returned by lsp_attach, is also passed for use
--  in e.g. cleanup of keybinds or bufferlocal autogroups/cmds ^.^, and a third argument,
--  the bufnr this is running on, is passed as well - even when the event is nil during epoch
--  cleanup nya
--
--  It is often desireable to run detach code also on the end of an epoch. This function
--  registers an epoch cleanup hook that will run your cleanup hook - with a `nil` event
--
---@param for_epoch Epoch|number? - The epoch this is for. Defaults to current epoch for
--  when you just synchronously do this nya.
--
---@return number? augid The autogroup ID for the LspAttach and LspDetach autocmds created nya
--   Cleanup is automatically managed. Returns nil if not in epoch
---@deprecated see autocmds.register_lsp_buffer_action
function autocmds.lsp_buffer_configuration(autogroup, lsp_attach, lsp_detach, for_epoch)
    local epoch = reload.epoch:get(autocmds, for_epoch)
    if epoch:in_epoch() then
        local augid = vim.api.nvim_create_augroup(autogroup, { clear = true })
        vim.api.nvim_create_autocmd("LspAttach", {
            group = augid,
            callback = function(event)
                lsp_buffer_on_attach(augid, lsp_attach, event, epoch)
            end
        })
        vim.api.nvim_create_autocmd("LspDetach", {
            group = augid,
            callback = function(event)
                lsp_buffer_on_detach(augid, lsp_detach, event, epoch)
            end
        })

        -- Add cleanup and special running of cleanup functions
        --
        -- The final detaches won't be run until after the epoch ends (which means they
        -- won't be run at all, as they will be deleted nya), so we reset everything here
        -- with nil events.
        epoch:cleanup(function()
                for bufnr, cnt in pairs(epoch:data().lsp_buffer_counts) do
                    if cnt > 0 then
                        -- Last argument allows forcing the callback to run - we are finalising,
                        -- so we want to do this.
                        lsp_buffer_on_detach(augid, lsp_detach, nil, epoch, bufnr, true)
                    end
                end

                -- Delete autocmd group
                vim.api.nvim_del_augroup_by_id(augid)
            end,
            "[lsp buffer-cfg cleanup - " .. autogroup .. " (augid: " .. augid .. ")]")
        return augid
    end
end

function autocmds.init(lsp)
    local epoch = reload.epoch:get(autocmds)
end

return autocmds
