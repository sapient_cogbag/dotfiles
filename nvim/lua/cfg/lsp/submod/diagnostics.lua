--- Module for configuring all the diagnostics stuff nya
local diagnostics = {}

local reload = require("reload")
local sign = require("cfg.utils.vimscript").sign


-- Table of information for each diagnostic level type
-- map from "Sign/HighlightGroup text postfixes" -> metadata like sign text and virtual text colour.
-- 
-- Note that by default all the various diagnostic highlight groups link to Diagnostic<Postfix> nya, so just specify specific
-- colours.
local diagnostic_specifiers = {}

diagnostic_specifiers.Error = { sign_text = "🞮|", colour = "#ff0000" }
diagnostic_specifiers.Warn = { sign_text = "|", colour = "orange" }
diagnostic_specifiers.Hint = { sign_text = "󰌵|", colour = "#ffff00" }
-- Colour is quite bright if the theme is light mode... provide an alternative nya
if vim.go.background == "light" then
    diagnostic_specifiers.Hint.colour = "#aaaa00"
end 

diagnostic_specifiers.Info = { sign_text = "|", colour = "white" }
if vim.go.background == "light" then
    diagnostic_specifiers.Info.colour = "black"
end



---Table of diagnostic sign text postfixes -> sign text nya
---Sign names are "DiagnosticSign" .. key
local signs = {}
signs.Error = "🞮|"
signs.Warn = "|"
signs.Hint = "󰌵|"
signs.Info = "|"

function diagnostics.init(lsp)
    local curr_epoch = reload.epoch:get(diagnostics)

    for postfix, data in pairs(diagnostic_specifiers) do
        sign { name = "DiagnosticSign" .. postfix, text =  data.sign_text }
        vim.api.nvim_set_hl(0, "Diagnostic" .. postfix,  { fg = data.colour })
    end

    vim.diagnostic.config({
        severity_sort = true
    })
end

return diagnostics
