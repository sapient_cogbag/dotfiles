--- Module for managing LSP handler configuration across my config.
local handlers = {}
local reload = require("reload")
local first_epoch = reload.epoch:get(handlers)

first_epoch:in_each_epoch(function (epoch)
    --- In practise the first argument so far is always nil in vim.lsp impl nya 
    ---@alias cfg.lsp.submod.handler fun(_: nil, ...) 
    ---@type table<string, cfg.lsp.submod.handler>
    --- This is the old default handlers from `vim.lsp.handlers`, when they have been replaced
    --- nya
    epoch:data().old_default_handlers = {} 

    epoch:cleanup(function ()
        for old_handler_string, old_handler in pairs(epoch:data().old_default_handlers) do
            vim.lsp.handlers[old_handler_string] = old_handler
        end 
        epoch:data().old_default_handlers = {}
    end, "[lsp - handlers - restore old handlers]")
end)

-- Update the registered handler for the given event if in the given epoch nya
---
---@param handler_name string 
---@param generator fun(original_handler: cfg.lsp.submod.handler): cfg.lsp.submod.handler 
---@param desc string? Optional extra description for the handler in debugging
---@param epoch_id Epoch|number?
function handlers.register(handler_name, generator, desc, epoch_id)
    local epoch = reload.epoch:get(handlers, epoch_id)
    if epoch:data().old_default_handlers[handler_name] ~= nil then
        local errstring = "Registering handler for '" .. handler_name .. "'"
        if desc then
            errstring = errstring .. " (" .. desc .. ") "
        end
        errstring =  errstring .. ", but an existing handler was already registered!"
    end
    local old_handler = vim.lsp.handlers[handler_name]
    if old_handler == nil then
        error("No existing handler for " .. handler_name)
    end
    epoch:data().old_default_handlers[handler_name] = old_handler
    vim.lsp.handlers[handler_name] = generator(old_handler)
end

function handlers.init(lsp)
    ---@module "cfg.lsp"
    lsp = lsp
end

return handlers
