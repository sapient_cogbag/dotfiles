-- Small module to provide inline hints

local inline_hints = {}
local reload = require("reload")
local log = require("cfg.log").with_prefix("LSP").with_prefix("submod").with_prefix("inline_hints")

function inline_hints.init(lsp)
    --- @module 'cfg.lsp'
    local lsp = lsp
    lsp.submod.autocmds.register_lsp_buffer_action({
        desc = "Enable inlay hints",
        on_enter_lsp = function(bufnr)
            vim.lsp.inlay_hint.enable(true, { bufnr = bufnr })
            return {}
        end,
        on_exit_lsp = function(bufnr, _empty)
            if not vim.api.nvim_buf_is_valid(bufnr) then return end
            vim.lsp.inlay_hint.enable(false, { bufnr = bufnr})
        end
    }, reload.epoch:get(inline_hints))

    log.info("Enabled inlay hints for LSP")
end

return inline_hints
