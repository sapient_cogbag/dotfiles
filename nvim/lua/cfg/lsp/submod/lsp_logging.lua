--  Configure @see |vim.lsp.log|
local lsp_logging = {}

function lsp_logging.init(lsp)
    -- @module cfg.lsp
    lsp = lsp
end

return lsp_logging
