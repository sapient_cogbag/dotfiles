--- Module for creating UI for LSP buffers and just general niceties.
--- Uses autocmds.
local ui = {}
ui.hover = require("cfg.lsp.submod.ui.hover")

function ui.init(lsp) 
    ui.hover.init(lsp)
end

return ui
