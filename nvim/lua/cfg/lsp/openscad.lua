-- This module is for OpenSCAD files.
--
-- Note: OpenSCAD is looking at embedding an LangServer directly in their binary nya:
-- https://github.com/openscad/openscad/pull/3635/commits
--
-- For now, this is not merged. Instead, we use: https://github.com/Leathong/openscad-LSP

local openscad = {}
local reload = require("reload")

local exepath = require("cfg.utils").exepathnil
local discovery = require("cfg.utils").discovery
local log = require("cfg.log").with_prefix("LSP").with_prefix("openscad")
local utils = require("cfg.utils")

local function openscad_lsp(lsp, epoch)
    --- @module "cfg.lsp"
    lsp = lsp
    local openscad_lsp_exe = exepath("openscad-lsp")
    if openscad_lsp_exe == nil then
        log.warn("Did not find openscad-lsp executable")
        return
    end
    log.info("Found openscad-lsp")
    lsp.register("openscad", {
        name = "openscad-lsp",
        cmd = { openscad_lsp_exe, "--stdio" },
        root_dir = function(bufnr, ftype, fname)
            fname = discovery.maybe_resolve(bufnr, fname)
            local root_structure_any_of = {
                discovery.patterns.GIT
            }
            return discovery.root_pattern(fname, root_structure_any_of)
        end
    }, nil, epoch)
end


function openscad.init()
    local lsp = require("cfg.lsp")
    local epoch = reload.epoch:get(openscad)
    openscad_lsp(lsp, epoch)
end

return openscad
