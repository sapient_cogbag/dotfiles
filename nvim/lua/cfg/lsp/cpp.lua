-- This module is actually for all the c/cpp family langs nya
--
-- Our primary LSP is clangd nya
local cpp = {}
local reload = require("reload")

local exepath = require("cfg.utils").exepathnil
local discovery = require("cfg.utils").discovery
local log = require("cfg.log").with_prefix("LSP").with_prefix("cpp")

local function clangd(lsp, epoch)
    local clangd_exe = exepath("clangd")
    if clangd_exe == nil then
        log.warn("Did not find clangd executable.")
        return
    end
    log.info("Found clangd.")

    lsp.register({
        "c",
        "cpp",
        "objc",
        "objcpp"
    }, {
        name = "clangd",
        cmd = { clangd_exe },
        -- See: https://github.com/neovim/nvim-lspconfig/blob/master/lua/lspconfig/server_configurations/clangd.lua
        --  for how this is done ^.^ nya 
        root_dir = function (bufnr, ftype, fname)
            local fname = discovery.maybe_resolve(bufnr, fname)
            if fname == nil then return nil end
            local root_structure_any_of = {
                {'.clangd'},
                {'.clang-tidy'},
                {'.clang-format'},
                {'compile_commands.json'},
                {'compile_flags.txt'},
                {'configure.ac'}, -- AutoTools 
                discovery.patterns.GIT
            }
            return discovery.root_pattern(fname, root_structure_any_of) 
        end 
    }, nil, epoch)
end

function cpp.init()
    local lsp = require("cfg.lsp")
    local epoch = reload.epoch:get(cpp)
    clangd(lsp, epoch)
end



return cpp
