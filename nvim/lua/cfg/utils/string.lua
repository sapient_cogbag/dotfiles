local curr_mod_string = {}

---Strip whitespace (including newlines) from the start and end of the given string
---@param str string
---@return string
function curr_mod_string.strip(str)
    --[[
    str = string.gsub(str, "^%s+", '')
    str = string.gsub(str, "%s+$", '')
    --]]
    return vim.trim(str)
end

return curr_mod_string
