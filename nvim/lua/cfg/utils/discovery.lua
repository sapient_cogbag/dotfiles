-- For discovering paths :) nya
local discovery = {}
discovery.patterns = require("cfg.utils.discovery.patterns")
discovery.path = require("cfg.utils.discovery.path")

-- Provides an easy solution to the problem of getting the full filepath for
-- a buffer, but only if it has some filename already nya.
--
-- If the last arg is nil, this returns nil, else this returns the full path of
-- the buffer at the given bufnr.
function discovery.maybe_resolve(bufnr, fname)
    if fname ~= nil then
        return vim.api.nvim_buf_get_name(bufnr)
    end
end

--- Convert either a single string, a single list of required files or directories (which are
--- encoded as a spec string (not processed by this), or a full list of alternative requirements,
--- into a full list of alternate requirements nya ^.^
--- Errors out on invalid specs
--- @param raw_patterns string|string[]|string[][]
--- @return string[][] normed_raw_patterns
local function normalize_pattern_structure(raw_patterns)
    -- Normalization of pattern specification nya.
    -- We need to turn it into a list of lists of strings.
    if type(raw_patterns) == "string" then
        -- The pattern is a single file that needs to be in the directory ^.^
        return { { raw_patterns } }
    elseif type(raw_patterns) == "table" then
        if type(raw_patterns[1]) == "string" then
            -- In this case, we are an argument that contains only one list of required files.
            -- so we need to wrap it up.
            return { raw_patterns }
        elseif type(raw_patterns[1]) == "table" then
            return raw_patterns
        else
            error("Pattern specification invalid")
        end
    else
        error("Pattern specification invalid")
    end
end

--- Convert a single string into a requirement structure. This is one component of a single requirement pattern
--- @alias cfg.utils.discovery.ItemRequirement {postfix: string, allow_file: bool, allow_directory: bool}
--- @param unparsed_item_requirement string single requirement inside all of the patterns
--- @return cfg.utils.discovery.ItemRequirement parsed_requirement
local function parse_item_requirement(unparsed_item_requirement)
    --- This is painful because of lack of zero-based indices :( nya

    -- First - check for the directory OR file matcher.
    local last_two_or_less = unparsed_item_requirement:sub(-2)
    local last_one_or_less = unparsed_item_requirement:sub(-1)
    if last_two_or_less == "/?" then
        -- Note that because of 1-based indexing, :sub(1, -2) is not just the
        -- leftover parts of :sub(-2). In fact, :sub(1, -3) is the leftover parts.
        return {
            postfix = unparsed_item_requirement:sub(1, -2 - 1),
            allow_file = true,
            allow_directory = true
        }
    elseif last_one_or_less == "/" then
        -- Directories only
        return {
            postfix = unparsed_item_requirement:sub(1, -1 - 1),
            allow_file = false,
            allow_directory = true
        }
    else
        return {
            postfix = unparsed_item_requirement,
            allow_file= true,
            allow_directory = false
        }
    end
end

--- Returns true if the given single specification is matched in the given parent directory nya
--- Returns false otherwise.
---@param parent_dir string
---@param directory_item_specification cfg.utils.discovery.ItemRequirement
---@return bool did_match
local function parent_directory_matches_sync(parent_dir, directory_item_specification)
    local normed_path_to_item = vim.fs.normalize(parent_dir .. "/" .. directory_item_specification.postfix)
    -- Early termination. This function is basically a fancy or statement ^.^
    if directory_item_specification.allow_file and discovery.path.is_file_sync(normed_path_to_item) then
        return true
    end
    if directory_item_specification.allow_directory and discovery.path.is_directory_sync(normed_path_to_item) then
        return true
    end
    return false
end

--- Returns true if the given list of specifications are all matched by the parent directory
---@param parent_dir string
---@param directory_item_specification_set cfg.utils.discovery.ItemRequirement[]
---@return bool matches_all
local function parent_directory_matches_all_sync(parent_dir, directory_item_specification_set)
    for _, specification in pairs(directory_item_specification_set) do
        if not parent_directory_matches_sync(parent_dir, specification) then
            return false
        end
    end
    return true
end

--- Attempt to discover a path up the tree that contains a certain file structure
--- TODO: make aync.
---
--- @param filepath string the path to look for a root pattern from. Note that this
---   MUST be a full filesystem path if you want to find roots reliably.
---   Use a function like vim.api.nvim_buf_get_name() nya
---
---   See: discovery.maybe_resolve()
--- @param patterns string|string[]|string[][]
---   This specifies what files and directories to look for to discover a root directory.
---
---   Each string involved represents an entry to look for when checking a directory. If
---   it doesn't end with anything specific, then it will only match files. If it ends with
---   "/", then it only matches directories. If it ends with "/?", then it can be either a
---   file or directory.
---
---   In the list of strings case, it requires every file in
---   the list to be present in a parent for that directory to be discovered as a root
--
---   In the list of list of strings case, each element of the outer list is a file structure
---   to look for, and if any of those file structures match completely a parent directory, that is
---   the root.
--
---   It can also be a single string - in which case it just tries to find that filepath.
---
---   These patterns are in priority order.
---   This means that as this function scans downward, even if there is a later-specified
---   requirement collection that is found first, if the first one is found further up the
---   list of parents that is used instead.
---
---   Note that this function doesn't use the "niave" trick of scanning up the tree multiple
---   times (which could slow things down significantly in the case of certain filesystem
---   boundaries). Instead, it keeps track of the resultant values for each pattern and
---   once it's reached the end it extracts the path from the highest priority pattern that
---   actually has one (if any - if none were found it returns nil.
--
--- @return string? maybe_found_root_directory Path to a root parent directory matching
---   the specified patterns, or nil
function discovery.root_pattern(filepath, patterns)
    local normalized_raw_patterns = normalize_pattern_structure(patterns)
    local normalized_patterns_and_paths = vim.tbl_map(function(single_requirement_set)
        return {
            requirement_set = vim.tbl_map(parse_item_requirement, single_requirement_set),
            found_path = nil
        }
    end, normalized_raw_patterns)

    for parent_dir in vim.fs.parents(filepath) do
        for pattern_id = 1, #normalized_patterns_and_paths, 1 do
            local path_not_found = (normalized_patterns_and_paths[pattern_id].found_path == nil)
            local requirement_set = normalized_patterns_and_paths[pattern_id].requirement_set
            -- "and" ensures laziness in case the path has already been matched
            -- and we avoid a bunch of nested ifs.
            local parent_directory_matches = path_not_found and
                parent_directory_matches_all_sync(parent_dir, requirement_set)
            if parent_directory_matches and pattern_id == 1 then
                -- Early return in case we are the highest priority
                return parent_dir
            elseif parent_directory_matches then
                normalized_patterns_and_paths[pattern_id].found_path = parent_dir
            end
        end
    end
    -- Locate the highest priority path, if any nyaaa ^.^
    local highest_priority_found_path
    for _, pattern_and_maybe_path in ipairs(normalized_patterns_and_paths) do
        if pattern_and_maybe_path.found_path ~= nil then
            highest_priority_found_path = pattern_and_maybe_path.found_path
            break
        end
    end
    return highest_priority_found_path
end

--- This is a version of discovery.root_pattern() designed for vim.fs.find-style
--- directories.
---
--- @param path string - the path to look in
--- @param names string|string[]|fun(basename: string, full_path: string): bool - either a single string name, table of base names, or
---     function criteria.
---     Like the parameter in `vim.fs.find`, this represents a list of names for which matching any of them
---     causes success nya. In particular, the path is to the item, NOT to the parent of the item.
--- @param find_parent bool - if true, return the path containing one of the names that was found, if it was indeed found nya
---     If false, return a path including the name that matched as an extension. That is, this controls
---     whether we return the actual item we found, or the *parent directory* containing the item we searched for.
--- @param type_restriction "file"|"directory" - like the `type` parameter in `vim.fs.find`, (optionally) allows
---     restricting the type of the located name to be a directory or a file or similar nya
--- @return string? # path matching the criteria
function discovery.root_locate(names, path, find_parent, type_restriction)
    -- Can do multi-find stuff but we don't do that here nya (default limit is 1, also)
    local bare_result = vim.fs.find(names, { path = path, upward = true, type = type_restriction })[1]
    if find_parent and bare_result then
        bare_result = vim.fs.dirname(bare_result)
    end
    return bare_result
end

function discovery.init()
    discovery.patterns.init()
end

return discovery
