-- FZF lua uses the method of splitting output into multiple, single-line :echom/e/hi/etc. commands to
-- avoid creating the messaging prompt when dealing with output.
--
-- This provides similar primitives to that for writing messages, that are then required in the log file nya
--
-- It is a simple module. No .init() or even importing other modules, so it can be used from log without problems

local output = {}

-- Write a multiline message line by line to avoid creating issues of prompting the
-- user nya.
-- Can provide a line prefix and a message, and optionally a hlgroup, and optionally 
-- a second hlgroup that acts on the message only (with the first being used for the
-- lineprefix nya) 
function output.echomsg_multiline(lineprefix, msg, hlgroup, msghlgroup)
    for _, line in ipairs(vim.fn.split(msg, "\n")) do
        vim.api.nvim_echo({{lineprefix, hlgroup}, {msg, msghlgroup or hlgroup}}, true, {})
    end 
end

-- Like output.echomsg_multiline, but for errors 
function output.echoerr_multiline(lineprefix, msg)
    for _, line in ipairs(vim.fn.split(msg, "\n")) do
        vim.api.nvim_err_writeln(lineprefix .. msg)
    end
end

return output
