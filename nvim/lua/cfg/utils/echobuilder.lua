--  Module for the purposes of building messages to be output to vim.api.nvim_echo in one go nya
--
--  Use echobuilder:new() to construct a new builder
local echobuilder = {}

-- Construct a new echobuilder.
--
-- Used to construct messages to be echo'd by vim.api.nvim_echo - either as a multiline thing
--  or line by line - or perhaps to construct raw strings in future, this is designed to construct
--  structured, formatted strings for a terminal nya, with well-managed indenting and such.
--
-- Indents should not have newlines in them.
--
-- You can provide options for this.
-- @param opts: table of the form
--  {
--      wrapcolumns = vim.o.co - total columns to wrap with.
--  }
function echobuilder:new(opts)
    local opts = opts or {}
    local obj = {}

    obj.indent_stack = {}
    obj.indent_string_cache = ""

    -- Number of columns available nya
    obj.linewrap = opts.wrapcolumns or vim.o.co

    -- List of pairlists, line by line.
    obj.echolines = {}
    -- Pairlist in one big go
    obj.echobulk = {}
    setmetatable(obj, {
        -- Disallow new indexes
        __newindex = function(inheritingself, key, value)
            error("Can't assign to unmanaged field")
        end,
        -- Provides a fallback table to retrieve methods from ourselves nya
        __index = self
    })
    return obj
end

-- Internal function to refresh the internal indent string cached. Used automatically
-- on changes to the indent string
function echobuilder:refresh_indent_cache()
    if #self.indent_stack == 0 then
        self.indent_string_cache = ""
    else
        self.indent_string_cache = table.concat(self.indent_stack)
    end
end

-- Start a new indent from within the current one, with the given string to indent with
--
-- It can also be nil, which is useful in cases of modifying and restoring the
-- indent stuff.
--
-- if the indent string is actually a table, then that is extended onto the stack nya
function echobuilder:indent(indent_string)
    if type(indent_string) == "table" then
        vim.list_extend(self.indent_stack, indent_string)
    else
        table.insert(self.indent_stack, indent_string)
    end
    self:refresh_indent_cache()
end

-- Terminate the current indent region nya, i.e. pop the deepest indent string
--
-- Returns the top indent dtring existing if any, which can
-- be used to build complex nested lists nya.
--
-- Optionally can pass the # of levels - default 1 - to unindent nya.
--
-- Returns the most outer level old marker.
function echobuilder:unindent(levels)
    if levels == nil then levels = 1 end
    local top_indent_string
    -- For loops are inclusive
    for i = 1, levels do
        top_indent_string = table.remove(self.indent_stack)
    end
    self:refresh_indent_cache()
    return top_indent_string
end

-- Reindent the current indent string with one of the same length
-- made of spaces, returning the old deepest indent if any
--
-- Can be used to construct complex nested lists by storing this value and restoring it.
--
-- In the case that the current indent is nil/toplevel, this works fine and puts nil on it :)
function echobuilder:reindent_spaces()
    local old_top_indent_string = self:unindent()
    local length
    if old_top_indent_string ~= nil then
        length = #old_top_indent_string
        local new_top = string.rep(" ", length)
        self:indent(new_top)
    end
    return old_top_indent_string
end

-- Reindent the deepest part with a new indent, returning the existing indent.
--
-- If the deepest indent is nil then this will not do anything, intentionally.
function echobuilder:reindent(new_string)
    local old_top_indent_string = self:unindent()
    if old_top_indent_string ~= nil then
        self:indent(new_string)
    end
    return old_top_indent_string
end

-- Combines self:reindent_spaces with an inner list marker to create a nested list.
--
-- Returns the original list marker that must be passed to end_inner_list to restore the
-- old inner list state.
function echobuilder:start_inner_list(list_marker)
    local old_list_marker = self:reindent_spaces()
    self:indent(list_marker)
    return old_list_marker
end

-- Finish an inner list by popping off the inner marker and the associated spaces,
-- and readding the inner list marker.
function echobuilder:end_inner_list(old_list_marker)
    self:unindent(2)
    self:indent(old_list_marker)
end

-- Runs the inner function to generate an inner list (of arbitrary depth), with the given marker
--
-- @param marker - Marker to use for the inner list nya
-- @param callback - function where the first argument is an echobuilder
-- @params ... - All other arguments to the callback
-- @returns the result of the callback
function echobuilder:with_inner_list(marker, callback, ...)
    local outer_list_marker = self:start_inner_list(marker)
    local cb_result = callback(self, ...)
    self:end_inner_list(outer_list_marker)
    return cb_result
end

-- Builds an extended list element without adding extra markers for each line.
--
-- Acts like `echobuilder:with_inner_list` in terms of parameters, except this is
-- designed just to extend the current indented message without adding markers.
--
-- The callback must take the echobuilder as a first parameter ^.^
function echobuilder:multiline_extra(callback, ...)
    local list_marker = self:reindent_spaces()
    local cb_result = callback(self, ...)
    self:reindent(list_marker)
    return cb_result
end

-- Normalise a message parameter into the standard neovim form of {{message, hlgroup}}.
--
-- Parameters:
-- * line_or_lines - either a string, or a table of strings, or a table of table of {string, highlight} pairs (empty = last)
--   a-la neovim echo cmds nya. Can be mixed {string} and {{string. hl}}
-- * default_highlight_group - If specified, a highlight group to use by-default or otherwise for the whole message.
--
-- Returns:
-- * The normalised arguments. Note that this does not handle wrapping or similar such things nya.
--   This also fills in the default default_highlight_group.
local function normalise_chunks(line_or_lines, default_highlight_group)
    if type(line_or_lines) == "string" then
        line_or_lines = { line_or_lines }
    end

    if type(line_or_lines) == "table" then
        local new_tbl = {}
        for _idx, msg in ipairs(line_or_lines) do
            if type(msg) == "string" then
                table.insert(new_tbl, { msg })
            else
                table.insert(new_tbl, msg)
            end
        end
        line_or_lines = new_tbl
    end

    -- By this point we have a standard list of tables nya
    for _idx, msgtable in ipairs(line_or_lines) do
        if msgtable[2] == nil then
            msgtable[2] = default_highlight_group
        end
    end

    return line_or_lines
end

-- Perform wrapping on the normalised chunks, in the given number of columns and with the given indent length
-- (or indent string from which to calculate it, nil => 0).
--
-- `\n` are included in returned list of pairlist nya.
--
-- This returns a list of pairlists, where each list is exactly one line.
local function wrap_normalised_chunks(columns, indent_length_or_string, normalised_chunks)
    local indent_level = 0
    if type(indent_length_or_string) == "string" then
        indent_level = string.len(indent_length_or_string)
    elseif type(indent_length_or_string) == "number" then
        indent_level = indent_length_or_string
    end

    local wrapwidth = columns - indent_level
    if wrapwidth < 10 then
        error(("Space for wrapping text (%s chars) is too small!"):format(wrapwidth))
    end

    local current_text_width = 0
    local last_was_not_line = true

    local linewise_pairlists = {}
    local new_chunks = {}

    -- Called to reset the new chunks and add the pairlist
    -- as a new line nya
    local function was_new_line()
        current_text_width = 0
        last_was_not_line = false
        table.insert(linewise_pairlists, new_chunks)
        new_chunks = {}
    end

    for _idx, existing_pair in ipairs(normalised_chunks) do
        local highlight_group = existing_pair[2]

        -- We do want to keep empty items, they're probably useful spacing nya
        -- `\zs` means to keep the newlines in place - some input chunks may not end
        -- with a line (e.g. if there are two highlighting sections on the same line),
        -- so we keep needed newlines inside the string nya
        --
        -- Also all this is working on one existing pair so we don't mess around with the
        -- highlighting group until the end nya
        local inner_maybe_lines = vim.fn.split(existing_pair[1], '\n\\zs', true)
        local curr_idx = 1


        -- Use inner_maybe_lines like a kind of inverse stack nya
        while inner_maybe_lines[curr_idx] ~= nil do
            local curr_string = inner_maybe_lines[curr_idx]
            local count_last = string.sub(curr_string, -1) ~= "\n"
            local ends_with_new_line = not count_last
            local length = string.len(curr_string)
            local remaining_space_on_line = wrapwidth - current_text_width
            if (count_last and length > remaining_space_on_line) or
                (not count_last and (length - 1) > remaining_space_on_line) then
                -- Not enough space, trim!
                -- (1-based indexing makes me sad, but just look at string.sub docs nya)
                --
                -- We also need to glue on \n, but we can avoid doing a second string realloc by putting them as separate segments.
                local to_glue_onto_this_line = string.sub(curr_string, 1, remaining_space_on_line)
                -- Remaining stuff to still wrap nya
                inner_maybe_lines[curr_idx] = string.sub(curr_string, -(length - remaining_space_on_line))
                -- Add to the new chunks as a hl group with \n
                table.insert(new_chunks, { to_glue_onto_this_line, highlight_group })
                table.insert(new_chunks, { "\n", highlight_group })
                was_new_line()
            else
                -- Add to the list
                table.insert(new_chunks, { curr_string, highlight_group })
                -- enough space, add and move on
                --
                -- We do have to reset the current width counter if the end is newline though
                if ends_with_new_line then
                    was_new_line()
                else
                    last_was_not_line = true
                    current_text_width = current_text_width + length
                end
                -- Increment the index nya
                curr_idx = curr_idx + 1
            end
        end
    end

    -- Force the last chunk onto the list nya
    if last_was_not_line then
        -- Force it
        was_new_line()
    end

    return linewise_pairlists
end

--- Produce a message of any number of lines that "just works" within lists, via autowrapping, and automanaging list prefixes nya
-- If this is not it's own line, then a newline is added.
--
-- See `normalise_chunks` for information on what form these arguments can take nya.
--
-- Second argument is a boolean, If it's true, then this is marked as a continuation of a previous
-- element, which allows interleaving inner lists with intermediary messages without adding more
function echobuilder:msg(message, continuation, default_highlight_group)
    local normalised = normalise_chunks(message, default_highlight_group)

    -- Take advantage of the fact that reindent_spaces results in a prefix of the same len nya
    local lines = wrap_normalised_chunks(self.linewrap, self.indent_string_cache, normalised)

    -- If there is no newline on the last line then we add one
    if #lines == 0 then
        lines[1] = { { "\n", default_highlight_group } }
    end

    local last_line = lines[#lines]
    if #last_line < 1 then
        last_line[1] = { "\n", default_highlight_group }
    end

    -- Check the last element - if it's empty or doesn't end in newline, we glue one other segment on the end nya
    -- (no string allocation like direct concat nya)
    if (string.len(last_line[#last_line][1]) == 0) or (string.sub(last_line[#last_line][1], -1) ~= "\n") then
        table.insert(last_line, { "\n", default_highlight_group })
    end

    local did_space = false
    local if_space_then_prev
    for idx, linepairs in ipairs(lines) do
        -- More than one line
        if (not continuation and idx == 2) or (continuation and idx == 1) then
            did_space = true
            if_space_then_prev = self:reindent_spaces()
        end

        table.insert(linepairs, 1, { self.indent_string_cache })
        -- Add each inner list of pairs directly to the bulk echo list nya
        vim.list_extend(self.echobulk, linepairs)
    end
    if did_space then
        self:reindent(if_space_then_prev)
    end
    -- Add all in one go to our big line list nya
    vim.list_extend(self.echolines, lines)
end

-- Echo each line as an individual message.
--
-- Note that this will not reset anything internally so you can do it again and again and a g a i n ^.^
-- For line by line, the default is to add to history nya
function echobuilder:echo_line_by_line(add_to_history)
    if add_to_history == nil then add_to_history = true end
    for _idx, linepairs in ipairs(self.echolines) do
        vim.api.nvim_echo(linepairs, add_to_history, {})
    end
end

function echobuilder:echo_bulk(add_to_history)
    if add_to_history == nil then add_to_history = false end
    vim.api.nvim_echo(self.echobulk, add_to_history, {})
end

return echobuilder
