-- Utility functions for constructing sets
--
-- Sets are tables mapping values to something truthy.
--
-- They can also act as boolean functions returning whether or not they
-- contain their argument, and iterators over their contained items.
--
-- # DEFINED OPERATIONS
--
-- ## Iteration:
-- Sets implement the *iterator* protocol. Their metatables have an __iter method that
-- iterates over all valid elements of the set nya
--
-- ## Addition and Subtraction:
-- Sets implement these operations as union (+) and set-difference (-) nya.
local set = {}

---@class Set
local set_metatable = {}

-- Mechanism of iterating over a set - tho it uses a reduced version of the iterator protocol.
function set.iter(set_to_iterate)
    set.assert_all_are_sets(set_to_iterate)
    return getmetatable(set_to_iterate).__iter(set_to_iterate)
end

-- Check if all the arguments are valid set objects (i.e. they have
-- the correct metatable).
function set.all_are_sets(...)
    local all_are = true
    for _idx, value in pairs({ ... }) do
        all_are = all_are and (getmetatable(value) == set_metatable)
    end
    return all_are
end

function set.assert_all_are_sets(...)
    assert(set.all_are_sets(...), "Set operation not permitted on non-sets")
end

-- Modify the first set to be the union of all sets passed as args
-- Returns the first set (for easy chaining nya)
---@param first Set
---@param ... Set
---@return any
function set.inplace_union(first, ...)
    set.assert_all_are_sets(first, ...)
    for _idx, curr_set in ipairs({ ... }) do
        for element in set.iter(curr_set) do
            first[element] = true
        end
    end
    return first
end

-- Return a new set that is the union of it's arguments
function set.union(...)
    local new_set = set.new()
    return set.inplace_union(new_set, ...)
end

-- Inplace set-difference. Modifies the first set (and returns it) to remove
-- all the elements of the rest of the sets.
function set.inplace_set_difference(first, ...)
    set.assert_all_are_sets(first, ...)
    for _idx, curr_set in ipairs({...}) do
        for element in set.iter(curr_set) do
            first[element] = nil
        end 
    end
    return first
end


-- Copy a set into another, new set. 
function set.clone(curr_set)
    set.assert_all_are_sets(curr_set)
    return setmetatable(vim.deepcopy(curr_set), set_metatable)
end

-- Return a new set that is the set difference between the first and the union of the rest nya
function set.set_difference(first, ...)
    -- Note in this case we have to make a copy of the first because 
    -- we subtract from it rather than add to it (like in union, where we can add 
    -- all the keys from the first nya) 
    local new_set = set.clone(first)
    return set.inplace_set_difference(new_set, ...)
end


-- Note that in the case the thing on the left has no __add and is not a number,
-- the set may be solely on the right.
--
-- Therefore we check LHS and RHS to have metatable of the set nya
--
-- This returns a new set. If you don't need the cost of duplicating all the keys in the 
-- lhs set, use `set.inplace_union`
function set_metatable.__add(lhs, rhs)
    -- Checks are done in the deep
    return set.union(lhs, rhs)
end

-- Set difference. 
--
-- If you don't want the cost of a new set with duplicate keys of `lhs`, 
-- use `set.inplace_set_difference` 
function set_metatable.__sub(lhs, rhs) 
    return set.set_difference(lhs, rhs) 
end

function set_metatable:__call(k)
    if self[k] then
        return true
    else
        return false
    end
end

-- Iterate
function set_metatable:__iter()
    local pair_function, s, k, v = pairs(self)
    -- make it first non-false or nil one
    while k ~= nil and not v do
        k, v = pair_function(s, k)
    end

    -- acts to wrap the existing pair function and eat it's output
    -- until it's either nil (iter ended) or truthy nya
    local function iter_function(s, k)
        -- Simple next() style
        local k, v = pair_function(s, k)
        -- Not end, and not actually contained
        while k ~= nil and not v do
            k, v = pair_function(s, k)
        end
        return k
    end

    return iter_function, s, k
end

---@vararg any
---@return Set
function set.new(...)
    local s = {}
    for _idx, p in ipairs({ ... }) do
        s[p] = true
    end
    setmetatable(s, set_metatable)
    return s
end

return set
