local buffer = {}

-- If the buffer is safe to programmatically edit. Buf id is default to current
-- buffer with nil
--
--
--
-- -- NOTE THAT RIGHT NOW THIS FUNCTION IS BROKEN! (it might be the is_loaded thing)
-- Based off of:
-- " Taken from /usr/share/vim/vimfiles/plugin/fzf.vim
-- " and is a function that returns whether or not the current
-- " buffer is unchanged, and empty and other such nyaa =>
-- " getline() is 1-based eurgh
-- function! s:is_buffer_safe_for_edit()
--     " nyaaa
--     return empty(expand('%')) && line('$') == 1 && empty(getline(1)) && !&modified
-- endfunction
function buffer.is_safe_for_edit(bufid)
    -- Important or the other values may just be useless
    local is_loaded = vim.api.nvim_buf_is_loaded(bufid or 0)
    if not is_loaded then return false end

    local buffer_name = vim.api.nvim_buf_get_name(bufid or 0)
    local line_count = vim.api.nvim_buf_line_count(bufid or 0)
    local is_modified = vim.bo[bufid or 0].modified
    local first_line = ""
    if line_count == 1 then
        -- Args are zero-based exclusive, but results are lua array (1-based)
        first_line = vim.api.nvim_buf_get_lines(bufid or 0, 0, 1, false)[1]
    end

    return (string.len(buffer_name) == 0 and line_count < 2 and string.len(first_line) == 0 and not is_modified)
end

function buffer.init()
end

return buffer
