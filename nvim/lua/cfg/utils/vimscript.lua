--- Utilities for more lua-ish APIs to work with vimscript
local vimscript = {}

--- Little utility function from https://vonheikemen.github.io/devlog/tools/setup-nvim-lspconfig-plus-nvim-cmp/#change-diagnostic-icons
--- to create or modify vim signs. Neovim diagnostic has some signs we can override 
--- using this nya
---@param opts cfg.utils.vimscript.SignOpts
---
---@class cfg.utils.vimscript.SignOpts
---@field name string name of the sign to override or define nya
---@field texthl string? name of the text highlight group it should use. If unspecified, set to name
---@field text string? text to display. If unspecified, this will use the original on override
---@field linehl string? highlight group for the whole line the sign is on, if any 
---@field numhl string? highlight group to put on the line number upon which the sign is placed, if any nya
---@field currlinehl string? highlight group to put on the line of the sign when the cursor is also on it. Only available when `cursorline` is enabled nya
---@field icon string? full path to the bitmap file for the sign, if any.
function vimscript.sign(opts)
    local name = opts.name
    local texthl = opts.texthl or opts.name
    vim.fn.sign_define(name, {
        texthl = texthl,
        text = opts.text,
        linehl = opts.linehl,
        numhl = opts.numhl,
        culhl = opts.currlinehl,
        icon = opts.icon
    })
end

function vimscript.init() end

return vimscript
