--- Utilities for working with paths. Has strong influences from
--- https://github.com/neovim/nvim-lspconfig/blob/master/lua/lspconfig/util.lua,
--- though async implementation is eventually intended ^.^
---
--- Provides e.g. better versions of existence checks and such, as well as async things
local path = {}

local async = require("cfg.async")
local uv = vim.loop
local libuv_async = require("cfg.libuv_async")

--- @param checkpath string
--- @return bool|uv.aliases.fs_stat_types type_if_exists false if the file does not exist,
--- a string describing it's type if it does.
function path.exists_sync(checkpath)
    local stat = uv.fs_stat(checkpath)
    return stat and stat.type or false
end

--- @param checkpath string
--- @return Future<bool|uv.aliases.fs_stat_types> type_if_exists false if the file does not exist,
--- a string describing it's type if it does.
path.exists = async.fn.from_async_body(function(checkpath)
    local stat = libuv_async.fs.stat(checkpath):await().result
    return stat and stat.type or false
end)

function path.is_file_sync(checkpath)
    return path.exists_sync(checkpath) == "file"
end

function path.is_directory_sync(checkpath)
    return path.exists_sync(checkpath) == "directory"
end

path.is_file = async.fn.o_wrap_async_fn(path.exists, function(v) return v == "file" end)
path.is_directory = async.fn.i_wrap_async_fn(path.exists, function(v) return v == "directory" end)

return path
