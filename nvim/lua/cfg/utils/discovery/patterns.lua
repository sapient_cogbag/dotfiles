--- Contains useful collections of requirements
--- that can be added into the list of alternative requirements taken in
--- by "cfg.utils.discovery.root_pattern"
---
--- For example, if you have a set of patterns like
--- ```lua
--- {
---     {'.clangd'},
---     {'compile_commands.json'}
--- }
--- ```
--- And you wanted to also look for a git directory if you don't find anything else, you'd do
--- something like this
--- ```lua
--- {
---     {'.clangd'},
---     {'compile_commands.json'},
---     require("cfg.utils.discovery.patterns").git
--- }
--- ```
local patterns = {}

patterns.GIT = { ".git/?" }

--- Merge the requirements of *n* patterns.
--- @vararg table
--- @return table
function patterns.merge(...)
    local merged_pattern = {}
    for _idx, pattern in ipairs({ ... }) do
        for _inner_idx, individual_matching_component in ipairs(pattern) do
            merged_pattern[#merged_pattern + 1] = individual_matching_component
        end
    end
    return merged_pattern
end

function patterns.init() end

return patterns
