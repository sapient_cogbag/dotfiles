-- Theming for the code action menu plugin 
local code_action_menu = {}
local log = require("cfg.log").with_prefix("theming").with_prefix("code_action_menu")
local reload = require("reload")

function code_action_menu.init() 
    local plugins = require("cfg.plugins")

    plugins.register_onloaded("nvim-code-action-menu", function (cam_plugin)
        vim.g.code_action_menu_window_border = "rounded" 
    end)
end

return code_action_menu
