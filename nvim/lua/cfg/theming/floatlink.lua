--- Module for linking up floats to look actually nice lol nya
-- Much of the linked hl groups here are taken from how FZFlua does it nya
--
-- Note we do need to write our own colour scheme here - fzf lua defaults don't work
-- on immediate floating window options for popups in lsp hover nya. In particular, the bg
-- being the same as normal is a problem
local floatlink = {}

function floatlink.init()
    -- Note that the "Float" hl group is for the numbers (1.439349e5) rather than
    -- the windows, which is why it's not here nya
    local linkmap = {
        NormalFloat = "Normal",
        FloatBorder = "Normal",
        FloatTitle = "Normal"
    }

    for target, src in pairs(linkmap) do
        vim.api.nvim_set_hl(0, target, { link = src })
    end
end

return floatlink
