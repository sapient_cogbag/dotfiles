-- Theming for lualine - uses plugin callbacks
local lualine = {}
local plugins = require("cfg.plugins")

-- TODO: Create progress component by consistently getting new messages and averaging
-- over clients for new buffers nya
--
-- See vim.lsp.utils.get_progress_messages
lualine.progress_component = {}


-- Understanding active and inactive sections:
-- * Active theming and sections are present for the buffer window that has focus
--   * Furthermore, themes have parameters for each mode (normal, insert,
--     visual, replace, command, and inactive) nya, and within those, for each section (a, b, c).
--     (x, y, z) (the right hand sections) can also be specified but if unspecified, they 
--     act to be symmetric to the a, b, c ones in reverse ^.^
-- * Inactive theming and sections are present for the buffer window without focus - e.g.
--   the inactive sections control what is there for the windows not currently active nya

function lualine.init() 
    plugins.register_callback("lualine", function(lualine) 
        ---@module "lualine" 
        lualine = lualine

        -- Customising the theme
        --
        -- Note that `themes` is not a submodule of lualine, so require is needed :(
        -- local powerline = lualine.themes.powerline_dark
        local powerline = require("lualine.themes.powerline_dark") 
        local colours = {
            white = "#FFFFFF"
        }

        -- Keep everything symmetrical nya
        for _, section in ipairs({"a", "b", "c"}) do
            powerline.inactive[section].fg = colours.white
            powerline.inactive[section].gui = 'none'
        end


        lualine.setup {
            options = { 
                theme = powerline, 
                component_separators = { left = '', right = '' },
                section_separators = { left = '', right = '' },
            },
            sections = {
                lualine_c = {
                    {
                        'filetype',
                    },
                    {
                        'filename',
                        newfile_status = true,
                        path = 1,
                        symbols = {
                            readonly = ''
                        }
                    }
                },
                lualine_x = {
                    {'encoding'}
                }
            },
            tabline = {
                lualine_a = {{
                    "buffers",
                    -- name + nr (mode 2 would be name plus *index* nya)
                    mode = 4
                }},
                -- RHS
                lualine_z = {{
                    "tabs",
                    -- nr + name nya
                    mode = 2
                }}
            }
        }
    end)
end

return lualine
