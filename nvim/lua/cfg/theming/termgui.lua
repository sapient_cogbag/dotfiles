-- Everything necessary to enable the use of full RGB on my terminals ^.^
local termgui = {}

function termgui.init()
    vim.o.termguicolors = true
    -- Note that this was necessary in my vim config but i dont think it is now nya
    --  vim.o.t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
    --  vim.o.t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
end

return termgui
