-- When developing my normal vim config I used system packages to do vim stuff.
--
-- These also get loaded by neovim, unfortunately :/
--
-- This prevents them from doing nasty things and slowing our neovim cfg nya
local clean_plugin_pkgs = {}

-- Vimlsp and all the asyncomplete friends nya
local function vimlsp()
    vim.g.lsp_loaded = true
    vim.g.asyncomplete_lsp_loaded = true
    vim.g.asyncomplete_loaded = true 
    vim.g.lsp_cxx_hl_loaded = true
end 

function clean_plugin_pkgs.init()
    vim.g.powerline_loaded = true
    vimlsp()
    -- We already have our own fzf 
    vim.g.loaded_fzf = true 
end


return clean_plugin_pkgs
