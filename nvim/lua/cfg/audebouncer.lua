-- Module to enable *debouncing* for autocmd events that are very frequent, to avoid
-- heavy work. For instance, with cursors and such.
--
-- This is inspired at least a little bit by the internal mechanisms of `vim-lsp`. That code
-- has an internal library that enables chaining of asynchronous communication, and one of the
-- things it has is related to debouncing of autocmd events for generating hover windows and
-- such.
--
-- This module provides some default debouncer stuff to prevent overwork.
---@alias bool boolean

local audebouncer = {}


local reload = require("reload")
local set = require("cfg.utils.set")
local log = require("cfg.log").with_prefix("audebouncer")


-- This is the core function by which debouncing of all kinds occurs.
--
-- It wraps a callback into an object that has methods to add and remove restrictions
-- on when it can be repeatedly called. In particular, every time it's called, whether
-- or not it should be called next time is set to false, then other events can act to
-- reset whether or not it should be called the next time nya
--
-- When first plugged through this function, it is not allowed to call next time. The
-- returned table can be used directly as a callable in e.g. autocmds or just manually nya
--
--
--- @param callback function Function to conditionally enable.
--- @return cfg.audebouncer.ConditionalCallback - Object that allows for e.g. registering
--  debounce resets, or manually re-enabling it nya
function audebouncer.conditional(callback)
    assert(vim.is_callable(callback), "Not actually callable u.u")
    ---@class cfg.audebouncer.ConditionalCallback
    ---@type cfg.audebouncer.ConditionalCallback
    local conditional = {
        -- The original function to conditionally run with arbitrary arguments.
        __callback = callback,
        -- Whether or not it's allowed to run next time it's called. Only reset to
        -- false after being set to true, once the extra conditions are also fulfilled
        __allowed_next_time = false,
        -- Extra conditions that have to be true for it to run, as checkable callbacks.
        --
        -- __allowed_next_time is only ever reset once these are all true nya
        __extra_conditions = {},
        -- Next index to insert a condition as nya
        __condition_next_id = 1,
        -- Mechanism to clean this thing up
        __cleanup_cbs = {}
    }

    setmetatable(conditional, {
        __call = function(self, ...)
            local extra_conditions = true
            for _idx, cb in pairs(self.__extra_conditions) do
                extra_conditions = extra_conditions and cb()
            end
            if self.__allowed_next_time and extra_conditions then
                self.__allowed_next_time = false
                return self.__callback(...)
            end
        end
    })

    -- Manually enable it to rerun the first time the list of conditions are all true nya
    function conditional:reset()
        self.__allowed_next_time = true
    end

    -- Add a generic condition function. This is called every time this thing tries to be
    -- called. All added conditions must be true before this will run.
    --
    -- Returns an ID of the condition which can be used to remove it nya
    --- @param condition fun():bool
    --- @return number
    function conditional:add_condition(condition)
        local cond_id = self.__condition_next_id
        self.__condition_next_id = self.__condition_next_id + 1
        self.__extra_conditions[cond_id] = condition
        return cond_id
    end

    -- Remove the condition with the given ID from the list of conditions nya
    -- If it's already removed, that's still fine :)
    --
    -- Returns the condition function, if any
    --- @param condition_id number - ID returned by `conditional:add_condition`
    --- @return nil|fun():bool
    function conditional:remove_condition(condition_id)
        local res = self.__extra_conditions[condition_id]
        self.__extra_conditions[condition_id] = nil
        return res
    end

    -- Reset this callback to true with a debounce timer.
    --- @param timer cfg.audebouncer.DebounceTimer
    --- @return self - Returns self for easy chaining nya 
    function conditional:with_debounce_timer(timer)
        local cb_id = timer:new_callback(function()
            self:reset()
        end)
        table.insert(self.__cleanup_cbs, function()
            timer:del_callback(cb_id)
        end)
        return self
    end

    -- Like `conditional:add_condition`, but returns self for easy chaining.
    --
    -- Obviously, in this case, you don't have the condition ID and can't remove it.
    --
    -- However, in most cases, constructing these sorts of callbacks is a one-time thing nya 
    --- @param additional_condition fun():bool
    ---@return self
    function conditional:and_requires(additional_condition)
        self:add_condition(additional_condition)
        return self
    end

    -- autocmds do not like __call-able tables
    --
    -- So we provide this to wrap as a function nya
    ---@return fun(...)
    function conditional:as_bare_function()
        return function (...)
            return self(...)
        end
    end

    -- Call all the cleanup stuff for this - removes
    function conditional:__cleanup()
        for _, cb in pairs(self.__cleanup_cbs) do
            cb()
        end
        self.__cleanup_cbs = {}
    end
    return conditional
end

---@class cfg.audebouncer.DebounceTimer
local debounce_timer = {}

-- Create a new debounce timer to register things for debouncing on.
---@param time number - Delay in ms between when it should re-enable a callback
---@param epoch Epoch - Epoch this is a part of. If not in this epoch, a timer will not
--  be created and started nya
---@return cfg.audebouncer.DebounceTimer
function debounce_timer.new(time, epoch)
    epoch = reload.epoch:get(audebouncer, epoch)

    -- @type table<number, bool>
    -- Variable IDs to reset periodically.
    local bool_variables = {}
    local next_bool_varid = 1

    -- @type table<number, fun()>
    -- Callbacks to call when the timer is triggered nya.
    local registered_callbacks = {}
    local next_callback_id = 1

    local raw_timer
    if epoch:in_epoch() then
        raw_timer = vim.loop.new_timer()

        epoch:cleanup(function()
            raw_timer:stop()
            raw_timer:close()
        end, "[audebouncer - debounce timer - " .. tostring(time) .. "ms]")
    end

    ---@type cfg.audebouncer.DebounceTimer
    local timer = {}

    -- Create a new boolean variable to periodically be set to true. Starts as true nya
    --
    ---@see debounce_timer.reset_variable  to be called when your desired function has been
    ---  triggered and you want to wait for the timer again.
    ---@see debounce_timer.get_variable
    ---
    ---@return number - An identifier for the given boolean variable.
    function timer:new_variable()
        local id = next_bool_varid
        next_bool_varid = next_bool_varid + 1
        bool_variables[id] = true
        return id
    end

    -- Reset the given variable ID to false, if it is an actual variable id
    ---@param variable_id number
    function timer:reset_variable(variable_id)
        if bool_variables[variable_id] ~= nil then
            bool_variables[variable_id] = false
        end
    end

    -- Get the current variable value.
    --
    -- Returns nil for invalid variable values nya
    ---
    ---@param variable_id number
    ---@return bool?
    function timer:get_variable(variable_id)
        return bool_variables[variable_id]
    end

    -- Delete the given variable, returning it's value at deletion (if any nya)
    ---@param variable_id number
    ---@return bool?
    function timer:del_variable(variable_id)
        local v = bool_variables[variable_id]
        bool_variables[variable_id] = nil
        return v
    end

    -- Add the given callback to be periodically called, with an id returned.
    ---@param cb fun()
    ---@return number # New callback ID
    function timer:new_callback(cb)
        local id = next_callback_id
        next_callback_id = next_callback_id + 1
        registered_callbacks[id] = cb
        return id
    end

    -- Delete the given callback with the id, returning it if it's actually a valid id
    ---@param id number callback ID
    ---@return fun()? callback The callback that was registered to this ID, if any nya
    function timer:del_callback(id)
        local cb = registered_callbacks[id]
        registered_callbacks[id] = nil
        return cb
    end

    if epoch:in_epoch() then
        raw_timer:start(0, time, function()
            for variable_id, _v in pairs(bool_variables) do
                bool_variables[variable_id] = true
            end
            -- Schedule this in case the callbacks don't do fast-api stuff nya
            vim.schedule(function()
                for _cb_id, callback in pairs(registered_callbacks) do
                    callback()
                end
            end)
        end)
    end

    return timer
end

local current_epoch = reload.epoch:get(audebouncer)

current_epoch:in_each_epoch(function(epoch)
    epoch:data().hover_window_debouncer = debounce_timer.new(100, epoch)
end)

-- Get the hover debouncer timer for the given epoch id, if specified. If not 
-- specified, then get it for the current one.
---comment
---@param epoch_id Epoch|number?
---@return cfg.audebouncer.DebounceTimer hover_window_debouncer The hover window debouncer for the given epoch,
---if any nya. If not in the current epoch, though, that debouncer will of course do nothing ^.^
function audebouncer.get_hover_window_debouncer(epoch_id)
    local epoch = reload.epoch:get(audebouncer, epoch_id)
    return epoch:data().hover_window_debouncer
end

function audebouncer.init()
end

return audebouncer
