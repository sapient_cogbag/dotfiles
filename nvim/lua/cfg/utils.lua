local utils = {}

utils.string = require("cfg.utils.string")
utils.buffer = require("cfg.utils.buffer")
utils.discovery = require("cfg.utils.discovery")
utils.set = require("cfg.utils.set")
utils.output = require("cfg.utils.output")
utils.echobuilder = require("cfg.utils.echobuilder")
utils.vimscript = require("cfg.utils.vimscript")

-- Like vim.fn.exepath but returns nil if the command doesn't exist rather than empty string
function utils.exepathnil(path)
    local p = vim.fn.exepath(path)
    if string.len(p) ~= 0 then return p else return nil end
end

-- Avoid unintended dependencies on data by forcing into true/false
function utils.bool(truthy)
    if truthy then return true else return false end
end

-- If a value is nil, then return a default, else return the value passed as a second argument
function utils.if_nil(v, default)
    if v == nil then return default else return v end
end

-- Apply the operation to the value (with it as the first argument) if it isn't nil nya
---@generic T,Q,A
---@param v T|nil
---@param op fun(v: T, ...: A): Q
---@param ... A
---@return Q|nil
function utils.nonnil_map(v, op, ...)
    if v ~= nil then
        return op(v, ...)
    end
end

-- Iterate using the iterator protocol.
--
-- If the metatable of an object has __iter, then this will call that and it
-- should return 3 results:
-- * a function that takes a current state and current value, and returns a next value
--   or nil to terminate nya
-- * an initial state
-- * an initial value
--
-- If it is a table but does not have a metatable with __iter, then acts like `pairs`
--
-- Usage looks like for i, j in utils.iter(some iterable)
function utils.iter(iterable)
    local mt = getmetatable(iterable)
    if mt ~= nil and mt.__iter ~= nil then
        return mt.__iter(iterable)
    elseif type(iterable) == "table" then
        return pairs(iterable)
    else
        error("Tried to iterate over non-iterable object (no metatable.__iter or not a table)")
    end
end

-- vim.F.pack_len. Preserves length explicitly for reasons of ensuring nil in args does not
-- result in lossy pack/unpack nya
function utils.pack(...)
    return vim.F.pack_len(...)
end

-- vim.F.unpack_len. Will work like unpack just fine for packs created without vim.F.pack_len/utils.pack
-- But ensures non-lossy argument unpacking when it was used nya.
function utils.unpack(t)
    return vim.F.unpack_len(t)
end

-- Function to enable injecting pre- and post- callback modification into an arbitrary
-- table of functions.
-- @param tbl: table - the table into which injections should be performed.
-- @param specification: table - A table specifying what to inject into the existing table.
--      This specification takes the form:
--      {
--          key = {
--              pre: nil|callable|table<callable>,
--              post: nil|callable|table<callable>,
--              postargs: bool? (default - false)
--          }
--      }.
--
--      Each key corresponds to a key upon which injection into the original table
--      should be performed.
--
--      `pre` is either a single callback, or a list of callbacks. These are callables that
--      allow modification of the original arguments - and other actions - before they are passed
--      to the original function in `tbl` nya. Each callback receives the following arguments:
--        * tbl,
--        * the original arguments packed with `utils.pack`/`vim.F.pack_len`, for modification,
--          as modified by previous `pre` callbacks when more than one is specified
--      It should return the following:
--        * a boolean indicating whether to return early (false/nil = don't, true = do)
--        * the values to return early with if they should, as a packed table (with `utils.pack`, `vim.F.pack_len`).
--          This will be processed by the postcallbacks
--
--      `post` is either a single callback, or a list of callbacks. These are callables that
--      allow modification of the return values, and other actions, after the original callable
--      in the table has been processed. Each callback receives the following arguments:
--         * tbl,
--         * the original return values packed with `utils.pack`/`vim.F.pack_len`, for modification,
--           as modified by previous `post` callbacks when more than one is specified.
--      It should return the following:
--         * a boolean indicating whether to return early.
--         * The values to return early with, as a packed table (with `utils.pack`/`vim.F.pack_len`)
--
--      `postargs`, if specified as true (default is false), causes the `post` callbacks to receive the
--        bundled, modified verions of the original arguments as an argument after the bundled results.
--        This is sometimes necessary but carries a memory usage and performance penalty nya.
-- @param nil_behaviour: table|string? - specifies what should be done when an injection key does not exist
--    The default behaviour is to error, but alternate behaviour can be specified:
--    * "error" => just the default behaviour
--    * "default" => Define the key to be a function that takes any # of args and returns nothing,
--      before wrapping it in the callbacks
--    * Table of defaults => A set of keys mapped to callables that are the default functions to
--      use, but error for any key not in this predefined list of defaults.
--      Note that if you set a metatable on this, it will not be overridden to check for errors,
--      so you can set one to provide "default-like" behaviour for unknown keys if you want that nya
--
-- @returns The modified tbl
function utils.inject(tbl, specification, nil_behaviour)
    local nil_behaviour = utils.if_nil(nil_behaviour, "error")
    -- Uses metatable to get defaults
    local default_table = {}

    -- In this case, nil_behaviour provides defaults available
    -- so __index is not called then.
    if type(nil_behaviour) == "table" then
        -- Deepcopy avoids setting metatable on argument table nya
        default_table = vim.deepcopy(nil_behaviour)
    end

    if type(nil_behaviour) == "table" or nil_behaviour == "error" then
        if getmetatable(default_table) == nil then
            -- Error on unknown index
            setmetatable(default_table, {
                __index = function(_, unspecified_key)
                    error("Cannot inject into nonexistent key " .. unspecified_key)
                end
            })
        end
    elseif nil_behaviour == "default" then
        -- In this case, just generate new functions on-demand nya
        setmetatable(default_table, {
            __index = function(_, unspecified_key)
                return function(...)
                    return nil
                end
            end
        })
    else
        error("Invalid nil behaviour " .. nil_behaviour)
    end

    -- Now time for hooks
    for injection_key, injection_specification in pairs(specification) do
        -- This manages validation with default_table's metatable nya
        local original_method = tbl[injection_key] or default_table[injection_key]
        local postargs = utils.if_nil(injection_specification.postargs, false)

        local prelist = nil
        if injection_specification.pre ~= nil then
            if vim.is_callable(injection_specification.pre) then
                prelist = { injection_specification.pre }
            else
                -- Validate the prelist elements
                for idx, precb in ipairs(injection_specification.pre) do
                    if not vim.is_callable(precb) then
                        error("Precallback #" .. idx .. " for key " .. injection_key .. " is not callable")
                    end
                end

                prelist = injection_specification.pre
            end
        end
        if prelist ~= nil and #prelist == 0 then prelist = nil end

        local postlist = nil
        if injection_specification.post ~= nil then
            if vim.is_callable(injection_specification.post) then
                postlist = { injection_specification.post }
            else
                -- Validate the postlist elements
                for idx, postcb in ipairs(injection_specification.post) do
                    if not vim.is_callable(postcb) then
                        error("Postcallback #" .. idx .. " for key " .. injection_key .. " is not callable")
                    end
                end

                postlist = injection_specification.post
            end
        end
        if postlist ~= nil and #postlist == 0 then postlist = nil end

        -- Now actually inject nya
        if prelist ~= nil or postlist ~= nil then
            tbl[injection_key] = function(...)
                local results
                -- Avoid unnecessary pack/unpack
                if prelist ~= nil then
                    local args = utils.pack(...)
                    for _idx, precb in ipairs(prelist) do
                        local early_return, early_vals = precb(tbl, args)
                        if early_return then
                            results = early_vals
                            break
                        end
                    end
                    if postlist == nil then
                        return original_method(utils.unpack(args))
                    else
                        results = utils.pack(original_method(utils.unpack(args)))
                    end
                else
                    -- If both were nil, this wouldn't be being wrapped,
                    -- so we always set results
                    results = utils.pack(original_method(...))
                end
                if postlist ~= nil then
                    local args = nil
                    if postargs then
                        args = utils.pack(...)
                    end
                    for _idx, postcb in ipairs(postlist) do
                        local early_return, early_vals = postcb(tbl, results, args)
                        if early_return then
                            return utils.unpack(early_vals)
                        end
                    end
                end
                return utils.unpack(results)
            end
        end
    end

    return tbl
end

-- Provide meaningful names to a table of numerical indices.
-- and apply changes to the named map directly onto the table using indices.
--
-- This is designed for use by callbacks that take opaque tables of arguments, like
-- in `utils.inject`.
-- @param indexed_table: table - The table made of indexed values you want to give names to
-- @param named_map: A list of names, or a map of names to numbers, that determines the association
--  between names and indices nya. This essentially functions by applying `vim.tbl_add_reverse_lookup`,
--  so {"queer", alpha = 1} would result in weirdness, but {"queer", "trans", zeta = 3} would result in
--  mapping queer => index 1, trans => index 2, zeta => index 3 nya
--
-- This doesn't affect your ability to access the table by index as well.
function utils.indexmap(indexed_table, named_map)
    vim.tbl_add_reverse_lookup(named_map)
    return setmetatable({}, {
        -- We do not ever set the value in the dummy table, otherwise
        -- __newindex and __index will stop being called.
        __newindex = function(_table, key, value)
            if type(key) == "number" then
                indexed_table[key] = value
            else
                indexed_table[named_map[key]] = value
            end
        end,
        __index = function(_table, key)
            if type(key) == "number" then
                return indexed_table[key]
            else
                return indexed_table[named_map[key]]
            end
        end
    })
end

function utils.init()
    utils.buffer.init()
    utils.discovery.init()
    utils.vimscript.init()
end

return utils
