--- Integration of the async module into raw libuv stuff.
--- This is often more suitable than using the vim API for developing async,
--- because the vim api is designed more for flexible plugin development with
--- more indistinguishability between errors and just "didn't send anything" states.

local libuv_async = {}
local async = require("cfg.async")
libuv_async.async = async
libuv_async.stream = require("cfg.libuv_async.stream")
libuv_async.fs = require("cfg.libuv_async.fs")

return libuv_async
