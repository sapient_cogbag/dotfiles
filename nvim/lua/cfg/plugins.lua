-- USING THIS MODULE
--
-- This module is our local config plugin loader, and provides utilities for configuring plugins
-- asynchronously on their load.
--
--
local plugins = {}
local reload = require("reload")
local log = require("cfg.log").with_prefix("plugins")

local on_unload_hooks = {}
local function on_unload(h)
    table.insert(on_unload_hooks, h)
end

plugins.packer_strap_state = {}

plugins.packer_strap_state.BOOTSTRAP_FAILED = {}
plugins.packer_strap_state.BOOTSTRAP_SUCCESS = {}
plugins.packer_strap_state.NO_BOOTSTRAP = {}



-- Registered global callbacks for plugins.
--
-- Maps from canonical name to functions.
--
-- Cannot be `local` due to lack of captures in packer config functions
plugins.precallbacks = {}

-- Postcallbacks are called after the immediate callbacks are executed for the plugin.
--
-- They receive the plugin module as a first argument, and the accumulated list of callback
-- results as a second argument.
plugins.postcallbacks = {}

-- Map of plugin name -> list of callback functions taking the loaded plugin
--
-- Note that this cannot be `local`
plugins.callbacks = {}

-- If there is an old config, then this will contain it.
plugins.old_config_functions = {}


-- Some plugins have a differently named file in their plugin/<name>.{lua, vim} from their
-- URL or their alias, which is what we have to `require` to ultimately load them.
--
-- As such, this table provides a map from `canonical_plugin_name` to the require param,
-- and if something isn't there then it just uses the canonical name.
--
-- Cannot be local, as it is accessed from inside the compiled packer data.
plugins.loadnames = {}

-- This is a map of plugin names to loaded plugin tables/general results
--
-- It is used to run callbacks after loading.
plugins.loaded = {}

-- Map of config keys to custom handlers and whether or not they are unconditional.
-- string -> {[handler] = function, [unconditional] = bool}
plugins.registration_handlers = {}


-- This resets all the old callback state, loading names, etc. for a new plugin definition
-- in case of reload nya
function plugins:__unload()
    -- this is all legacy bs anyway >.< 
    for _idx, h in ipairs(on_unload_hooks) do
        h()
    end
    on_unload_hooks = {}
    plugins.precallbacks = {}
    plugins.postcallbacks = {}
    plugins.callbacks = {}
    plugins.old_config_functions = {}
    plugins.loadnames = {}
    plugins.loaded = {}
    plugins.registration_handlers = {}
end

-- Like packer's set_handler(), except it works when registering a plugin with `use`,
-- even when the plugin is not "managed" by packer.
--
---@param key string - what custom key in the plugin table to potentially modify
---@param handler fun(plugin_table :table, current_value: string?) Handles the key
---@param unconditional bool? - default false, this indicates whether to call the handler
--      even when the value is unset in the table. If true, then the value will be nil
--
-- Note that new handlers will override existing ones if added nya
local function set_registration_handler(key, handler, unconditional)
    plugins.registration_handlers[key] = { handler = handler, unconditional = unconditional }
end

-- Register a callback to be run on a plugin, either when it is loaded or immediately.
--
-- Unlike with `plugins.register_callback()`, even if this is registered before load, the
-- return value is not going to be passed through to the plugin's postcallback.
--
-- Why? Because sometimes you need to set up an interaction between more than one plugin and make a new callback
-- inside an outer one, by which point theres a good chance it's all already loaded
--
-- Why would you use anything else? In case you need decentralised configuration and centralised
-- finalisation with postcallback on a given plugin, which can be performed by returning values
-- from a callback.
--
-- In certain cases of recursion where a plugin adds a callback to itself within its own callback,
-- this *MAY* get run more than once, depending on the behaviour of `pairs` when adding something
-- while it is already iterating nya.
function plugins.register_onloaded(plugin_name, plugin_callback)
    plugins.register_callback(plugin_name, function(...) plugin_callback(...) return nil end)
    local log = require("cfg.log").with_prefix("plugins:" .. plugin_name)
    if plugins.loaded[plugin_name] then
        log.info("Running deferred callback for plugin %s", plugin_name)
        local did_callback, callback_result = pcall(plugin_callback, plugins.loaded[plugin_name])
        if not did_callback then
            log.warn("Error running callback for package aliased to " .. plugin_name)
            log.warn(callback_result)
        end
    end
end

-- Register a callback for when a plugin loads, with the given alias.
--
-- If the plugin is already loaded, this will NOT be run. For that, see `plugins.register_onloaded()`
--
-- Why would you want to use this? Because some plugins it is useful to have decentralised callbacks
-- and then centralised finalisation with postcallbacks.
--
-- Arguments:
-- * The alias to act on when loaded - should be the canonical name of the plugin,
--   which with the table-specified plugins is the key.
-- * The callback to actually run - takes an argument of `requires("plugin_alias")` - i.e.
--   the loaded plugin table
--
-- Returns:
-- * A value appended to a table of the results of all callbacks for the plugin, that the
--   plugin can process in it's postcallback.
function plugins.register_callback(plugin_name, plugin_callback)
    if plugins.callbacks[plugin_name] == nil then plugins.callbacks[plugin_name] = {} end
    table.insert(plugins.callbacks[plugin_name], plugin_callback)
end

-- Sometimes, there is no way to combine register_callback with register_onloaded to combine
-- two or more plugins. Or at the very least, if two plugins both need to use postcallback
-- initialisation but also need to interact, it is not possible use register_onloaded to reference
-- the table within the other.
--
-- This provides a direct way to retrieve the plugin table if it is loaded, or nil otherwise,
-- for the cases where it is almost impossible to solve the problem (though, really, you could
-- set a local with a register_onloaded, but that's too complex nya)
function plugins.loaded_or_nil(plugin_name)
    return plugins.loaded[plugin_name]
end

-- Github plugin path for packer.
local function gh(plugin_user_and_name)
    return "https://github.com/" .. plugin_user_and_name
end

-- Gitlab plugin path for packer
local function gl(plugin_user_and_name)
    return 'https://gitlab.com/' .. plugin_user_and_name
end

-- Get the name from the packer config table.
--
-- This is a copy of some code in packer.util that is not public API. Because it's
-- not public API, we won't depend on it for anything to do with going to and from packer
-- names nya,
function plugins.canonical_plugin_name(plugin_config_table)
    local path = vim.fn.expand(plugin_config_table[1])
    -- local name_segments = vim.split(path, util.get_separator())
    local name_segments = vim.split(path, "/", { plain = true })
    local segment_idx = #name_segments
    local name = plugin_config_table.as or name_segments[segment_idx]
    while name == '' and segment_idx > 0 do
        name = name_segments[segment_idx]
        segment_idx = segment_idx - 1
    end
    return name --, path
end

-- Cannot *capture* anything, because it will be injected into the packer compiled item.
local function hijacked_config(packer_plugin_name, plugin_information_table)
    -- Load everything from memory or passed table because this function can't have captures
    local plugins = require("cfg.plugins")
    local log = require("cfg.log").with_prefix("plugins").with_prefix(packer_plugin_name or "nil-plugin")

    -- The plugin information table does not carry the name in it for some reason. If you
    -- look in the compiled thing it has the storage path and a url, and then we have
    -- our canonical plugin name function.
    --
    -- We have to trust here that packer names are sufficiently close to our own - we
    -- did copy the function to derive short names from the combination of url and any
    -- `as` aliases almost verbatim barring not having a special dynamic separator, but
    -- still nya
    -- local plugin_name = plugins.canonical_plugin_name(plugin_information_table)
    local plugin_name = packer_plugin_name

    local precallback = plugins.precallbacks[plugin_name] or function(_) end
    local postcallback = plugins.postcallbacks[plugin_name] or function(_) end
    local callbacks = plugins.callbacks[plugin_name] or {}
    local old_config_function = plugins.old_config_functions[plugin_name] or function(_, _) end
    local loadname = plugins.loadnames[plugin_name] or plugin_name

    local loaded_plugin
    if type(loadname) == "function" then
        -- Sometimes, plugins are not loadable at all directly - that is, they have no `init.lua` and
        -- have to be required module-by-module. In that case, the user has to provide a function to construct
        -- a module table - this may be a lazy loader, too, nya.
        log.info("Loading %s with special function", plugin_name)
        local did_load, module = pcall(loadname)
        if not did_load then
            log.warn("Could not load plugin %s with custom loader function. Perhaps consider :PackerSync. The error was:\n%s"
                , plugin_name, module)
            return
        end
        loaded_plugin = module
    else
        -- Sometimes it is impossible to load a plugin - for instance, if it's been added#
        -- to the table but not :PackerSync'd and installed. in that case, we skip configs for
        -- that plugin nya
        local did_load, module = pcall(require, loadname)
        if not did_load then
            log.warn("Could not load plugin %s with require('%s'), skipping. Perhaps consider :PackerSync", plugin_name,
                loadname)
            log.warn(module)
            return
        end
        loaded_plugin = module
    end

    -- This is marked immediately after loading rather than during callbacks, to avoid the risk of not
    -- running a callback.
    --
    -- However, it may run more than once.
    plugins.loaded[plugin_name] = loaded_plugin

    log.debug("Running any other config function originally provided")
    local did_config, config_result = pcall(old_config_function, packer_plugin_name, plugin_information_table)
    if not did_config then
        log.warn("Failed to run original config function for plugin " .. plugin_name)
        log.warn(config_result)
    end

    log.debug("Running configuration precallback for plugin " .. plugin_name)
    local did_precallback, precallback_result = pcall(precallback, loaded_plugin)
    if not did_precallback then
        log.warn("Failed to run precallback for plugin " .. plugin_name)
        log.warn(precallback_result)
    end

    log.debug("Running configuration callbacks for plugin " .. plugin_name)
    local callback_result_table = {}
    for _idx, callback in ipairs(callbacks) do
        local did_callback, callback_result = pcall(callback, loaded_plugin)
        if not did_callback then
            log.warn("Error running callback for package aliased to " .. plugin_name)
            log.warn(callback_result)
        else
            table.insert(callback_result_table, callback_result)
        end
    end

    log.debug("Running configuration postcallback for plugin" .. plugin_name)
    local did_postcallback, postcallback_result = pcall(postcallback, loaded_plugin, callback_result_table)
    if not did_postcallback then
        log.warn("Failed to run postcallback for plugin " .. plugin_name)
        log.warn(postcallback_result)
    end

    log.info("Loaded plugin " .. (plugin_name or "nil-plugin"))
end

-- Make a function that acts like `packer.use` but injects our special config function
-- into it while dumping the original config into the table.
--
-- We actually could - theoretically - do this with a packer.set_handler(config). Except for the
-- problem that it won't work on a key the user didn't specify :( nya. Moreover, this means that
-- `requires` packages are not included! :(
--
-- EDIT:
-- Turns out packer handlers are only called when packer "manages" a plugin. Unless you are doing
-- some sort of packer operation, this doesn't actually happen. What this means is that essential
-- plugin data, on load, does not get put into the various data tables in this module for our own
-- loading and callback system built on top of packer.
--
-- As such, what we need to do is build our own key transformation module to modify configs as they
-- are registered nya. This function hijacks the original packer.use to add our own transforms on top,
-- that can be registered with `set_registration_handler`.
--
-- Note that the returned function will not
local function make_hijacked_use(packer_use)
    return function(use_param)
        if type(use_param) == "string" then
            use_param = { use_param }
        end

        for key_name, handler_info in pairs(plugins.registration_handlers) do
            local unconditional = handler_info.unconditional
            local handler = handler_info.handler
            if unconditional or use_param[key_name] ~= nil then
                handler(use_param, use_param[key_name])
            end
        end
        return packer_use(use_param)
    end
end

-- Initialise the transformations/handlers for use configs nya
local function initialise_config_transforms()
    local canon_plug_name = plugins.canonical_plugin_name

    set_registration_handler("precallback", function(current_plugin, precallback)
        plugins.precallbacks[canon_plug_name(current_plugin)] = precallback
    end)

    set_registration_handler("postcallback", function(current_plugin, postcallback)
        plugins.postcallbacks[canon_plug_name(current_plugin)] = postcallback
    end)

    set_registration_handler("loadname", function(current_plugin, loadname)
        plugins.loadnames[canon_plug_name(current_plugin)] = loadname
    end)

    -- This lets us use our universal config that loads data from information
    -- registered in the above, without needing to store upvalues in the config nya
    set_registration_handler("config", function(current_plugin, maybe_existing_config)
        if maybe_existing_config ~= nil then
            plugins.old_callback_functions[canon_plug_name(current_plugin)] = maybe_existing_config
        end
        current_plugin.config = hijacked_config
    end, true)
end


-- See https://github.com/wbthomason/packer.nvim <- readme has the basic idea.
-- We try and be more tolerant of e.g. missing git
--
-- Returns an entry in plugins.packer_strap_state
local function packer_strap()
    local fn = vim.fn

    local install_path = fn.stdpath('data') .. '/site/pack/packer/start/packer.nvim'
    local packer_git_repo_remote = 'https://github.com/wbthomason/packer.nvim'
    -- No need to bootstrap if already installed!
    if not (fn.empty(fn.glob(install_path)) > 0) then return plugins.packer_strap_state.NO_BOOTSTRAP end

    log.warn("Packer not found. Bootstrapping!")

    local git_executable_path = vim.fn.exepath("git")
    if string.len(git_executable_path) == 0 then
        log.warn("No git executable found! Needed for bootstrapping packer.")
        return plugins.packer_strap_state.BOOTSTRAP_FAILED
    end

    fn.system({ git_executable_path, 'clone', '--depth', '1', packer_git_repo_remote, install_path })
    -- search in the package directory to load the newly-downloaded package. It's installed in pack/*/start/*
    -- so in future it will be loaded on startup
    vim.cmd([[packadd packer.nvim]])
    return plugins.packer_strap_state.BOOTSTRAP_SUCCESS
end

-- Table of the plugins to load along with their aliases and other information.
--
-- This is related to packer's `use` function, except preprocessing occurs.
-- * A config function is automatically added that retrieves this module and others, along with plugins,
--   via "string" - because the config function provided by packer does not let you have upvalues (captures),
--   - and calls the registered callbacks on it.
-- * The key is used as an "as" parameter for packer.
--
-- This is modified to add a config function that loads the plugin module via a string
-- (since the config function can't have captures due to being packer-compiled), which can then
-- extract callbacks.
--
-- This also has a pre-all-other-callbacks for config stuff. Some plugins have configuration that does
-- not neatly go into one module and yet is universal, and should be applied before other things.
--
-- Note that this function creates the table on each init, to avoid issues of mutation
function plugins.plugins()
    return {
        lualine = {
            gh('nvim-lualine/lualine.nvim'),
            -- This was causing weird issues
            requires = { gh('kyazdani42/nvim-web-devicons') }
        },
        -- Underscores make packer very unhappy :(
        ["fzf-lua"] = {
            gh('ibhagwan/fzf-lua'),
            -- optional for icon support
            requires = { gh('nvim-tree/nvim-web-devicons') },
            precallback = function(fzf_lua)
                fzf_lua.setup {
                    winopts = {
                        -- For the hover window not the files, so i dont want
                        -- split = "belowright vnew"
                    },
                    --[[ Decided against this for now nya, instead we:
                    -- * config in keybinds
                    actions = {
                        files = {
                            ["default"] = fzf_lua.actions.file_vsplit
                        }
                    },]]
                }
            end
        },
        neodev = {
            gh("folke/neodev.nvim"),
            precallback = function(neodev)
                -- This forcefully initializes the config of neodev properly
                -- but only the raw config and not LSP stuff, which we do manually 
                -- later, because the root dir stuff inside neodev isnt working so 
                -- well with our setup.
                require("neodev.config").setup({
                    setup_jsonls = false,
                    lspconfig = false 
                })

                -- 
                -- neodev.setup {
                --    lspconfig = false,
                --    setup_jsonls = false,
                -- }
            end
        },
        luasnip = {
            gh("L3MON4D3/LuaSnip"),
            -- Bind to a version
            -- This causes excessive redownloads of required packages i think nya
            -- tag = "v1.*"
        },
        ['cmp-nvim-lsp'] = {
            -- Use our own fork that stops this from destroying all the capabilities >.< 
            -- gh("hrsh7th/cmp-nvim-lsp"),
            gh("sapient-cogbag/cmp-nvim-lsp"),
            loadname = "cmp_nvim_lsp",
        },
        -- nvim-cmp *DOES* seem to have the ability to do setup lazily, which is good for
        -- interaction stuff. However, merging together seems like The Way
        ["nvim-cmp"] = {
            gh("hrsh7th/nvim-cmp"),
            loadname = "cmp",
            -- We use this one to setup after all existing callbacks by deepmerging everything
            postcallback = function(nvim_cmp, initial_setup_merges)
                local nil_free = {}
                -- ipairs can stop on ANY nil
                for _, maybe_table in pairs(initial_setup_merges) do
                    if maybe_table ~= nil then
                        table.insert(nil_free, maybe_table)
                    end
                end
                -- Error on fail - important to find conflicting config early nya
                -- the {} {} is necessary incase of no- or 1- result cause the
                -- deep extend function needs at least 2 args nya
                local setup_table = vim.tbl_deep_extend("error", {}, {}, unpack(nil_free))
                nvim_cmp.setup(setup_table)

                -- Force setup from the sources
                -- This is after .setup() :) nya
                --
                -- This is needed for reloads - without it the after/ plugin only ever
                -- gets called once nya
                plugins.register_onloaded("cmp-nvim-lsp", function(lspsource)
                    lspsource.setup()
                end)
            end
        },
        hop = { 
            -- gh("phaazon/hop.nvim") - archived/defunct
            gh("smoka7/hop.nvim")
        },
        ["nvim-code-action-menu"] = {
            gh("weilbith/nvim-code-action-menu"),
            loadname = "code_action_menu"
        },
        ["plenary"] = {
            gh("nvim-lua/plenary.nvim"),
            loadname = "plenary"
        },
    }
end

-- Some plugins have a nice, easy setup mechanism that we integrate with all other
-- callbacks from other config modules nya
--
-- This does that for those
local function initialise_plugin_callbacks()
    plugins.register_callback("nvim-cmp", function(cmp)
        return {
            snippet = {
                expand = function(args)
                    local luasnip = plugins.loaded_or_nil("luasnip")
                    if luasnip == nil then error("Luasnip not loaded yet") end
                    luasnip.lsp_expand(args.body)
                end
            },
            -- Priority bundle ^.^ nya
            sources = cmp.config.sources({
                { name = "nvim_lsp" },
                -- Wait until later to add luasnip specifically as a source
                -- { name = "luasnip" },
            }, {
                { name = "buffer" }
            }),
            --[[
            formatting = {
                format = function(entry, item) 

                end
            }
            ]]
        }
    end)

    plugins.register_callback("fzf-lua", function(fzf_lua)
        -- Make this the general selection UI ^.^ nya
        fzf_lua.register_ui_select()
        on_unload(fzf_lua.deregister_ui_select)
    end)
end

-- Get the directory into which plugins are downloaded nya
function plugins.plugin_location()
    return vim.fn.stdpath("data") .. "/site/pack/packer/" 
end

function plugins.init()
    initialise_config_transforms()
    initialise_plugin_callbacks()

    plugins.bootstrap_status = packer_strap()
    if plugins.bootstrap_status == plugins.packer_strap_state.BOOTSTRAP_FAILED then
        log.error("FAILED TO BOOTSTRAP PACKER FROM GITHUB - NO PLUGINS CAN BE LOADED")
        return
    elseif plugins.bootstrap_status == plugins.packer_strap_state.BOOTSTRAP_SUCCEEDED then
        log.info("Bootstrapped packer from GitHub")
    end

    plugins.packer = require("packer")
    local packer = plugins.packer

    plugins.packer.reset()
    plugins.packer.init({
        -- Compile path was default dumped in my personal config and leaked some personal info
        -- we put it in the appropriate "generated data" location as a plugin, mirroring where it
        -- was placed by default as part of runtimepath except in a data part rather than actual git-commited
        -- config nya
        compile_path = vim.fn.stdpath("data") .. "/site/plugin/packer_compiled.lua"
    })


    -- Packer is special, and using `as` with it seems to make stuff really janky
    -- So we don't put it in the table of things. In particular, the compiled file
    -- seems to assume packer is stored at `<pack path>/<packer>/packer.nvim` even with
    -- aliasing nya
    packer.use { gh("wbthomason/packer.nvim") }

    -- Accumulate a real list of plugin names here using universal handlers. This is for reload purposes
    local constructed_plugin_reload_list = {}
    set_registration_handler("--", function(plugin, _)
        -- Don't actually care about this key, just calling back on all plugins
        local plugin_name = plugins.canonical_plugin_name(plugin)
        constructed_plugin_reload_list[plugin_name] = plugin
    end, true)

    local use = make_hijacked_use(packer.use)
    local plugin_list = plugins.plugins()

    -- Conveniently do table things
    --
    -- `use` is actually clean enough now that we could do it directly, but still. Maybe later
    for alias, package_spec in pairs(plugin_list) do
        package_spec.as = alias
        use(package_spec)
    end

    -- Config is now set up
    if plugins.bootstrap_status == plugins.packer_strap_state.BOOTSTRAP_SUCCEEDED then
        vim.ui.select({ "Yes", "No" },
            { prompt = "Successfully bootstrapped packer. Do you want to sync plugins (can be done later with :PackerSync)?" }
            , function(item, idx)
            -- If backed out, we default to no
            if item == "Yes" then
                log.info("Syncing packages")
                plugins.packer.sync()
            else
                log.warn("Not syncing packages. ")
            end
        end)
    end

    -- Turns out that no, packer does not rerun configs until
    -- a packer operation is done (:PackerCompile/Sync/etc.) nya.
    --
    -- Therefore, we need to RELOAD THE FUCK OUTTA THINGS if not
    -- on first load (i.e. reload = true).
    --
    -- EVERYTHING is done through hijacked config nya, so we call out to that

    if reload.reloaded then
        for plugin_name, plugin_info in pairs(constructed_plugin_reload_list) do
            hijacked_config(plugin_name, plugin_info)
        end
    end
end

return plugins
