-- Module for registering LSP servers.
--
-- Provides useful utilities for registering and unregistering on load/unload respectively.
local lsp = {}

lsp.submod = require("cfg.lsp.submod")

local reload = require("reload")
local log = require("cfg.log").with_prefix("LSP")


local plugins = require("cfg.plugins")
local echobuilder = require("cfg.utils.echobuilder")
local set = require("cfg.utils.set")
local iter = require("cfg.utils").iter
local if_nil = require("cfg.utils").if_nil
local utils = require("cfg.utils")

-- Things to do on unload
local on_unload_hooks = {}
local function on_unload(hook)
    table.insert(on_unload_hooks, hook)
end

-- Epoch-universal set of all client id's that have ever been requested to stop.
--
-- Because neovim uses new IDs forever, this doesn't need to be epoch'd.
local lsp_stopped_clients = set.new()

-- Mark a client or client id as stopped
--
-- We inject this into all clients.
local function client_stopped(client_or_client_id)
    if type(client_or_client_id) == "table" then
        client_stopped(client_or_client_id.id)
    elseif type(client_or_client_id) == "number" then
        lsp_stopped_clients[client_or_client_id] = true
    elseif type(client_or_client_id) == "nil" then
        -- Probably a client that has already been stopped and was retrieved from an ID that is
        -- no longer present in vim.lsp's table.
        -- Do nothing.
    else
        error("Invalid client or client ID marked as stopped")
    end
end

-- Perform injections of our own bookkeeping into existing vim.lsp client objects
---@param client_table vim.lsp.Client The client into which callbacks should be injected.
---@param modification_table table|nil Table for specifying more modifications to
--  the client table:
--  * always_force_stop: default false - if true, it force-stops the LSP from the first
--      stop request.
---@param epoch Epoch The epoch this client was created in (epoch is associated with the LSP
--- module table nya
--
-- This operation is *idempotent*. In particular, if it is called on the same client twice, it will not
-- perform the injections a second time. This is important, because `lsp.start` deduplicates language servers,
-- and that means that the client table passed to this is likely to be one that has already been injected.
--
-- What injections does this perform on the client:
-- * It adds immediate-stop to the client - i.e. requests for stopping immediately register
--   the client as stopped nya
-- * It adds a key "injected" with the value true to the table - this is used to prevent
--   duplication.
-- * it adds a key "epoch" with the epoch when this client was started.
local function perform_client_injections(client_table, modification_table, epoch)
    modification_table = vim.deepcopy(modification_table) or {}
    if client_table.injected then return end

    local injections = {
        stop = {
            pre = function(client, args)
                lsp_stopped_clients[client.id] = true
                if modification_table.always_force_stop then
                    args[1] = true
                end
            end,
        },
        -- This function is only used to determine whether or not to send changes in vim.lsp.client -
        -- so modifying it to use our stop request table so that it returns true from the moment being stopped
        -- was requested is fine nya
        is_stopped = {
            post = function(client, results)
                results[1] = results[1] or lsp_stopped_clients[client.id]
            end
        }
    }

    -- Inject into client
    utils.inject(client_table, injections)
    client_table.injected = true
    client_table.epoch = epoch
end

reload.epoch:get(lsp):in_each_epoch(function(epoch)
    local loc_lsp = epoch:data()

    -- Map of filetype  -> list of tables that look like {
    --    lsp_config = vim.lsp.start config table with some slightly extended capabilities documented
    --      in `lsp.register`.
    --    reuse_client = nil | function following the specification set out in `lsp.register`
    -- }
    --
    -- This is an actual map, not a set nya
    loc_lsp.langservers = {}

    -- All managed clients as a set of ids - that is, indexing an id results in a truthy
    -- value indicating if it's managed or not.
    loc_lsp.managedclients = set.new()
    -- Managed clients as a table of [language] -> {set of client ids}
    loc_lsp.managedclients_per_language = {}

    -- All active clients. This is not tied to the language stuff and more advanced things,
    -- and instead works almost identically to the vim.lsp internals in terms of timing
    -- (slightly before, actually). It is easier to do this than try and keep in sync
    -- a map of which active clients are associated with which epoch nya, and it
    -- makes the format correct which was an issue with the old active clients nya
    --
    -- This is actually a set of client IDs, rather than clients nya
    loc_lsp.active_clients = set.new()

    -- Set of [client ids] in the process of initialising
    --
    -- Without going through these, starting the same LS too frequently will result in
    -- multiple instances due to the delay between the call to vim.lsp.start_client() and
    -- the actual activation of that client (when the LSP server responds)
    --
    -- The internal details of vim.lsp.start have something similar to this. Unfortunately
    -- vim.lsp doesn't have a way to get a list of all initialised and uninitialised clients,
    -- only active ones nya
    --
    -- NOTE: before_init is not called with the starting client, but it's config. This meant that
    -- shimming this through callbacks was actually impossible in our own implementation of lsp.start.
    --
    -- However, this was only necessary due to the fact that `lsp.start` did not allow you to specify bufnr.
    -- By using the beta/dev neovim-0.9, we can get that, and hence avoid most of the need for our custom start
    -- function (but we still use it, because it allows us to set up automatic client stopping and stoptracking nya)
    --
    -- In our old implementation, severe issues were caused because this was not really usable. In particular, because
    -- it didn't count these half-starting clients as in the set to check for reusability, there was a race condition
    -- that caused a client to be started for *every* buffer on things like reload etc. nya
    -- loc_lsp.currently_starting_clients = set.new()
end)




-- when a new client is created for a given filetype. Duplicate ids are fine
--
-- Works within a specific epoch - it is an asynchronously used function.
local function maybe_new_client(ft, client_id, epoch_id)
    local epoch_data = reload.epoch:get(lsp, epoch_id):data()
    epoch_data.managedclients[client_id] = true
    epoch_data.managedclients_per_language[ft] = epoch_data.managedclients_per_language[ft] or set.new()
    epoch_data.managedclients_per_language[ft][client_id] = true
end

-- Called when a filetype client exits - deregisters it from the managed clients of all
-- filetypes it is related to
--
-- this automatically finds everywhere the client is registered, and unregisters it. Including
-- in different epoches. nya
local function client_exiting(client_id)
    reload.epoch:get(lsp):on_every_epoch(function(epoch)
        epoch:data().managedclients[client_id] = nil

        -- Identify filetypes for which the client is acting as an LSP provider
        for _, clients in pairs(epoch:data().managedclients_per_language) do
            if clients(client_id) then clients[client_id] = nil end
        end
    end)
end

-- Transform the table provided as config to one usable by vim.lsp.start() as config.
local function bufferlocal_start_config(config_table, bufnr, filetype, filename)
    -- I think this might actually pass through ref sooooo deepcopy!
    local config_table = vim.deepcopy(config_table)
    local transformkeys = {
        "cmd",
        "root_dir",
        "workspace_folders",
        "cmd_cwd",
        "cmd_env",
        "init_options"
    }

    for _, transkey in ipairs(transformkeys) do
        if type(config_table[transkey]) == "function" then
            config_table[transkey] = config_table[transkey](bufnr, filetype, filename)
        end
    end

    return config_table
end

-- Like `vim.lsp.start` but injects configuration into the clients, including setting
-- opts.bufnr as appropriate and adding epoch cleanup for it nya.
--
-- It also fuses in cmp-nvim-lsp capabilities if that plugin is loaded.
--
-- Options have extra keys here, beyond the standard:
-- * reuse_client - same as the normal vim.lsp.start, but the bufnr, filetype, and filename
--   for the location the `lsp.start` function is being called for are passed in after the 
--   normal two arguments. There is no guaruntee that the `bufnr` is a valid buffer when 
--   the user function is called, especially when ephemeral buffers are often a thing. 
-- * always_force_stop - Any request to stop does a force stop. Useful for clients
--   that don't respond to normal stop requests and mess up reuse_client detection nya
--
-- The last argument, if specified, controls what epoch we should be in. If not specified,
-- work in the current epoch nya. Can be an epoch id or epoch.
function lsp.start(config, opts, bufnr, epoch_id)
    local epoch = reload.epoch:get(lsp, epoch_id)
    if epoch:in_epoch() then
        bufnr = bufnr or vim.api.nvim_get_current_buf()
        -- Options for vim.lsp.start nya
        local start_opts = {}
        start_opts.bufnr = bufnr
        start_opts.reuse_client = (opts or {}).reuse_client
        local always_force_stop = (opts or {}).always_force_stop

        if type(start_opts.reuse_client) == "function" then
            local original_reuse_client = start_opts.reuse_client
            local filetype = vim.bo[bufnr].ft
            local filename = vim.api.nvim_buf_get_name(bufnr)
            start_opts.reuse_client = function(client, full_config)
                return original_reuse_client(client, full_config, bufnr, filetype, filename)
            end
        end

        -- Add bookkeeping into the config nya
        config = utils.inject(vim.deepcopy(config), {
            on_init = {
                pre = function(_, args)
                    local args = utils.indexmap(args, { "client", "init_result" })
                    epoch:data().active_clients[args.client.id] = true
                end
            },
            on_exit = {
                postargs = true,
                post = function(_, _, args)
                    -- In case of exiting *while starting*, on_init may not have been called, so we
                    -- clear ourselves from both currently starting clients and active clients nya
                    local args = utils.indexmap(args, { "code", "signal", "client_id" })
                    epoch:data().active_clients[args.client_id] = nil
                    client_exiting(args.client_id)
                end
            }
        }, "default")

        -- Add the nvim-cmp capabilities, if nvim-cmp is an available plugin.
        --
        -- This allows for real snippets and stuff like that nya
        local original_capabilities = config.capabilities
        config.capabilities = lsp.nvim_cmp_capabilities(nil, original_capabilities)


        -- vim.lsp.start will perform client deduplication :)
        --
        -- This may be a new client, but it also might *not* be
        local maybe_new_client = vim.lsp.start(config, start_opts)
        -- If nil then there was an error and we dont return anything
        if maybe_new_client ~= nil then
            local client = vim.lsp.get_client_by_id(maybe_new_client)
            -- Not new, has already gone through this process and is reused nya
            if client.injected then return maybe_new_client end
            -- It's now new for sure!
            local new_client = maybe_new_client
            -- Now inject everything
            perform_client_injections(
                client,
                { always_force_stop = always_force_stop },
                epoch
            )


            -- make_client_stop_hook(new_client_id)
            -- Add an epoch cleanup that shuts down this langserver.
            --
            -- Can't believe I forgot this! 🤦 nya
            epoch:cleanup(function()
                local client = vim.lsp.get_client_by_id(new_client)
                if client ~= nil then
                    client.stop()
                end
            end, "[lsp-cleanup] id: " .. new_client)
            return new_client
        end
    end
end

-- Attempt to scan langserver clients for the given buffer with the given filetype (programming
-- language or similar) and filename (if any), and attach them to the given buffer.
--
-- Bufnr - the buffer to be rescanned for client registration (if nil, then 0/current). The bufnr is also
--   checked for validity (if it's invalid, then no further processing happens).
-- Filetype - the filetype that the buffer has been discovered to be. If nil, read from the buffer,
--   but in e.g. filetype events this can be used to always use the newly discovered filetype nya
--   This can also be used to forcefully load a language server for a specific filetype/language
-- Filename - the filename of the buffer. May be nil in the case of e.g. unnamed buffers (empty string is
--   turned to nil also),
-- epoch_id - If specified, the epoch this originated from. If not specified, is done in the current
--   epoch
--
-- Note that if you use this to forcefully reload LSP clients, you probably need to kill all lsp clients
-- for a given set of filetypes beforehand so that the client-reuse function does not prevent loading
-- your new one nya
function lsp.scan_registered_servers_for_buffer(bufnr, filetype, filename, epoch_id)
    bufnr = bufnr or vim.api.nvim_get_current_buf()
    if not vim.api.nvim_buf_is_valid(bufnr) then return end
    filetype = filetype or vim.bo[bufnr].ft
    filename = filename or vim.api.nvim_buf_get_name(bufnr)

    if filename ~= nil and string.len(filename) == 0 then filename = nil end

    local epoch = reload.epoch:get(lsp, epoch_id)

    if epoch:in_epoch() then
        local serverlist = epoch:data().langservers[filetype]
        -- No servers to look at
        if serverlist == nil or #serverlist == 0 then
            log.warn("No registered servers with filetype " .. filetype)
            return
        end

        for _idx, lsp_server in ipairs(serverlist) do
            local lsp_config = lsp_server.lsp_config
            local lsp_start_opts = lsp_server.opts

            -- localise LSP config
            local lsp_config = bufferlocal_start_config(lsp_config, bufnr, filetype, filename)


            -- start it mfer! The reuse function does the work of preventing unnecessary new clients
            -- This uses our own internal reimplementation of start to allow for arbitrary bufnr
            local client_id = lsp.start(lsp_config, lsp_start_opts, bufnr, epoch)
            maybe_new_client(filetype, client_id, epoch)
        end
    end
end

reload.epoch:get(lsp):in_each_epoch(function(epoch)
    -- Set of filetypes that already have detection autogroups
    epoch:data().filetypes_with_autogroups = set.new()
    -- Set of autogroup IDs
    epoch:data().filetype_detection_augids = set.new()

    epoch:cleanup(function()
        for augid in iter(epoch:data().filetype_detection_augids) do
            vim.api.nvim_del_augroup_by_id(augid)
        end
        epoch:data().filetype_detection_augids = set.new()
        epoch:data().filetypes_with_autogroups = set.new()
    end, "[lsp - filetype detection autogroups]")
end)

-- Called on a set of filetypes that have been registered
--
-- Any new ones will get their own autogroups for filetype detection added, if in
-- the passed epoch (default current)
local function language_server_registered_for(filetype_set, epoch_id)
    local epoch = reload.epoch:get(lsp, epoch_id)
    if epoch:in_epoch() then
        local new_filetypes = filetype_set - epoch:data().filetypes_with_autogroups
        for new_ft in iter(new_filetypes) do
            local augname = "lsp_ft_detect_" .. new_ft
            local augid = vim.api.nvim_create_augroup(augname, { clear = true })
            -- Add to the set to clean up
            epoch:data().filetypes_with_autogroups[new_ft] = true
            epoch:data().filetype_detection_augids[augid] = true
            -- amatch is set to the filetype
            vim.api.nvim_create_autocmd("FileType", {
                group = augid,
                -- This is what makes sure the filetype is right nya
                pattern = new_ft,
                desc = "Automatic rescanning for LSP servers registered for filetype " .. new_ft,
                callback = function(event)
                    local bufnr = event.buf
                    local filename = event.afile
                    if filename ~= nil and string.len(filename) == 0 then filename = nil end
                    -- We actually use the original filetype because it means if there's some
                    -- fancy multi-filetype thing in future we're good since it will still use the
                    -- original registered filetype.
                    -- local filetype = event.amatch
                    local filetype = new_ft
                    -- scan the newly detected buffer thing as the detected filetype
                    lsp.scan_registered_servers_for_buffer(bufnr, filetype, filename, epoch)
                end
            })
        end
    end
end

-- Rescan the buffers with a filetype in the filetype set for language servers.
--
-- Only occurs in the given epoch. Existing servers should be shut down before running this,
-- or there is a good chance that this will just merge into the existing servers that were found nya
local function rescan_for_filetype_set(filetype_set, epoch_id)
    local epoch = reload.epoch:get(lsp, epoch_id)
    if epoch:in_epoch() then
        for _idx, buffer_id in pairs(vim.api.nvim_list_bufs()) do
            if filetype_set[vim.bo[buffer_id].ft] and vim.api.nvim_buf_is_loaded(buffer_id) then
                -- Everything should be rederived from buffer
                lsp.scan_registered_servers_for_buffer(buffer_id, nil, nil, epoch)
            end
        end
    end
end

-- Function called when a new language server was registered for the given filetype set
-- ({[ft1] = true, [ft2] = true, ...})
--
-- Scans existing buffers with any of the given filetypes. This relies on the fact that
-- the new server has been inserted into the table and can hence be discovered, started up,
-- etc. Note that previous versions of the server that would match it's current reuse function
-- must be shutdown or it probably won't start.
--
-- It also sets up the filetype detection (even if it already exists, it works fine :) nya)
--
-- This exists especially for the cases when:
--  * config is reloaded and filetypes are already detected (but ofc
--    there is no filetype event now)
--  * lsp servers are registered later on in config, asynchronously ^.^ (e.g. if they
--    call out and in sync config that would be slow)
--
-- Note that this requires epoch input - it's often sourced from registration of langservers nya,
-- which are very asynchronous
local function on_register(filetype_set, epoch_id)
    language_server_registered_for(filetype_set, epoch_id)
    rescan_for_filetype_set(filetype_set, epoch_id)
end

-- Register a new LSP config and the method to select if a client should be `vim.lsp.start()`-ed
-- for a buffer.
--
-- Parameters:
--  filter: either a string (single filetype) or a table-array of filetypes.
--    neovim has an excellent system for filetypes (custom or not) in the `vim.filetype`
--    library. You should use that if you want things like content based detection and such,
--    and then hook it into this.
--    https://neovim.io/doc/user/lua.html#lua-filetype
--
--  config: A table matching that specified in vim.lsp.start(), except with some extra
--    capabilities to use per-buffer init functions (in cases where the value can be nil this
--    can return nil also!
--
--    * `root_dir` if specified and a function that takes the arguments (bufnr, filetype, filename),
--      where filename is nil for unnamed buffers, can return a path. By default, `vim.lsp.start()`
--      can use this to deduplicate language servers and in combination with using the filename
--      can be used to split out 1 LS per project. This can also be used to provide e.g. special
--      config for when you are just looking at an isolated file with no project or something.
--
--    * `workspace_folders` can also have a value generated by a function in the same manner as
--      `root_dir` on a per-buffer basis. Result should be the normal value as specified in
--      vim.lsp, just like with `root_dir`
--
--    * `cmd` is yet more of the same.
--    * `cmd_cwd` is more of the same.
--    * `cmd_env` is even more of the same,
--    * `init_options` is EVEN MORE of the same ^.^
--
--  opts: A table containing various options for modifying the configured language server
--    * reuse_client: If specified, this determines if the client should be reused or not.
--      It gets run over all existing clients at time of start, including on delayed registration,
--      parameters are:
--       * The client to check (a vim.lsp.client - see :help)
--       * our full config (localised to the current buffer)
--       * The bufnr of the buffer it is being started for
--       * The filetype of the buffer it is being started for
--       * The filename of the buffer it's being started for 
--      Default checks the name AND the root directory nya
--      See: https://github.com/neovim/neovim/blob/93d99aefd314bc4abfc54c0c29a4de84b6fcc823/runtime/lua/vim/lsp.lua#L864-L867
--    * always_force_stop: Some language servers are not.... cooperative when being stopped the first
--       time. This doesn't play well with our start code when it looks for
--
--  epoch_id: In what epoch was this registered. Must be specified, as this is too
--    errorprone without this nya. Note that registration always occurs even for old epochs, but
--    starting is managed fine :). Can be an epoch or a number nya
function lsp.register(filter, config, opts, epoch_id)
    if epoch_id == nil then
        error("LSP registration is an asynchronous process, so you need to pass an epoch")
    end

    if type(filter) == "string" then
        filter = { filter }
    end

    if type(filter) ~= "table" then
        error("Filter was not a string or table of filetypes")
    end

    -- Flip filter into a set where the filetypes are keys to booleans
    local filter_set = set.new(unpack(filter))
    filter = filter_set

    local epoch = reload.epoch:get(lsp, epoch_id)

    if type(config.name) == "string" then
        log.info("Registering language server with name %s (epoch: %s)", config.name, epoch:epoch_id())
    else
        log.info("Registering language server with dynamic or derived name (epoch: %s)", epoch:epoch_id())
    end


    for filetype in iter(filter) do
        epoch:data().langservers[filetype] = epoch:data().langservers[filetype] or {}
        table.insert(epoch:data().langservers[filetype], {
            lsp_config = config,
            opts = opts
        })
    end

    -- Schedule registration callbacks to be run later
    on_register(filter_set, epoch)
end

-- HYPOTHESIS - some lsp servers will not respond to stop request (in particular,
-- lua-language-server). This means they hang around on restart in the active client, and
-- then reuse_client is unhappy and everything just breaks.
--
-- SOLUTION - mark servers as needing force-stop somehow nya. Perhaps a table in this module
-- nyaa ^.^


function lsp:__unload()
    for _idx, hook in ipairs(on_unload_hooks) do
        hook()
    end
    on_unload_hooks = {}
end

-- Default omnifunc for nvim has much reduced LSP capabilities.
--
-- This provides capabilities from the cmp-nvim-lsp plugin if present, or nil if not nya,
-- and will let you pass overrides and existing capabilities. Returns capabilities to 
-- be passed o
function lsp.nvim_cmp_capabilities(overrides, capabilities)
    local cmp_lsp = plugins.loaded_or_nil("cmp-nvim-lsp")
    if cmp_lsp then
        return cmp_lsp.modify_capabilities(overrides, capabilities)
    else
        return capabilities 
    end
end

-- All langs which have at least one registered running language server, as a list
function lsp.running_langs()
    local langs = {}
    for lang, _ls in pairs(lsp.get_current_managed_client_ids_per_language()) do
        table.insert(langs, lang)
    end
    return langs
end

-- All languages that have at least one registered language server, as a list
function lsp.langs()
    local langs = {}
    for lang, _cfg in pairs(reload.epoch:get(lsp):data().langservers) do
        table.insert(langs, lang)
    end
    return langs
end

-- Get the managed client ids in the current epoch, as a set
function lsp.get_current_managed_client_ids()
    return reload.epoch:get(lsp):data().managedclients
end

function lsp.get_current_managed_client_ids_per_language()
    return reload.epoch:get(lsp):data().managedclients_per_language
end

-- Garbage-collect any clients not attached to any buffers (that arent in the process of initializing nya),
-- and stop them ^.^.
--
-- Applies to all LSP clients in neovim, so this works if an epoch has ended badly. This schedules the clients to stop.
function lsp.gc_clients()
    for _, client in pairs(vim.lsp.get_active_clients()) do
        if #vim.lsp.get_buffers_by_client_id(client.id) < 1 then
            vim.schedule(client.stop)
        end
    end
end

--[[
-- Start a periodic garbage collection timer for the current epoch that will remove
-- unattached language servers every 2 minutes.
--
-- Automatically registers a timer deletion for the epoch cleanup. This also prevents
-- accidental duplicate timer registration nya. It performs immediate gc the first run.
local function periodic_gc()
    -- 2 min * 60 s / min * 1000 ms/s
    local time_period = 2 * 60 * 1000
    reload.with_epoch_local_data(lsp, function(loc_lsp)
        if loc_lsp.client_garbage_collection_timer == nil then
            loc_lsp.client_garbage_collection_timer = vim.loop.new_timer()
            loc_lsp.client_garbage_collection_timer:start(0, time_period, function()
                lsp.gc_clients()
            end)
            reload.epoch_cleanup(lsp, function(loc_lsp)
                if loc_lsp.client_garbage_collection_timer ~= nil then
                    loc_lsp.client_garbage_collection_timer:close()
                end
            end)
        end
    end)
end
]]
-- Stop all LSP clients (in the current epoch). If language filter is not nil, then
-- only stop the ones for the given language(s) - the languages can be either a string
-- (for one) or a set (for multi).
--
-- This will also kill clients in the process of starting, btw ^.^ - though for languages,
-- just-starting clients will not be stopped.
--
-- Second argument is a boolean. If true, then it force stops everything immediately nya
function lsp.stop_all(language_filter, force_stop)
    local epoch_data = reload.epoch:get(lsp):data()

    local client_set
    if language_filter ~= nil then
        if type(language_filter) == "string" then
            language_filter = set.new(language_filter)
        end
        client_set = set.new()
        for language in iter(language_filter) do
            local language_client_set = epoch_data.managedclients_per_language[language] or set.new()
            set.union_in_place(client_set, language_client_set)
        end
    else
        client_set = epoch_data.active_clients
    end
    for client_id in iter(client_set) do
        local client = vim.lsp.get_client_by_id(client_id)
        if client ~= nil then
            client.stop(force_stop)
        end
    end
end

---@deprecated
lsp.lsp_buffer_configuration = require("cfg.lsp.submod.autocmds").lsp_buffer_configuration
lsp.register_lsp_buffer_action = require("cfg.lsp.submod.autocmds").register_lsp_buffer_action


function lsp.init()
    lsp.submod.init(lsp)

    local rust = require("cfg.lsp.rust")
    local lua = require("cfg.lsp.lua")
    local cpp = require("cfg.lsp.cpp")
    local vim_lsp = require("cfg.lsp.vim")
    local python = require("cfg.lsp.python")
    local markdown = require("cfg.lsp.markdown")
    local openscad = require("cfg.lsp.openscad")
    local go = require("cfg.lsp.go")

    rust.init()
    lua.init()
    cpp.init()
    vim_lsp.init()
    python.init()
    markdown.init()
    openscad.init()
    go.init()
    --periodic_gc()
end

return lsp
