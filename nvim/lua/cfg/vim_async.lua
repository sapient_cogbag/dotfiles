--- Integration of the async module into vim stuff
local vim_async = {}
local async = require("cfg.async")
vim_async.async = async


--- Wait for a fixed time before continuing with an async function.
---@async
---@param timeout_ms integer The time to wait in milliseconds.
---@return Future
function vim_async.delay(timeout_ms)
    return async.future.new(function()
        ---@param outer_future Future
        ---@param outer_bundle any
        coroutine.yield(function(outer_future, outer_bundle)
            vim.defer_fn(function() outer_future:resume(nil, outer_bundle) end, timeout_ms)
        end)
        return nil
    end)
end

--- Resume "soon" on the main event loop.
--- Useful to avoid for example, textlock or similar nya. Essentially an equivalent to vim.schedule, 
--- except the callback-ness of that function is just encapsulated within the future.
--- @async
--- @return Future
function vim_async.defer_soon()
    return async.future.new(function()
        coroutine.yield(function(outer_future, outer_bundle)
            vim.schedule(function() outer_future:resume(nil, outer_bundle) end)
        end)
        return nil
    end)
end

function vim_async.init()
end

return vim_async
