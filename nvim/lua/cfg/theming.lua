-- Entry point for anything around terminal colour stuff and general theming.
local theming = {}

theming.termgui = require("cfg.theming.termgui")
theming.lualine = require("cfg.theming.lualine")
theming.floatlink = require("cfg.theming.floatlink")
theming.code_action_menu = require("cfg.theming.code_action_menu")
-- Looking at the work involved in writing powerline-style stuff in lua 
-- (as opposed to rust or something nya), gonna just use a plugin.
-- theming.electroline =  require("cfg.theming.electroline")

local log = require("cfg.log").with_prefix("theming")

function theming.tabs() 
    log.info("Loading tab management options")
    -- Use indents of 4 spaces
    vim.o.shiftwidth = 4
    -- Always replace tabs with spaces
    -- Smarttab does this in front of lines tho
    vim.o.expandtab = true
    -- What a real tab counts for in files. Insertion means we need not worry 
    -- since we always use tab -> space conv
    vim.o.tabstop = 4
    -- Calculating tabs from spaces in editing operations (e.g. backspace or deletion)
    vim.o.softtabstop = 4
end

-- Stuff like line numbers, not wrapping, etc. nya
function theming.textoptions()
    log.info("Loading buffer style options")
    vim.o.wrap = false
    vim.o.number = true
end

-- The funny thing at the top that shows buffers or tabs or whatever, and the thing at the bottom
-- too
function theming.lines() 
    -- Always show the tabline even when there's only 1 tab
    vim.o.showtabline = 2
end
 

function theming.init()
    theming.tabs()
    theming.textoptions()
    theming.lines()
    theming.termgui.init()
    
    -- For now
    --
    -- We put stuff after this so linking successfully overrides nya
    vim.cmd.colorscheme("industry") 

    theming.lualine.init()
    theming.floatlink.init()
    
    theming.code_action_menu.init()

    log.info("THEMING LOADED")
end

return theming
