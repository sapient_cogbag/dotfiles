-- Log is a special package. 
--
-- It provides certain guaruntees - in particular, it avoids doing anything complex in init(), and loads output 
-- directly rather than via reload.mod (because that prevents issues of cycles when reload loads this
local log = {}

local output = require("cfg.utils.output")

log.level = {}

-- We always add to history.
-- Our logging's visibility is managed in init() nya

-- Add an extra, module-local prefix if not nil
local function withpre(normal, prefix)
    if not prefix then
        return "[" .. normal .. "] "
    else 
        return "[" .. normal .. "][" .. prefix .. "] "
    end
end

log.level.TRACE = {}
function log.level.TRACE.emit(s, prefix )  
    output.echomsg_multiline(withpre("TRACE", prefix), s)
end


log.level.DEBUG = {}
function log.level.DEBUG.emit(s, prefix)
    output.echomsg_multiline(withpre("DEBUG", prefix), s)
end

log.level.INFO = {}
function log.level.INFO.emit(s, prefix)
    output.echomsg_multiline(withpre("INFO", prefix), s, "ModeMsg", "None")
end

log.level.WARN = {}
function log.level.WARN.emit(s, prefix)
    output.echomsg_multiline(withpre("WARN", prefix), s, "WarningMsg")
end

log.level.ERROR = {}
function log.level.ERROR.emit(s, prefix)
    output.echoerr_multiline(withpre("ERROR", prefix), s)
end

-- Perform logging with the given level table, prefix,
-- fmt_message, and formatting args nya
--
-- fmt_message is usually just a string. However, it
-- can be a table, with the first integer key being a string, and with
-- the following other parameters:
-- * schedule (default false) - if true, schedule logging to be executed "soon" on 
--   the event loop rather than immediately. 
--
-- Uses the magic prefix
function log.log(level, prefix, fmt_message, ...)
    if type(fmt_message) == "string" then
        fmt_message = {fmt_message}
    end

    local capture = { ... }
    local do_it = function ()
        local actual_string = string.format(fmt_message[1], unpack(capture))
        if level.enabled then 
            level.emit(actual_string, prefix)
        end
    end

    if fmt_message.schedule then
        local old_do_it = do_it
        do_it = function ()
            vim.schedule_wrap(function() old_do_it() end)
        end
    end

    do_it()
end

for level_name, level_table in pairs(log.level) do 
    log[string.lower(level_name)] = function (fmt_message, ...) 
	    log.log(level_table, nil, fmt_message, ...) 
    end
end

-- This just sets up the magic enable/disable values. 
function log.init() 
   log.level.TRACE.enabled = false
   log.level.DEBUG.enabled = false
   log.level.INFO.enabled = true
   log.level.WARN.enabled = true
   log.level.ERROR.enabled = true
end

-- Create a new `with_prefix` function that adds on an extra prefix
-- when provided with the existing prefix and the base-level log table nya
local function new_with_extra_prefix(baselog, existing_prefix)
    return function (new_extension) 
        local new_log = {}
        local new_prefix
        if existing_prefix then 
            new_prefix = existing_prefix .. ":" .. new_extension
        else
            new_prefix = new_extension
        end 

        -- Use actual prefix nya
        for level_name, level_table in pairs(baselog.level) do
            new_log[string.lower(level_name)] = function(fmt_message, ...)
                baselog.log(level_table, new_prefix, fmt_message, ...)
            end
        end
        
        new_log.with_prefix = new_with_extra_prefix(baselog, new_prefix)

        return new_log
    end
end

log.with_prefix = new_with_extra_prefix(log)

return log
