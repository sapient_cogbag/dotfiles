-- (incomplete) module for modes: https://neovim.io/doc/user/map.html#%3Amap
local s = {}
s.edit_mode = {} 

-- Table is [name] and [map_prefix] for now.
--
-- Adds to module table
local function mode (datatable)
    s[datatable.name] = datatable
    s.edit_mode[datatable.name] = datatable.map_prefix
end

-- The basic map-y-ness
-- :map
mode { name = "DEFAULT", map_prefix = "" } 
-- :nmap
mode { name = "NORMAL", map_prefix = "n" }
-- :vmap 
mode { name = "VISUAL", map_prefix = "v" }
-- :xmap
mode { name = "VISUAL_NOSELECT", map_prefix = "x" }
-- :smap
mode { name = "SELECT", map_prefix = "s" } 
-- :omap
-- Used for essentially custom operators after y/c/etc.
mode { name = "OPERATOR_PENDING", map_prefix = "o" }
-- :map!
mode { name = "INSERT_COMMANDLINE", map_prefix = "!" }
-- :imap
mode { name = "INSERT", map_prefix = "i" }
-- :cmap
mode { name = "COMMANDLINE", map_prefix = "c" }
-- :lmap
-- Essentially applies when entering text, used for e.g. shorthands (I think) nya
mode { name = "LANGUAGE", map_prefix = "l" }
-- :tmap
mode { name = "TERMINAL", map_prefix = "t"} 

-- Modes as resulting from mode() function. Only matched on the first letter
s.detected_mode = {}

local function d_mode(tbl)
    s.detected_mode[tbl.name] = tbl.char
end

d_mode { name = "NORMAL", char = 'n' }

d_mode { name = "VISUAL", char = 'v' } 
d_mode { name = "VISUAL_LINE", char = 'V' }
d_mode { name = "VISUAL_BLOCK", char = vim.api.nvim_replace_termcodes("<C-V>", true, true, true)}

d_mode { name = "SELECT", char = 's' } 
d_mode { name = "SELECT_LINE", char = 'S' }
d_mode { name = "SELECT_BLOCK", char = vim.api.nvim_replace_termcodes("<C-S>", true, true, true)}

d_mode { name = "INSERT", char = 'i' }
d_mode { name = "REPLACE", char = 'R' }

d_mode { name = "COMMANDLINE", char = 'c' } 
d_mode { name = "PROMPT", char = 'r' }
d_mode { name = "SHELL", char = '!' } 
d_mode { name = "TERMINAL", char = 't' } 

local mod = require("reload").mod
local set = mod("utils.set")
local dmode = s.detected_mode

s.detected_mode.normal = set.new(dmode.NORMAL)
s.detected_mode.visual = set.new(dmode.VISUAL, dmode.VISUAL_LINE, dmode.VISUAL_BLOCK) 
s.detected_mode.select = set.new(dmode.SELECT, dmode.SELECT_LINE, dmode.SELECT_BLOCK) 
s.detected_mode.insert_replace = set.new(dmode.INSERT, dmode.REPLACE)
s.detected_mode.other = set.new(dmode.COMMANDLINE, dmode.PROMPT, dmode.SHELL, dmode.TERMINAL) 

return s

