-- The only module that does not get forcefully unloaded.
--
-- When reloading, the first request for reload will mark a variable "reloaded" that can
-- allow things like forcefully re-doing events that would only be triggered once.
--
-- Mechanism inspired by: https://gitlab.com/CraftedCart/dotfiles/-/blob/master/.config/nvim/lua/c/reload.lua
-- but with a mechanism for reverting breaking changes in case of config load failure (so
-- further loads work and config is restored to known state nya)
--
-- # EPOCHS
-- This module provides a reload mechanism, and this can cause problems with e.g. callbacks, autocmds that are
-- put through vim.schedule(), etc. nya
--
-- To solve this, this module provides an advanced mechanism for determining the loadedness of a module nya, and
-- also some special techniques for "epoch" management. To enable this, simply don't interfere with the __reload_data
-- key.
--
-- # RELOADING
-- When creating a module, it is often desirable to write state without needing to put it in .init() or something so that
-- it can be placed next to functions that use it.
--
-- To enable this, there exists a key inside __reload_data - in particular "reload hooks" - that acts as a list of callbacks
-- for when reloading. As such you can register callbacks to that to rerun them on reload, even if the file is not replaced
-- by a new one. Furthermore, by default, before cfg.init() is called, any epoch_local_data calls are automatically added to
-- this hook list unless specified afterward, and hence these calls can be placed directly in the module without worry for
-- when reloading nya.
local reload = {}
local mod_prefix = "cfg"

-- Instance of log that is held by reload independent of all other packages nya
--
-- Normal unload is fine since we have a local ref ^.^
local log = require("cfg.log")
log.init()
log = log.with_prefix("reload")


-- Current epoch
local epoch = 1

-- When reloading, whether or not the default for epoch_local_data calls is to register them
-- as reload callbacks or not nya. Before init - true. After init - false
--
-- In practise it is slightly more complex than that. `mod` calls are wrapped to ensure this is true
-- while they go through initial processing and false afterwards, so that mods inside init() functions
-- still work ok.
--
-- note that this is DEPRECATED!!!!!!
---@deprecated
local default_register_data_reload = true

-- Perform a function with the reload data as an argument for the given module table,
-- only if the reload data is an actual table ^.^, and error otherwise.
--
-- If it is nil, this will also initialise that key with a new empty table.
local function with_reload_data(module_table, callback)
    if module_table.__reload_data == nil then
        module_table.__reload_data = {}
    end

    if type(module_table.__reload_data) == "table" then
        return callback(module_table.__reload_data)
    else
        error("__reload_data was not table or nil, can't use advanced features of reload")
    end
end

local function module_has_reload_data(module_table)
    return type(module_table) == "table" and type(module_table.__reload_data) == "table"
end

-- If hook is nil, returns the data. Else does nothing.
function reload.reload_hook(module_table, hook)
    return with_reload_data(module_table, function(data)
        data.reload_hooks = data.reload_hooks or {}
        if hook ~= nil then
            table.insert(data.reload_hooks, hook)
        else
            return data.reload_hooks
        end
    end)
end

-- Retrieves the epoch of the current module at the given time. This also acts
-- to initialise epochs.
--
-- You should not use this inside a scheduled callback - that would return the epoch
-- for when the given callback was actually called, and would be useless nya
--
-- Note that all this data is actually stored inside the module itself rather than in reload.
--
-- This avoids causing reload to lock down references to every version of every module on each reload nya
--
-- Fake epoch can be used to force-init a certain epoch for a module.
---@deprecated
function reload.deprecated_epoch(module_table, fake_epoch)
    error("Deprecated call to epoch")
    local is_epoch = true
    if fake_epoch ~= nil then
        is_epoch = epoch == fake_epoch
    end
    if fake_epoch == nil then
        fake_epoch = epoch
    end
    return with_reload_data(module_table, function(data)
        data.epoch = data.epoch or {}
        data.epoch[fake_epoch] = data.epoch[fake_epoch] or {
            -- Whether still in this epoch
            -- @type bool
            in_epoch = is_epoch,
            -- Epoch-local data
            epoch_local_data = setmetatable({}, { __index = { epoch = fake_epoch } }),
            -- Epoch cleanup callbacks. Take epoch local data as sole param
            epoch_cleanup_hooks = {}
        }
        return epoch
    end)
end

-- Epoch is a class-like structure.
---@generic Module: table<any, any> 
---@class Epoch <Module>
---@field protected __epoch number
---@field protected __for_module Module The module table this epoch is actually for nya
---@field protected __in_epoch bool
---@field protected __on_epoch_end (table|fun())[] Either a table with [1]: function taking the epoch, and optionally a [name] key,
--  or an actual bare function nya
---@field protected __epoch_local_data table<any, any> Data for the module of this epoch
reload.epoch = {}

-- Error in case of invalid keys
setmetatable(reload.epoch, {
    __index = function (tbl, unknown_key)
        error("Unknown key: " .. unknown_key)
    end
})

-- Construct or access the epoch for the module.
---@param module_table table - the module to obtain epoch-local data for
---@param epoch_identifier  Epoch|number? - The epoch to get epoch-local data for. Defaults to the current epoch nya.
--   This can also function when passed an epoch object.
---@return Epoch - The epoch data for the given module table, as derived from the epoch
--   ID or the epoch itself nya
function reload.epoch:get(module_table, epoch_identifier)
    if type(epoch_identifier) == "table" then
        epoch_identifier = epoch_identifier.__epoch
    end
    ---@type number
    epoch_identifier = epoch_identifier or epoch
    return with_reload_data(module_table, function(reload_data)
        ---@type table<number, Epoch>
        reload_data.epoch = reload_data.epoch or {}
        ---@type Epoch
        local field_table = {
            __epoch = epoch_identifier,
            __for_module = module_table,
            __in_epoch = (epoch_identifier == epoch),
            __on_epoch_end = {},
            ---@type table<any, any>
            __epoch_local_data = {}
        }
        -- Outer table contains allowed fields - __newindex checks with rawget.
        reload_data.epoch[epoch_identifier] = reload_data.epoch[epoch_identifier] or setmetatable(field_table, {
            -- Assigning directly as index rather than using a function lets us get better LSP
            -- support, and we can still get the nice erroring because we set the metatable on 
            -- reload.epoch instead! nya 
            __index = reload.epoch,
            -- Only ever called for new, currently-nil fake values nya.
            -- We always want to error in this case
            __newindex = function(field_table, key)
                error("Cannot assign new field to epoch table :" .. key)
            end
        })
        return reload_data.epoch[epoch_identifier]
    end)
end

-- When receiving epoch data from other modules, calling this method
-- will obtain the epoch data for the passed module in the same epoch and 
-- return it.
---@generic Module: table<any, any>
---@param module_table Module
---@return Epoch<Module>
function reload.epoch:for_module(module_table)
    -- Self, here, is the epoch data nya
    return reload.epoch:get(module_table, self:epoch_id())
end

-- Ensure in_epoch inside this, matches the actual current epoch state nya
function reload.epoch:refresh_epoch()
    self.__in_epoch = self.__epoch == epoch
end

-- True if in this epoch, false otherwise
function reload.epoch:in_epoch()
    return self.__in_epoch
end

-- Get the epoch identifier for this epoch
function reload.epoch:epoch_id()
    return self.__epoch
end

-- Add a callback to be called on the end of the epoch.
--
-- Parameter to the callback is this epoch object.  Callback can also be
-- a table with the first element a function and a name = "debug name here" element nya
--
-- Or if it gets a second argument, that will be the name instead, and the callback should just
-- be a plain function.
---@generic Module: table<any, any>
---@param callback fun(epoch: Epoch<Module>)|table
---@param name string?
function reload.epoch:on_epoch_end(callback, name)
    if name ~= nil then
        table.insert(self.__on_epoch_end, { callback, name = name })
    else
        table.insert(self.__on_epoch_end, callback)
    end
end

-- Alias for on_epoch_end
reload.epoch.cleanup = reload.epoch.on_epoch_end

-- Get the local epoch data table nya
---@return table<any, any>
function reload.epoch:data()
    return self.__epoch_local_data
end

-- Run a callback on every epoch that the table for this epoch has experienced nya
--
-- Callback is passed the epoch object
---@generic Module: table<any, any>
---@param callback fun(epoch: Epoch<Module>)
function reload.epoch:on_every_epoch(callback)
    with_reload_data(self.__for_module, function(reload_data)
        for epoch_id, epoch in pairs(reload_data.epoch) do
            callback(epoch)
        end
    end)
end

-- Runs a function taking the epoch object as a parameter - AND registers that function to run
-- on each new epoch the module table experiences nya
---@generic T
---@param callback fun(new_current_epoch: Epoch): T?
---@return T? - but only on the first load of this module is this function usually called. 
function reload.epoch:in_each_epoch(callback)
    local module = self.__for_module
    reload.reload_hook(module, function()
        -- Get the new epoch for the module that this one is nya
        return callback(reload.epoch:get(module))
    end)
    return callback(self)
end

-- End the epoch - assumes this does indeed need to be ended, and calls the epoch end functions.
-- Will e.g. set the epoch to not being in-it automatically.
--
-- This will not fail (uses pcall nya)
function reload.epoch:__end_epoch()
    self.__in_epoch = false
    for _idx, cleanup_hook in ipairs(self.__on_epoch_end) do
        local name = _idx
        if type(cleanup_hook) == "table" then
            if cleanup_hook.name ~= nil then
                name = cleanup_hook.name
            end
            cleanup_hook = cleanup_hook[1]
        end
        log.debug("cleanup-hook: %s", name)
        local ok, err = pcall(cleanup_hook, self)
        if not ok then
            log.warn("err running cleanup hook: %s", name)
            log.warn("%s", err)
        end
    end
end

-- Returns whether or not this actually needs ending by comparing to the current epoch nya
function reload.epoch:needs_ending()
    return self.__epoch < epoch
end

-- Make it so that the callback is only called within this epoch.
--
-- If not in this epoch, the returned function returns nil nya
---@generic Args, Return
---@param callback fun(args: Args): Return
---@return fun(args: Args): Return?
function reload.epoch:only_in_epoch(callback)
    return function(...)
        if self.__in_epoch then
            return callback(...)
        else
            return nil
        end
    end
end

-- Utility function for getting epoch data for the given module and acting on it's data.
-- Last argument is a callback taking the epoch data table nya. Returns the same as the
-- callback inside.
--
-- In the case of no epoch data for the given epoch, this will pass in nil
local function with_epoch(module_table, epoch_id, epoch_data_callback)
    return with_reload_data(module_table, function(data)
        data.epoch = data.epoch or {}
        return epoch_data_callback(data.epoch[epoch_id])
    end)
end

-- Modify your module's per-epoch data for the current epoch with a callback taking that
-- data as a table. Or just retrieve it :)
--
-- Returns whatever the callback returns.
--
-- @param register_reload_hook - If not nil, indicates whether or not to register this data
-- modifier as something to be automatically run on reload. If nil, then it uses the default
-- (true before cfg.init() is called, false after ^.^).
--
---@deprecated
function reload.with_epoch_local_data(module_table, data_modifier, register_reload_hook)
    local current_epoch = reload.deprecated_epoch(module_table)
    if register_reload_hook == nil then
        register_reload_hook = default_register_data_reload
    end

    if register_reload_hook then
        reload.reload_hook(module_table, function()
            local new_epoch = reload.deprecated_epoch(module_table)
            -- Obviously in this case there is nowhere to return it to nya
            with_epoch(module_table, new_epoch, function(raw_epoch_data)
                return data_modifier(raw_epoch_data.epoch_local_data)
            end)
        end)
    end

    return with_epoch(module_table, current_epoch, function(raw_epoch_data)
        return data_modifier(raw_epoch_data.epoch_local_data)
    end)
end

-- Your primary means of running code but only in the correct epoch.
--
-- Takes a number returned from `reload.epoch`, and then - ONLY if the epoch is still correct,
-- is the callback actually run. It is provided the epoch-local data as an argument.
--
-- Returns nil if not in epoch, returns the value of the callback if in epoch nya
--
-- If the callback is nil, this does something a little different. It produces a function that
-- can then be used to wrap another function.
--
-- In that case, the parameter of the returned function is a function that takes the epoch local data
-- as first argument, and the rest are forwarded to it. In the case that the epoch was not current, then
-- the local epoch data is instead passed as nil, so you can handle the default case nya
--
-- Epoch local data by default contains a .epoch key with the current epoch, in case you need to
-- chain things further with inner in_epoch calls or things taking them nya.
---@deprecated Old epoch shite
function reload.in_epoch(module_table, epoch_id, callback)
    -- Sometimes raw_epoch_data is nil when nothing epoch-y has been
    -- done with a module. In such a case, we initialise that with fake epoch
    -- to avoid that problem - it just ensures raw_epoch_data isnt nil ^.^
    reload.deprecated_epoch(module_table, epoch_id)
    if callback ~= nil then
        return with_epoch(module_table, epoch_id, function(raw_epoch_data)
            if raw_epoch_data.in_epoch then
                return callback(raw_epoch_data.epoch_local_data)
            end
        end)
    else
        -- Transform function
        return function(inner_callback)
            -- Function that forwards arguments to the inner callback conditionally on
            -- epoch.
            return function(...)
                -- Selector
                -- Note that we can't capture varargs directly, so we put in a table
                -- then unpack again on the inside nya
                local varargs = { ... }
                return with_epoch(module_table, epoch_id, function(raw_epoch_data)
                    if raw_epoch_data.in_epoch then
                        return inner_callback(raw_epoch_data.epoch_local_data, unpack(varargs))
                    else
                        return inner_callback(nil, unpack(varargs))
                    end
                end)
            end
        end
    end
end

-- Very aggressive function that lets you run a callback for all epoches. Rarely useful,
-- but can sometimes be used when dealing with global neovim state with globally unique
-- IDs, to ensure absolutely no remaining references nya.
--
-- Unlike with reload.in_epoch and friends, this one does not have an option to transform
-- a function into a conditional callback, as it can repeat any number of times nya.
---@deprecated
function reload.in_all_epoch(module_table, callback)
    -- Force init nya
    local current_epoch = reload.deprecated_epoch(module_table)
    with_reload_data(module_table, function(reload_data)
        for epoch, epoch_data in pairs(reload_data.epoch) do
            callback(epoch_data.epoch_local_data)
        end
    end)
end

-- Like `reload.in_epoch`, except epoch_id can be nil. If it is nil, then
-- the callback is immediately run in the current epoch, but if it is specified
-- then it is only run in the passed epoch.
--
-- This is useful for APIs that may be called immediately OR called with some
-- amount of delay/asynchrony nya
--
-- Like in_epoch, this also has a mode - when callback is nil - that it produces
-- a function that can transform any other function into one conditional on the current
-- epoch nya.
---@deprecated
function reload.in_epoch_if_specified(module_table, epoch_id, callback)
    local actual_epoch = epoch_id
    if actual_epoch == nil then
        actual_epoch = reload.deprecated_epoch(module_table)
    end
    -- If the current epoch then this will immediately run.
    return reload.in_epoch(module_table, actual_epoch, callback)
end

-- Callback to be run at the end of the current epoch.
--
-- Sole parameter to the callback is the epoch local data nya.
-- When you want to associate this with some epoch-local data creation, it's best to include
-- a call to this function inside the local data generator. That way it ensures
-- that if that data generator is registered as a reload hook, the cleanup is also registered
-- at the same time, every time.
--
-- A third argument can also be provided - a name - which can be useful in debugging nya
--
-- If callback is nil then return the list of hooks that must be called :)
---@deprecated
function reload.epoch_cleanup(module_table, callback, name)
    local current_epoch = reload.deprecated_epoch(module_table)
    return with_epoch(module_table, current_epoch, function(raw_epoch_data)
        if callback ~= nil then
            table.insert(raw_epoch_data.epoch_cleanup_hooks, { callback, name = name })
        else
            return raw_epoch_data.epoch_cleanup_hooks
        end
    end)
end

-- By default, the lua/ folder in neovim's modules are dumped directly into the global neovim
-- module namespace nya
--
-- We want to do reload in the same style as https://gitlab.com/CraftedCart/dotfiles/-/blob/master/.config/nvim/lua/c/reload.lua
-- To do this, we provide a load function that extends 'cfg.' to the start of a package name, and dump our outermost level packages in there
--
-- DEPRECATED: Now that:
-- * The old mechanism of epochs is deprecated, and running things on each reload is 
--   managed from within the epoch objects with reload hooks as opposed to with with_epoch_local_data
--   and automatic registering
-- * We have real LSP navigation and full paths can be used for serious completion in my config
-- This becomes more of a hinderance than a help. Use `require` instead nya, the reload mechanisms
-- work on `package.loaded` and were designed from the start to work with native requires anyhow.
---@deprecated
function reload.mod(postfix)
    -- local old_default_reload_autocallback = default_register_data_reload
    -- default_register_data_reload = true
    local inner_module = require(mod_prefix .. "." .. postfix)
    -- default_register_data_reload = old_default_reload_autocallback
    return inner_module
end

-- New epoch occurs.
local function new_epoch()
    epoch = epoch + 1
    log.info("new epoch %s", epoch)
end

-- Run every single necessary thing. If the epoch has changed, this will run all the cleanups and such.
local function epoch_end(module_table)
    if module_has_reload_data(module_table) then
        with_reload_data(module_table, function(raw_reload_data)
            -- NOT IPAIRS because epoches don't start from 1 after the first one is deleted
            for epoch_id, epoch in pairs(raw_reload_data.epoch) do
                if epoch:needs_ending() then
                    epoch:__end_epoch()
                end
            end
        end)
    else
        log.debug("No epoch reload data detected.")
    end
end

-- Reload an old module table with it's reload hooks. Errors in this case are not
-- ignored by this function (and shouldn't be). Handle them in reinit nya
local function reload_old(module_table)
    if module_has_reload_data(module_table) then
        local hooks = reload.reload_hook(module_table)
        for _idx, hook in ipairs(hooks) do
            hook()
        end
    end
end

-- Forcefully unloads and reloads everything - user modules and entry point
--
-- Also accounts for errors in loading the new files and reverts to old config.
function reload.reinit()
    -- currently loaded.
    local old_tables = {}
    old_tables["cfg"] = require(mod_prefix)
    for module_name, module in pairs(package.loaded) do
        if vim.startswith(module_name, mod_prefix .. ".") then
            old_tables[module_name] = module
        end
    end

    -- New epoch
    new_epoch()

    -- Attempt unload but errors are warn-and-ok, we want to ensure we have a usable
    -- config in all cases.
    --
    -- In some cases, a new module is added but fails to load. In this case, old_module
    -- will not be a table
    for module_name, old_module in pairs(old_tables) do
        if type(old_module) == "table" and old_module.__unload ~= nil then
            local ok, res = pcall(old_module.__unload, old_module)
            if not ok then
                log.warn("Error during unload of %s:\n%s", module_name, res)
            end
        end

        -- End their old epochs ^.^
        log.debug("Ending epoch for %s", module_name)
        local ok, res = pcall(epoch_end, old_module)
        if not ok then
            log.warn("Error ending epoch for %s:\n%s", module_name, res)
        end

        -- Unconditionally unload it
        package.loaded[module_name] = nil
    end


    -- Reload, we also unconditionally set reload to true. Any .init() after this point
    -- will be a reload even if its just the old modules
    reload.reloaded = true

    -- Reload the old tables and init them
    local function revert_to_old()
        -- Put them back in the loaded first. This is
        -- because these modules may re-access things via mod() or require()
        -- and we don't want to load any failed modules that are new nya.
        for old_module_name, module in pairs(old_tables) do
            package.loaded[old_module_name] = module
        end

        -- Run any reloads, ignoring errors because at this point it's the best
        -- we got nya
        for old_module_name, module in pairs(old_tables) do
            local ok, res = pcall(reload_old, module)
            if not ok then
                log.warn("Error reloading the old module of %s:\n%s", old_module_name, res)
            end
        end

        -- Reinit
        --
        -- Here, we now set reload hook by default to false.
        local old_default_reload = default_register_data_reload
        default_register_data_reload = false
        require("cfg").init()
        default_register_data_reload = old_default_reload
    end

    local new_loaded_ok, msg_or_result = pcall(require, mod_prefix)
    if not new_loaded_ok then
        log.warn("Error loading modified cfg")
        log.warn(msg_or_result)
        log.warn("Reloading old modules")
        revert_to_old()
        return
    end

    -- In case of success, the result is the module table, which should have .init
    local new_init_ok, msg = pcall(msg_or_result.init)
    if not new_init_ok then
        log.warn("Error running init of modified cfg")
        log.warn(msg)
        log.warn("Reloading old modules")
        revert_to_old()
        return
    end
    -- Init has just been called so nothing new to do
end

return reload
